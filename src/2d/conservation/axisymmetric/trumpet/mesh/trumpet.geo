c=0.01;
clen_trumpet = 0.00135;
clen_box = 0.05;
w = 0.00062; //was 0.0006


// tube
Point(1) = {0.0*c,0.625*c,0,clen_trumpet};
Point(2) = {102*c,0.625*c,0,clen_trumpet};

// flare
Point(3)  = {109.5*c,0.75*c-w,0,clen_trumpet};
Point(4)  = {116*c,0.85*c-w,0,clen_trumpet};
Point(5)  = {122*c,0.975*c-w,0,clen_trumpet};
Point(6)  = {124.62*c,1.07*c-w,0,clen_trumpet};
Point(7)  = {126.26*c,1.13*c-w,0,clen_trumpet};
Point(8)  = {128.11*c,1.22*c-w,0,clen_trumpet};
Point(9)  = {131.03*c,1.34*c-w,0,clen_trumpet};
Point(10) = {132.54*c,1.42*c-w,0,clen_trumpet};
Point(11) = {133.76*c,1.51*c-w,0,clen_trumpet};
Point(12) = {134.54*c,1.56*c-w,0,clen_trumpet};
Point(13) = {135.62*c,1.64*c-w,0,clen_trumpet};
Point(14) = {136.41*c,1.71*c-w,0,clen_trumpet};
Point(15) = {137.36*c,1.79*c-w,0,clen_trumpet};
Point(16) = {138.98*c,1.96*c-w,0,clen_trumpet};
Point(17) = {139.61*c,2.07*c-w,0,clen_trumpet};
Point(18) = {140.34*c,2.24*c-w,0,clen_trumpet};
Point(19) = {141.24*c,2.45*c-w,0,clen_trumpet};
Point(20) = {142.13*c,2.69*c-w,0,clen_trumpet};
Point(21) = {142.72*c,2.89*c-w,0,clen_trumpet};
Point(22) = {143.72*c,3.3*c -w,0,clen_trumpet};
Point(23) = {144.25*c,3.57*c-w,0,clen_trumpet};
Point(24) = {144.77*c,3.89*c-w,0,clen_trumpet};
Point(25) = {145.22*c,4.17*c-w,0,clen_trumpet};
Point(26) = {145.8*c, 4.64*c-w,0,clen_trumpet};
Point(27) = {146.08*c,4.86*c-w,0,clen_trumpet};
Point(28) = {146.4*c,5.12*c-w,0,clen_trumpet};
Point(29) = {146.73*c,5.44*c-w,0,clen_trumpet};
Point(30) = {147.04*c,5.74*c-w,0,clen_trumpet};
Point(31) = {147.48*c,6.21*c-w,0,clen_trumpet};
Point(32) = {147.91*c,6.69*c-w,0,clen_trumpet};
Point(33) = {148*c,6.8*c-w,0,clen_trumpet};
Point(34) = {148*c,100*c,0,clen_box};
Point(35) = {250*c,100*c,0,clen_box};
Point(36) = {250*c,0*c,0,clen_trumpet};
Point(37) = {0*c,0*c,0,clen_trumpet};



Line(1) = {37, 36};
Line(2) = {36, 35};
Line(3) = {35, 34};
Line(4) = {34, 33};
Spline(5) = {33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
Line(6) = {2, 1};
Line(7) = {1, 37};
Line Loop(8) = {1, 2, 3, 4, 5, 6, 7};
Plane Surface(9) = {8};

Physical Surface(10) = {9};
Physical Line(40000) = {1}; //axis of symmetry
Physical Line(20000) = {2, 3, 4}; //outflow
Physical Line(10000) = {5, 6}; //reflecting
Physical Line(30000) = {7}; //inflow
