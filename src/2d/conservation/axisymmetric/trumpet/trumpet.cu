#include "../axisymmetric.cu"


#define GAMMA 1.4
#define PI 3.14159265359    

int limiter = NO_LIMITER;  
int time_integrator = RK2;  


/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

/* initial condition function
 *
 * returns the value of the intial condition at point x,y
 */
__device__ void U0(double *U, double x, double y) {
    U[0] = y*GAMMA;
    U[1] = y*0.;
    U[2] = y*0.;
    U[3] = y*1./ (GAMMA - 1.0);
}

/***********************
*
* INFLOW CONDITIONS
*
************************/

__device__ void U_inflow(double *U, double x, double y, double t) {
    // U[0] = y*GAMMA;
    // U[1] = y*0.;
    // U[2] = y*0.;
    // U[3] = y*1./ (GAMMA - 1.0);

//B3 dip first
	double p=1.0 + 0.000562 +((2*0.024103969335556)*cos(2*PI*0.712149262428284*t+-2.39985990524292))
                            +((2*0.0173638761043549)*cos(2*PI*1.42429852485657*t+-1.65178859233856))
                            +((2*0.00843499042093754)*cos(2*PI*2.13644790649414*t+-0.817656874656677))
                            +((2*0.00472985440865159)*cos(2*PI*2.84859704971313*t+-0.213300824165344))
                            +((2*0.00339356483891606)*cos(2*PI*3.56074643135071*t+-0.0655308961868286))
                            +((2*0.00256951176561415)*cos(2*PI*4.27289581298828*t+0.524391353130341))
                            +((2*0.00161341612692922)*cos(2*PI*4.98504495620728*t+0.860456168651581))
                            +((2*0.0008113564690575)*cos(2*PI*5.69719409942627*t+0.92175030708313))
                            +((2*0.000582741457037628)*cos(2*PI*6.40934324264526*t+0.62296724319458))
                            +((2*0.000501520407851785)*cos(2*PI*7.12149286270142*t+-0.220038667321205))
                            +((2*0.000474320579087362)*cos(2*PI*7.83364200592041*t+-0.434197187423706))
                            +((2*0.000284080480923876)*cos(2*PI*8.54579162597656*t+-1.21876406669617))
                            +((2*0.000177623485797085)*cos(2*PI*9.2579402923584*t+-0.982475578784943))
                            +((2*0.00026928965235129)*cos(2*PI*9.97008991241455*t+-1.51344656944275))
                            +((2*0.000165729026775807)*cos(2*PI*10.6822385787964*t+-2.16661334037781))
                            +((2*6.12890435149893e-005)*cos(2*PI*11.3943881988525*t+-2.24699807167053))
                            +((2*4.10981047025416e-005)*cos(2*PI*12.1065368652344*t+-3.0718514919281))
                            +((2*9.849512935034e-006)*cos(2*PI*12.8186864852905*t+3.12177920341492))
                            +((2*2.9585973607027e-005)*cos(2*PI*13.5308361053467*t+2.17879343032837))
                            +((2*2.76109149126569e-005)*cos(2*PI*14.2429857254028*t+2.24417519569397))
                            +((2*7.18223018338904e-005)*cos(2*PI*14.9551343917847*t+1.81599175930023))
                            +((2*3.77126525563654e-005)*cos(2*PI*15.6672840118408*t+2.17267441749573))
                            +((2*3.92250221921131e-005)*cos(2*PI*16.3794326782227*t+1.6726336479187))
                            +((2*2.08027067856165e-005)*cos(2*PI*17.0915832519531*t+2.17634844779968))
                            +((2*2.81628144875867e-005)*cos(2*PI*17.803731918335*t+0.624499201774597))
                            +((2*2.11836249945918e-005)*cos(2*PI*18.5158805847168*t+0.38453134894371))
                            +((2*4.63783544546459e-005)*cos(2*PI*19.2280311584473*t+1.66157603263855))
                            +((2*2.3801429051673e-005)*cos(2*PI*19.9401798248291*t+1.18837666511536))
                            +((2*3.38559475494549e-005)*cos(2*PI*20.6523284912109*t+2.31887578964233))
                            +((2*1.19861360872164e-005)*cos(2*PI*21.3644771575928*t+-2.23446178436279));

    double rho = GAMMA * pow(p, (1. / GAMMA));
    double u = ((p - (1+ 0.000562)) / GAMMA);
    double v = 0.;


    U[0] = y*rho;
    U[1] = y*u * rho; 
    U[2] = y*v * rho; 
    U[3] = y*(0.5*rho*(u*u+v*v) + (p / (GAMMA-1.)) ); 
}

/***********************
*
* OUTFLOW CONDITIONS
*
************************/

__device__ void U_outflow(double *U, double x, double y, double t) {
    // U_inflow(U, x, y, t);

    U[0] = y*GAMMA;
    U[1] = y*0.;
    U[2] = y*0.;
    U[3] = y*1./ (GAMMA - 1.0);
}

/***********************
*
* REFLECTING CONDITIONS
*
************************/
__device__ void U_reflection(double *U_left, double *U_right,
                             double x, double y, double t,
                             double nx, double ny) {

    // set rho and E to be the same in the ghost cell
    // normal reflection
    double dot;
    dot = U_left[1] * nx + U_left[2] * ny;

    U_right[0] = U_left[0];
    U_right[1] = U_left[1] - 2*dot*nx;
    U_right[2] = U_left[2] - 2*dot*ny;
    U_right[3] = U_left[3];

}

/***********************
 *
 * EXACT SOLUTION
 *
 ***********************/

__device__ void U_exact(double *U, double x, double y, double t) {
    U[0] = y*GAMMA;
    U[1] = y*0.;
    U[2] = y*0.;
    U[3] = y*1./ (GAMMA - 1.0);
}


/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/

__device__ double get_GAMMA() {
    return GAMMA;
}

int main(int argc, char *argv[]) {
    run_dgcuda(argc, argv);
}
