
#include <stdio.h>
#include "memoryCounters.cuh"
#include "operations.cuh"
#include "error_estimate.cuh"

#define N_MAX 4
#define NP_MAX 25
#define PI 3.14159265358979323

void eval_basis1d(double **basis_side_local, int local_n_p, int local_n_quad1d, double *s_r);
void eval_basis(double **basis_local, int local_n_p, int local_n_quad, double *r1_local, double *r2_local);

int e_mesh_max;

double degree1[] = {0.333333333333333, 0.333333333333333, 1.0};
// 3 points
double degree2[] = {0.166666666666666, 0.166666666666666, 0.333333333333333,
                   0.666666666666666, 0.166666666666666, 0.333333333333333,
                   0.166666666666666, 0.666666666666666, 0.333333333333333};
// 4 points
double degree3[] = {0.333333333333333,0.3333333333333333,-0.5625,
                   0.6,0.2,.520833333333333,
                   0.2,0.6,.520833333333333,
                   0.2,0.2,.520833333333333};
// 6 points
double degree4[] = {0.816847572980459,0.091576213509771,0.109951743655322,
                   0.091576213509771,0.816847572980459,0.109951743655322,
                   0.091576213509771,0.091576213509771,0.109951743655322,
                   0.108103018168070,0.445948490915965,0.223381589678011,
                   0.445948490915965,0.108103018168070,0.223381589678011,
                   0.445948490915965,0.445948490915965,0.223381589678011};
// 7 points
double degree5[] = {0.333333333333333,0.333333333333333,0.225000000000000,
                   0.797426985353087,0.101286507323456,0.125939180544827,
                   0.101286507323456,0.797426985353087,0.125939180544827,
                   0.101286507323456,0.101286507323456,0.125939180544827,
                   0.470142064105115,0.059715871789770,0.132394152788506,
                   0.059715871789770,0.470142064105115,0.132394152788506,
                   0.470142064105115,0.470142064105115,0.132394152788506};
// 12 points
double degree6[] = {0.873821971016996,0.063089014491502,0.050844906370207,
                   0.063089014491502,0.873821971016996,0.050844906370207,
                   0.063089014491502,0.063089014491502,0.050844906370207,
                   0.501426509658179,0.249286745170910,0.116786275726379,
                   0.249286745170910,0.501426509658179,0.116786275726379,
                   0.249286745170910,0.249286745170910,0.116786275726379,
                   0.636502499121399,0.310352451033784,0.082851075618374,
                   0.310352451033784,0.636502499121399,0.082851075618374,
                   0.636502499121399,0.053145049844816,0.082851075618374,
                   0.310352451033784,0.053145049844816,0.082851075618374,
                   0.053145049844816,0.310352451033785,0.082851075618374,
                   0.053145049844816,0.636502499121399,0.082851075618374};
// 13 points
double degree7[] = {0.333333333333333,0.333333333333333,-0.149570044467682,
                   0.479308067841920,0.260345966079040,0.175615257433208,
                   0.260345966079040,0.479308067841920,0.175615257433208,
                   0.260345966079040,0.260345966079040,0.175615257433208,
                   0.869739794195568,0.065130102902216,0.053347235608838,
                   0.065130102902216,0.869739794195568,0.053347235608838,
                   0.065130102902216,0.065130102902216,0.053347235608838,
                   0.048690315425316,0.312865496004874,0.077113760890257,
                   0.312865496004874,0.048690315425316,0.077113760890257,
                   0.638444188569810,0.048690315425316,0.077113760890257,
                   0.048690315425316,0.638444188569810,0.077113760890257,
                   0.312865496004874,0.638444188569810,0.077113760890257,
                   0.638444188569810,0.312865496004874,0.077113760890257};
// 16 points
double degree8[] = {0.333333333333333,0.333333333333333,0.144315607677787,
                   0.081414823414554,0.459292588292723,0.095091634267285,
                   0.459292588292723,0.081414823414554,0.095091634267285,
                   0.459292588292723,0.459292588292723,0.095091634267285,
                   0.658861384496480,0.170569307751760,0.103217370534718,
                   0.170569307751760,0.658861384496480,0.103217370534718,
                   0.170569307751760,0.170569307751760,0.103217370534718,
                   0.898905543365938,0.050547228317031,0.032458497623198,
                   0.050547228317031,0.898905543365938,0.032458497623198,
                   0.050547228317031,0.050547228317031,0.032458497623198,  
                   0.008394777409958,0.728492392955404,0.027230314174435,
                   0.728492392955404,0.008394777409958,0.027230314174435,
                   0.263112829634638,0.008394777409958,0.027230314174435,
                   0.008394777409958,0.263112829634638,0.027230314174435,
                   0.263112829634638,0.728492392955404,0.027230314174435,
                   0.728492392955404,0.263112829634638,0.027230314174435};
// 19 points
double degree9[] = {0.333333333333333,0.333333333333333,0.097135796282799,
                   0.020634961602525,0.489682519198738,0.031334700227139,
                   0.489682519198738,0.020634961602525,0.031334700227139,
                   0.489682519198738,0.489682519198738,0.031334700227139,
                   0.125820817014127,0.437089591492937,0.077827541004774,
                   0.437089591492937,0.125820817014127,0.077827541004774,
                   0.437089591492937,0.437089591492937,0.077827541004774,
                   0.623592928761935,0.188203535619033,0.079647738927210,
                   0.188203535619033,0.623592928761935,0.079647738927210,
                   0.188203535619033,0.188203535619033,0.079647738927210,
                   0.910540973211095,0.044729513394453,0.025577675658698,
                   0.044729513394453,0.910540973211095,0.025577675658698,
                   0.044729513394453,0.044729513394453,0.025577675658698,
                   0.036838412054736,0.221962989160766,0.043283539377289,
                   0.221962989160766,0.036838412054736,0.043283539377289,
                   0.036838412054736,0.741198598784498,0.043283539377289,
                   0.741198598784498,0.036838412054736,0.043283539377289,
                   0.741198598784498,0.221962989160766,0.043283539377289,
                   0.221962989160766,0.741198598784498,0.043283539377289};
// 25 points
double degree10[] = {0.333333333333333,0.333333333333333,0.090817990382754,
                   0.028844733232685,0.485577633383657,0.036725957756467,
                   0.485577633383657,0.028844733232685,0.036725957756467,
                   0.485577633383657,0.485577633383657,0.036725957756467,
                   0.781036849029926,0.109481575485037,0.045321059435528,
                   0.109481575485037,0.781036849029926,0.045321059435528,
                   0.109481575485037,0.109481575485037,0.045321059435528,
                   0.141707219414880,0.307939838764121,0.072757916845420,
                   0.307939838764121,0.141707219414880,0.072757916845420,
                   0.307939838764121,0.550352941820999,0.072757916845420,
                   0.550352941820999,0.307939838764121,0.072757916845420,
                   0.550352941820999,0.141707219414880,0.072757916845420,
                   0.141707219414880,0.550352941820999,0.072757916845420,
                   0.025003534762686,0.246672560639903,0.028327242531057,
                   0.246672560639903,0.025003534762686,0.028327242531057,
                   0.025003534762686,0.728323904597411,0.028327242531057,
                   0.728323904597411,0.025003534762686,0.028327242531057,
                   0.728323904597411,0.246672560639903,0.028327242531057,
                   0.246672560639903,0.728323904597411,0.028327242531057,
                   0.009540815400299,0.066803251012200,0.009421666963733,
                   0.066803251012200,0.009540815400299,0.009421666963733,
                   0.066803251012200,0.923655933587500,0.009421666963733,
                   0.923655933587500,0.066803251012200,0.009421666963733,
                   0.923655933587500,0.009540815400299,0.009421666963733,
                   0.009540815400299,0.923655933587500,0.009421666963733};

double *quadrature_2d[] = {degree1, degree2, degree3,
                            degree4, degree5, degree6,
                            degree7, degree8, degree9,
                            degree10}; 

////////////////////////////////////////
// 1D Quadrature Rules
////////////////////////////////////////

// order goes (r1, w1, r2, w2, ...) 
double degree1_1d[] = {0.0000000000000000e+00,2.000000000000000e+00};
double degree2_1d[] = {-5.773502691896257e-01,1.,
                             5.773502691896257e-01, 1.};

double degree3_1d[] = {-7.745966692414834e-01, 5.555555555555552e-01, 
                           0.000000000000000e0, 8.888888888888888e-01,  
                           7.745966692414834e-01, 5.555555555555552e-01};

double degree4_1d[] = {-8.611363115940526e-01,3.478548451374537e-01, 
                            -3.399810435848563e-01, 6.521451548625464e-01,  
                            3.399810435848563e-01, 6.521451548625464e-01, 
                            8.611363115940526e-01, 3.478548451374537e-01};

double degree5_1d[] = {-9.061798459386640e-01,2.369268850561890e-01, 
                            -5.384693101056831e-01,  4.786286704993665e-01, 
                            0.000000000000000e+00, 5.688888888888889e-01, 
                            5.384693101056831e-01, 4.786286704993665e-01, 
                            9.061798459386640e-01,2.369268850561890e-01};

double degree6_1d[] = {-9.324695142031521e-01,1.713244923791705e-01, 
                            -6.612093864662646e-01, 3.607615730481386e-01, 
                            -2.386191860831969e-01,4.679139345726913e-01, 
                            2.386191860831969e-01, 4.679139345726913e-01, 
                            6.612093864662646e-01, 3.607615730481386e-01, 
                            9.324695142031521e-01, 1.713244923791705e-01};

double degree7_1d[] = {-9.491079123427585e-01,1.294849661688697e-01, 
                            -7.415311855993945e-01, 2.797053914892767e-01, 
                            -4.058451513773972e-01, 3.818300505051190e-01, 
                            0.000000000000000e+00, 4.179591836734694e-01, 
                            4.058451513773972e-01, 3.818300505051190e-01, 
                            7.415311855993945e-01, 2.797053914892767e-01, 
                            9.491079123427585e-01, 1.294849661688697e-01};

double degree8_1d[] = {-9.602898564975363e-01,1.012285362903768e-01, 
                            -7.966664774136268e-01, 2.223810344533745e-01, 
                            -5.255324099163290e-01,3.137066458778874e-01, 
                            -1.834346424956498e-01, 3.626837833783620e-01, 
                            1.834346424956498e-01, 3.626837833783620e-01, 
                            5.255324099163290e-01, 3.137066458778874e-01, 
                            7.966664774136268e-01, 2.223810344533745e-01, 
                            9.602898564975363e-01, 1.012285362903768e-01};

double *quadrature_1d[] = {degree1_1d, degree2_1d, degree3_1d,
                          degree4_1d, degree5_1d, degree6_1d,
                          degree7_1d, degree8_1d};


int n_quad_higher_order, local_np;
int approx_order;


__device__ static double *d_w;
__device__ static double *d_basis_higher_order;
int n_quad1d;
__device__ static double *d_w_oned;
__device__ static double *d_basis_side;
__device__ __constant__ double r1_higher_order[32];
__device__ __constant__ double r2_higher_order[32];

double **h_rhs;
__device__ double *d_length;
__device__ static double *d_basis_vertex;




// gradient error estimate
__global__ void eval_gradient(double *error, double **c, 
                                    double *gxr, double *gxs,
                                    double *gyr, double *gys,
                                    double *gJ,
                                    int n_p, int n_quad,
                                    double *d_w, double *d_basis_grad_x, double *d_basis_grad_y,
                                    int curr_num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    double dx, dy;
    double xr, xs, yr, ys, J;

    if (idx < curr_num_elem) {

        dx = 0;
        dy = 0; 
        xr = gxr[idx];
        xs = gxs[idx];
        yr = gyr[idx];
        ys = gys[idx];
        J = gJ[idx];
 
        for(int i = 0; i < n_p; i++) {
            dx += c[0*n_p + i][idx] * (d_basis_grad_x[n_quad * i + 0] * ys + d_basis_grad_y[n_quad * i + 0] * (-yr));
            dy += c[0*n_p + i][idx] * (d_basis_grad_x[n_quad * i + 0] * (-xs) + d_basis_grad_y[n_quad * i + 0] * xr);
        }

        error[idx] = sqrtf(dx * dx + dy * dy)/J/d_w[0];
    }

}



__global__ void eval_gradient(double *error, double **c, 
                                   double *V1x, double *V1y,
                                   double *V2x, double *V2y,
                                   double *V3x, double *V3y,
                                    double *gxr, double *gxs,
                                    double *gyr, double *gys,
                                    double *gJ,
                                    int n_p, int n_quad,
                                    double *d_w, double *d_basis_grad_x, double *d_basis_grad_y,
                                    int curr_num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    double dx, dy;
    double xr, xs, yr, ys, J;
    double centroid_y;
    double rU, dr, dz;

    if (idx < curr_num_elem) {

        dx = 0;
        dy = 0; 
        rU = c[0][idx]*sqrtf(2); 
        xr = gxr[idx];
        xs = gxs[idx];
        yr = gyr[idx];
        ys = gys[idx];
        J = gJ[idx];
        centroid_y = (V1y[idx] + V2y[idx] + V3y[idx])/3. ;

        for(int i = 0; i < n_p; i++) {
            dx += c[0*n_p + i][idx] * (d_basis_grad_x[n_quad * i + 0] * ys + d_basis_grad_y[n_quad * i + 0] * (-yr))/J/d_w[0];
            dy += c[0*n_p + i][idx] * (d_basis_grad_x[n_quad * i + 0] * (-xs) + d_basis_grad_y[n_quad * i + 0] * xr)/J/d_w[0];
        }

        dz = (1/centroid_y) * dx;
        dr = (-1/centroid_y/centroid_y) * rU + dy/centroid_y;
        error[idx] = sqrtf(dz * dz + dr * dr);
    }

}

void allocate_error_estimate(memoryCounters *counter, int in_e_mesh_max){
    e_mesh_max = in_e_mesh_max;
    allocate((void **) &d_w, 1e3 * sizeof(double), &(counter->emesh_memory));
    allocate((void **) &d_basis_higher_order, 1e3 * sizeof(double), &(counter->emesh_memory));
    allocate((void **) &d_w_oned, 1e3 * sizeof(double), &(counter->emesh_memory));
    allocate((void **) &d_basis_side, 1e3 * sizeof(double), &(counter->emesh_memory));
    allocate((void **) &d_length, e_mesh_max * sizeof(double), &(counter->emesh_memory));
}

void init_error_estimate(int local_n_p, int degree1d, int degree2d, int in_approx_order, double **in_h_k1, double *in_basis_vertex){
    d_basis_vertex = in_basis_vertex;


    double *basis, *r1, *r2, *w;
    double *basis_side, *r, *w_1d;

    switch (degree1d) {
        case 0: n_quad1d = 1;
                break;
        case 1: n_quad1d = 2;
                break;
        case 2: n_quad1d = 3;
                break;
        case 3: n_quad1d = 4 ;
                break;
        case 4: n_quad1d = 5;
                break;
        case 5: n_quad1d = 6;
                break;
    }
    // allocate integration points
    r = (double *)  malloc(n_quad1d * sizeof(double));
    w_1d  = (double *) malloc(n_quad1d * sizeof(double));

    // set 2D quadrature rules
    for (int i = 0; i < n_quad1d; i++) {
        r[i] = quadrature_1d[degree1d][2*i];
        w_1d[i]  = quadrature_1d[degree1d][2*i+1] ; //weights are 2 times too big for some reason
    }
    eval_basis1d(&basis_side, local_n_p, n_quad1d, r);
    cudaMemcpy(d_w_oned,w_1d, n_quad1d * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_basis_side,basis_side, 9*n_quad1d * local_n_p * sizeof(double), cudaMemcpyHostToDevice);

    switch (degree2d) {
        case 0: n_quad_higher_order = 1;
                break;
        case 1: n_quad_higher_order = 3;
                break;
        case 2: n_quad_higher_order = 6;
                break;
        case 3: n_quad_higher_order = 12 ;
                break;
        case 4: n_quad_higher_order = 16;
                break;
        case 5: n_quad_higher_order = 25;
                break;
    }

    // allocate integration points
    r1 = (double *)  malloc(n_quad_higher_order * sizeof(double));
    r2 = (double *)  malloc(n_quad_higher_order * sizeof(double));
    w  = (double *) malloc(n_quad_higher_order * sizeof(double));

    // set 2D quadrature rules
    for (int i = 0; i < n_quad_higher_order; i++) {
        r1[i] = quadrature_2d[2 * degree2d - 1][3*i];
        r2[i] = quadrature_2d[2 * degree2d - 1][3*i+1];
        w[i]  = quadrature_2d[2 * degree2d - 1][3*i+2] / 2.; //weights are 2 times too big for some reason
    }

    eval_basis(&basis, local_n_p, n_quad_higher_order, r1, r2);
    cudaMemcpy(d_w,w, n_quad_higher_order * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_basis_higher_order,basis, n_quad_higher_order * local_n_p * sizeof(double), cudaMemcpyHostToDevice);

    cudaMemcpyToSymbol(r1_higher_order, r1, n_quad_higher_order*sizeof(double));
    cudaMemcpyToSymbol(r2_higher_order, r2, n_quad_higher_order*sizeof(double));

    approx_order = in_approx_order;
    local_np = local_n_p;
    h_rhs = in_h_k1;

}

__device__ void get_coordinates_2d_higher_order(double *x, double *V, int j) {
    // x = x2 * r + x3 * s + x1 * (1 - r - s)
    x[0] = V[2] * r1_higher_order[j] + V[4] * r2_higher_order[j] + V[0] * (1 - r1_higher_order[j] - r2_higher_order[j]);
    x[1] = V[3] * r1_higher_order[j] + V[5] * r2_higher_order[j] + V[1] * (1 - r1_higher_order[j] - r2_higher_order[j]);
}


// #define GAMMA 1.4
// #define PI 3.14159265358979323846
// #define MACH 10.
// #define X0 0.16666666666666666
// #define THETA 90.

// __device__ static void U_shock(double *U, double x, double y) {
//     double p;
//     double ratio = (90.-THETA) / 180.;

//     p = 116.5;
//     U[0] = 8.;
//     U[1] =  8.25 * cospi(ratio) * U[0];
//     U[2] = -8.25 * sinpi(ratio) * U[0];
//     U[3] = 0.5 * U[0] * 8.25 * 8.25 + p / (GAMMA - 1.0);
// }

// __device__ static void U0(double *U, double x, double y) {
//     double angle = atan(y / (x - X0));
//     if (angle > THETA * PI / 180 || x < X0) {
//         U_shock(U, x, y);
//     } else {
//         U[0] = GAMMA;
//         U[1] = 0.;
//         U[2] = 0.;
//         U[3] = 1./ (GAMMA - 1.0);
//     }
// }

__device__ static void U0(double *U, double x, double y) {
    // no3 vs all
    // double x0=-0.25, y0= 0. ; 
    // if (x - x0< .25 && x - x0> -.25 && y -y0< .25 && y -y0 > -.25) {
    //     U[0] = 1. ;
    // } else {
    //     U[0] = 0.;  
    // }


    // c2 rot
    // double x0, y0 ; 
    // double x_rot, y_rot ; 

    // x_rot =  sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // y_rot =  -sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // x0 = -0.25/sqrt(2.);
    // y0 = 0.25/sqrt(2.);
    // if (x_rot - x0< .25 && x_rot - x0> -.25 && y_rot -y0< .25 && y_rot -y0 > -.25) {
    //     U[0] = 1.;
    // } else {
    //     U[0] = 0;  
    // }


        U[0] = -0.5 * x*x*x*x - 0.5 * y * y * y* y; 
        // U[0] = -0.5 * x*x*x - 0.5 * y * y * y;  
        // U[0] = -0.5 * x*x - 0.5 * y * y;  
        // U[0] = -0.5 * x - 0.5 * y;  



    // double x0, y0 ,r; 
    // x0 = 0.;
    // y0 = 0.;
    // r =   sqrtf((x+x0)*(x+x0) +(y+y0)*(y+y0))  ;
    // U[0] = 5*exp(-pow(r,2)/(0.01)    );



    // example comparing no3 and all
    // double x0, y0 ,r; 
    // x0 = -0.25;
    // y0 = 0.;
    // r = 0.15;
    // U[0] = 5.*exp(-(pow(x - x0,2) + pow(y - y0,2))/(2.*r*r)    );
  
    // advecting hill
    // double x0, y0 ,r; 
    // x0 = -0.25;
    // y0 = -0.25;
    // r = 0.15;
    // U[0] = 5*exp(-(pow(x - x0,2) + pow(y - y0,2))/(2.*r*r)    );
 
    //compare rot no3 and rot all
    // U[0] = 0.;
    // double x0 = 0.25, y0 = 0.25;
    // double r =   sqrtf((x+x0)*(x+x0) +(y+y0)*(y+y0))  ;
    // if(r <= 0.25){ // hill
    //     U[0] = cospi(2*r) * cospi(2*r);
    // }


    
    // double x0, y0 ,r; 
    // x0 = -0.25;
    // y0 = 0.;
    // r = 0.15;
    // U[0] = exp(-(pow(x - x0,2) + pow(y - y0,2))/(2.*r*r)    );

    // example rotating shapes
    // U[0] = 0.;
    // double x0 = 0.5, y0 = 0.;
    // double r =   sqrtf((x+x0)*(x+x0) +(y+y0)*(y+y0)) /0.25 ;
    // if(r <= 1.-1e-10){
    //   U[0] = exp(- .1 / (1-r*r));
    // }
    // double r = 0.1;



    // double x0, y0 ,r; 
    // x0 = -0.25; 
    // y0 = -0.25;
    // r = 0.25;
    // if(sqrt((x-x0)*(x-x0) + (y-y0)*(y-y0))/r < 1.)
    //   U[0] = exp( - 1. / (1.-sqrt((x-x0)*(x-x0)+ (y-y0)*(y-y0))/r) ) + 1.;
    // else
    //   U[0] = 0.;




    // if(r <= 0.25){ // cone
    //     U[0] = cospi(2*r) * cospi(2*r);
    // }

    // x0 = 0.35; y0 = 0.;
    // r =  fmax( fabs(x-x0), fabs(y-y0));
    // if(r <= 0.25){ // square
    //     U[0] = 1. ;
    // }


    // double x0, y0 ; 
    // double x_rot, y_rot ;  

    // // x_rot =  sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // // y_rot =  -sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // x_rot =  sqrt(2.)/2. * x - sqrt(2.)/2. * y;
    // y_rot =  sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // x0 = 0.;
    // y0 = 0.;
    // if (x_rot - x0< .75 && x_rot - x0> -.25 && y_rot -y0< .25 && y_rot -y0 > -.25) {
    //     U[0] = (x_rot+0.25); 
    // } else {
    //     U[0] = 0;  
    // }

    // double x0, y0 ,r; 
    // x0 = -0.25;
    // y0 = -0.25;
    // r = 0.15;
    // U[0] = 5*exp(-(pow(x - x0,2) + 
    //                   pow(y - y0,2))/(2.*r*r)    );
 



    // double x0, y0 ,r; 
    // x0 = -0.25;
    // y0 = -0.25;
    // r = 0.15;
    // U[0] = 5*exp(-(pow(x - x0,2) + 
    //                   pow(y - y0,2))/(2.*r*r)    );
 

    // double x0=-0.25, y0=-0.25 ; 
    // if (x - x0< .25 && x - x0> -.25 && y -y0< .25 && y -y0 > -.25) {
    //     U[0] = 1. - (x-x0)*(x-x0) - (y-y0)*(y-y0);
    // } else {
    //     U[0] = 0.;  
    // }








  
    // if(x <= 0.)
    //     U[0] = 1.;
    // else
    //     U[0] = 0.;

    // USED IN CONVERGENCE ANALYSIS
    // double x0, y0 ,r; 
    // x0 = -0.2;
    // y0 = -0.2;
    // r = 0.15;
    // U[0] = 5*exp(-(pow(x - x0,2) + 
    //                   pow(y - y0,2))/(2.*r*r)    );



    // U[0] =  - 0.5 * x - 0.5 * y;
    // U[0] = sinpi(x) * sinpi(y);  
    // double x0=0., y0=0. ; 
    // if (x - x0< .25 && x - x0> -.25 && y -y0< .25 && y -y0 > -.25) {
    //     U[0] = 1.;
    // } else {
    //     U[0] = 0;  
    // }

    // double xp = (x+1.)/2.;
    // double yp = (y+1.)/2.;


  // experiment 1
    // U[0] = 0.;
    // double x0, y0 ,r0 =  0.15, r ;
    // double xp = (x+1)/2., yp = (y+1.)/2.;
    // x0 = 0.5; y0 = 0.75;
    // r =  sqrt(  (xp-x0)*(xp-x0) +(yp-y0)*(yp-y0)  )/r0;

    // if(r <= 1.){ // slotted cylinder
    //     if(abs(xp-x0) >= 0.025 || yp >=0.85)
    //         U[0] = 1.;
    //     else
    //         U[0] = 0.;
    // }

    // x0 = 0.5; y0 = 0.25;
    // r =  sqrt(  (xp-x0)*(xp-x0) +(yp-y0)*(yp-y0)  )/r0;

    // if(r <= 1.){ // cone
    //     U[0] = 1. - r;
    // }

    // x0 = 0.25; y0 = 0.5;
    // r =  sqrt(  (xp-x0)*(xp-x0) +(yp-y0)*(yp-y0)  )/r0;

    // if(r <= 1.){ // hump
    //     U[0] = (1+cospi(r))/4.;
    // }


    // experiment 2
    // U[0] = 0.;
    // double x0, y0 ,r0 =  0.35, r ;

    // x0 = -0.45; y0 = 0.;
    // r =  sqrt(  (x-x0)*(x-x0) +(y-y0)*(y-y0)  )/r0;

    // if(r <= 1.){ // cone
    //     U[0] = 1. - r;
    // }

    // if(0.1 < x && x < 0.6 && -0.25 < y && y < 0.25)
    //     U[0] = 1.;




    // double x0, y0 ,r; 
    // x0 = -0.25; 
    // y0 = -0.25;
    // r = 0.7;
    // if(sqrt((x-x0)*(x-x0) + (y-y0)*(y-y0))/r < 1.)
    //   U[0] = exp(1.)*exp( - 1. / (1.-sqrt((x-x0)*(x-x0)+ (y-y0)*(y-y0))/r) );
    // else
    //   U[0] = 0.;

    // double x0, y0 ; 
    // x0 = 0.5;
    // y0 = 0;
    // if (x - x0< .25 && x - x0> -.25 && y -y0< .25 && y -y0 > -.25) {
    //     U[0] = 5.;
    // } else {
    //     U[0] = 0;  
    // }
    // U[0] = -0.5*x-0.5*y; 

    // double x0, y0 ; 
    // x0 = -0.5;
    // y0 = -0.5;
    // if (x - x0< .25 && x - x0> -.25 && y -y0< .25 && y -y0 > -.25) {
    //     U[0] = 1.;
    // } else {
    //     U[0] = 0;  
    // }
    // double x0, y0 ; 
    // double x_rot, y_rot ; 

    // x_rot =  sqrt(2.)/2. * x - sqrt(2.)/2. * y;
    // y_rot =  sqrt(2.)/2. * x + sqrt(2.)/2. * y;

    // x0 = 0.;
    // y0 = 0.;
    // if (x_rot - x0< .25 && x_rot - x0> -.25 && y_rot -y0< .25 && y_rot -y0 > -.25) {
    //     U[0] = 1.;
    // } else {
    //     U[0] = 0;   
    // }
    // double x0, y0 ; 
    // if (x - x0< .25 && x - x0> -.25 && y -y0< .25 && y -y0 > -.25) {
    //     U[0] = 1.;
    // } else {
    //     U[0] = 0;   
    // }

    // U[0] = 0.;
    // double x0, y0 ,r0 =  0.3, r ;
    // x0 = 0; y0 = 0.5;
    // r =  sqrt(  (x-x0)*(x-x0) +(y-y0)*(y-y0)  )/r0;

    // if(r <= 1.){ // slotted cylinder
    //     if(abs(x-x0) >= 0.05 || y >=0.65)
    //         U[0] = 1.;
    //     else
    //         U[0] = 0.;
    // }

    // x0 = 0.; y0 = -0.5;
    // r =  sqrt(  (x-x0)*(x-x0) +(y-y0)*(y-y0)  )/r0;

    // if(r <= 1.){ // cone
    //     U[0] = 1. - r;
    // }

    // x0 = -0.5; y0 = 0.;
    // r =  sqrt(  (x-x0)*(x-x0) +(y-y0)*(y-y0)  )/r0;

    // if(r <= 1.){ // hump
    //     U[0] = (1+cospi(r))/4.;
    // }

    // double p, rho, u, v, r, alpha = 0.9, beta = 10., GAMMA = 1.4;
    // y += 0.5;
    // x*=10.;
    // y*=10.;

    // r = sqrt(x*x + y*y);
    // rho = pow( 2. - (GAMMA-1)*beta*beta * exp(1-r*r)/(8 * GAMMA * PI * PI), 1./(GAMMA-1.));
    // p = pow(rho, GAMMA);
    // u = 0. - y * beta * exp(0.5*(1-r*r)) / (2 * PI);
    // v = 1  + x * beta * exp(0.5*(1-r*r)) / (2 * PI);

    // U[0] = rho;
    // U[1] = rho*u; 
    // U[2] = rho*v; 
    // U[3] = (0.5*rho*(u*u+v*v) + (p / (GAMMA-1.)) ); 
}
__device__ static void riemann_problem(double *U, double x, double t){
double rho_l, u_l, p_l, c_l; //left states
double rho_r, u_r, p_r, c_r; //right states
double GAMMA = 1.4;

double p, u, rho_l_star, rho_r_star, c_l_star, c_r_star;
double rho_out, u_out, p_out, u_temp, p_temp, rho_temp, xi;
double ws[5];



// stationary shock
rho_l = 1.;
u_l = -19.58745;
p_l = 1000.;
c_l = sqrt(GAMMA*p_l/rho_l);

rho_r = 1.;
u_r = -19.58745;
p_r = 0.01;
c_r = sqrt(GAMMA*p_r/rho_r);

ws[0] = -5.7004023868e+01;
ws[1] = -3.3487082201e+01;
ws[2] =  1.0001388723e-02;
ws[3] = 3.9300869669e+00;
ws[4] = 3.9300869669e+00;

rho_l_star=5.7506229848e-01;
rho_r_star=5.9992407048e+00 ;
u = 1.0001388723e-02;
p = 4.6089378749e+02;


if(t > 0.){
  xi = x/t;
	if(xi <= ws[0]){ // left state
	  rho_out = rho_l;
	  u_out = u_l;
	  p_out = p_l;
	}
	else if(ws[0] < xi && xi <= ws[1]){ // 1-rarefaction
	  u_temp = ((GAMMA-1.)*u_l + 2*(c_l + xi))/(GAMMA+1.);
	  rho_temp = pow((pow(rho_l,GAMMA)*(u_temp-xi)*(u_temp-xi)/(GAMMA*p_l)), 1./(GAMMA-1.) );
	  p_temp = p_l*pow((rho_temp/rho_l),GAMMA);
	  rho_out = rho_temp;
	  u_out = u_temp;
	  p_out = p_temp;
	}
	else if(ws[1] < xi && xi <= ws[2]){ // left-of-contact
	  rho_out = rho_l_star;
	  u_out = u;
	  p_out = p;
	}
	else if(ws[2] < xi && xi <= ws[3]){ // right-of-contact
	  rho_out = rho_r_star;
	  u_out = u;
	  p_out = p;
	}
	else if(ws[3] < xi && xi <= ws[4]){ // 3-rarefaction
	  u_temp = ((GAMMA-1.)*u_r - 2*(c_r - xi))/(GAMMA+1.); 
	  rho_temp = pow((pow(rho_r,GAMMA)*(xi-u_temp)*(xi-u_temp)/(GAMMA*p_r)), 1./(GAMMA-1.) );
	  p_temp = p_r*pow((rho_temp/rho_r),GAMMA);

	  rho_out = rho_temp;
	  u_out = u_temp;
	  p_out = p_temp;
	}
	else{
	  rho_out = rho_r;
	  u_out = u_r;
	  p_out = p_r;
	}
}
else{
	if(x <= 0){ // left state
	  rho_out = rho_l;
	  u_out = u_l;
	  p_out = p_l;
	}
	else{
	  rho_out = rho_r;
	  u_out = u_r;
	  p_out = p_r;
	}
}

U[0] = rho_out;
U[1] = rho_out * u_out;
U[2] = 0.;
U[3] = p_out/(GAMMA-1.) + 0.5*rho_out*u_out*u_out;

}


__device__ static void U_exact(double *U, double x, double y, double t) {
    U0(U, x - t , y - t);
    // initial(U, x*cospi(2*t) + y*sinpi(2*t), -x*sinpi(2*t) + y*cospi(2*t));
    // double s = 1./3.;
    // if(x <=  s*t)
    //     U[0] = 1.;
    // else
    //     U[0] = 0.;

  
    // U0(U, x - t, y );
    // U0(U, x*cospi(2*t) + y*sinpi(2*t), -x*sinpi(2*t) + y*cospi(2*t));

    // double x0, y0 ; 
    // double x_rot, y_rot ;  

    // x_rot =  sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // y_rot =  -sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // // x_rot =  sqrt(2.)/2. * x - sqrt(2.)/2. * y;
    // // y_rot =  sqrt(2.)/2. * x + sqrt(2.)/2. * y;
    // x0 = 0.;
    // y0 = 0.;
    // if (x_rot - x0< .75 && x_rot - x0> -.25 && y_rot -y0< .25 && y_rot -y0 > -.25) {
    //     U[0] = (x_rot+0.25); 
    // } else {
    //     U[0] = 0;  
    // }






    // experiment 85
    // U[0] = 0.;
    // double x0, y0 ,r0 =  0.15, r ;
    // double xp = (x+1)/2., yp = (y+1.)/2.;
    // x0 = 0.5; y0 = 0.75;
    // r =  sqrt(  (xp-x0)*(xp-x0) +(yp-y0)*(yp-y0)  )/r0;

    // if(r <= 1.){ // slotted cylinder
    //     if(abs(xp-x0) >= 0.025 || yp >=0.85)
    //         U[0] = 1.;
    //     else
    //         U[0] = 0.;
    // }

    // x0 = 0.5; y0 = 0.25;
    // r =  sqrt(  (xp-x0)*(xp-x0) +(yp-y0)*(yp-y0)  )/r0;

    // if(r <= 1.){ // cone
    //     U[0] = 1. - r;
    // }










    

    // U[0] = 0.;
    // double x0, y0 ,r0 =  0.3, r ;
    // x0 = 0; y0 = 0.5;
    // r =  sqrt(  (x-x0)*(x-x0) +(y-y0)*(y-y0)  )/r0;

    // if(r <= 1.){ // slotted cylinder
    //     if(abs(x-x0) >= 0.05 || y >=0.65)
    //         U[0] = 1.;
    //     else
    //         U[0] = 0.;
    // }

    // x0 = 0.; y0 = -0.5;
    // r =  sqrt(  (x-x0)*(x-x0) +(y-y0)*(y-y0)  )/r0;

    // if(r <= 1.){ // cone
    //     U[0] = 1. - r;
    // }

    // x0 = -0.5; y0 = 0.;
    // r =  sqrt(  (x-x0)*(x-x0) +(y-y0)*(y-y0)  )/r0;

    // if(r <= 1.){ // hump
    //     U[0] = (1+cospi(r))/4.;
    // }
    // riemann_problem(U, x, t);


    // double p, GAMMA = 1.4; 
    // if(x < 0 + 10. * t){
    //   p = 116.5;
    //   U[0] = 8.;
    //   U[1] =  8.25 * U[0];
    //   U[2] = 0.;
    //   U[3] = 0.5 * U[0] * 8.25 * 8.25 + p / (GAMMA - 1.0);
    // }
    // else{
    //     U[0] = GAMMA;
    //     U[1] = 0.;
    //     U[2] = 0.;
    //     U[3] = 1./ (GAMMA - 1.0);
    // }

    // double x0=0., y0=0. ; 
    // if (x - x0 - t< .25 && x - x0 - t> -.25 && y -y0 - t < .25 && y -y0 -t > -.25) {
    //     U[0] = 1.;
    // } else {
    //     U[0] = 0;  
    // }

}



__global__ void eval_error_L1_higher_order_m(double **C, double *error,
                                           double *V1x, double *V1y,
                                           double *V2x, double *V2y,
                                           double *V3x, double *V3y,
                                           double *J,
                                           int n, double t,
                                           double *w, double *basis_higher_order,
                                           int n_quad_higher_order,
                                           int n_p,
                                           int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i, j;
        double e;
        double X[2];
        double U;
        double u[4];
        double V[6];
        double detJ;

        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        detJ = J[idx];
        e = 0.;
        for (j = 0; j < n_quad_higher_order; j++) {

            // get the grid points on the mesh
            get_coordinates_2d_higher_order(X, V, j);
            
            // evaluate U at the integration point
            U = C[n*n_p + 0][idx] * sqrt(2.);
            // for (i = 0; i < n_p; i++) {
            //     U += C[n*n_p + i][idx] * basis_higher_order[i * n_quad_higher_order + j];
            // } 

            // get the exact solution
            U_exact(u, X[0], X[1], t);

            // evaluate the L2 error
            e += w[j] * fabs(u[n] - U)  * detJ;
        }
        // printf("%i %f\n",idx, e);
        // store the result 
        error[idx] = e; 
    }
}

__global__ void eval_error_Linfinity(double **C, double *error,
                                           double *V1x, double *V1y,
                                           double *V2x, double *V2y,
                                           double *V3x, double *V3y,
                                           int n, double t,
                                           double *basis_vertex,
                                           int n_p,
                                           int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i, j;
        double e;
        double U;
        double u[4];
        double V[6];

        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        e = 0.;
        for (j = 0; j < 3; j++) {
            U = 0;
            // evaluate U at the integration point
            for (i = 0; i < n_p; i++) {
                U += C[n*n_p + i][idx] * basis_vertex[i * 3 + j];
            } 

            // get the exact solution
            if(j == 0){
              U_exact(u, V[0], V[1], t);
            }
            else if(j == 1){
              U_exact(u, V[2], V[3], t);
            }
            else{
              U_exact(u, V[4], V[5], t);
            }


            // evaluate the L2 error
            e =  (e >  fabs(u[n] - U)) ? e :  fabs(u[n] - U) ;
        }
        // printf("%i %f\n",idx, e);
        // store the result 
        error[idx] = e; 
    }
}


__global__ void eval_error_L1_higher_order(double **C, double *error,
                                           double *V1x, double *V1y,
                                           double *V2x, double *V2y,
                                           double *V3x, double *V3y,
                                           double *J,
                                           int n, double t,
                                           double *w, double *basis_higher_order,
                                           int n_quad_higher_order,
                                           int n_p,
                                           int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i, j;
        double e;
        double X[2];
        double U;
        double u[4];
        double V[6];
        double detJ;

        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        detJ = J[idx];
        e = 0.;
        for (j = 0; j < n_quad_higher_order; j++) {

            // get the grid points on the mesh
            get_coordinates_2d_higher_order(X, V, j);
            
            // evaluate U at the integration point
            U = 0.;
            for (i = 0; i < n_p; i++) {
                U += C[n*n_p + i][idx] * basis_higher_order[i * n_quad_higher_order + j];
            } 

            // get the exact solution
            U_exact(u, X[0], X[1], t);

            // evaluate the L2 error
            e += w[j] * fabs(u[n] - U)  * detJ;
        }
        // printf("%i %f\n",idx, e);
        // store the result 
        error[idx] = e; 
    }
}


__global__ void eval_error_L2_higher_order(double **C, double *error,
                                           double *V1x, double *V1y,
                                           double *V2x, double *V2y,
                                           double *V3x, double *V3y,
                                           double *J,
                                           int n, double t,
                                           double *w, double *basis_higher_order,
                                           int n_quad_higher_order,
                                           int n_p,
                                           int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i, j;
        double e;
        double X[2];
        double U;
        double u[4];
        double V[6];
        double detJ;

        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        detJ = J[idx];
        e = 0.;
        for (j = 0; j < n_quad_higher_order; j++) {

            // get the grid points on the mesh
            get_coordinates_2d_higher_order(X, V, j);
            
            // evaluate U at the integration point
            U = 0.;
            for (i = 0; i < n_p; i++) {
                U += C[n*n_p + i][idx] * basis_higher_order[i * n_quad_higher_order + j];
            } 

            // get the exact solution
            U_exact(u, X[0], X[1], t);

            // evaluate the L2 error
            e += w[j] * (u[n] - U)* (u[n] - U)  * detJ;
        }
        // printf("%i %f\n",idx, e);
        // store the result
        error[idx] = e; 
    }
}


__global__ void eval_error_rhs(double *rhs, double *error,
                               double *length,
                               int *curr_s1, int *curr_s2, int *curr_s3,
                               int *spos, int *d_schild1, int *d_schild2,
                               int *soriginal, 
                               int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int sID[3];
    int haschildren;
    double len;

    if (idx < max) {
      sID[0] = curr_s1[idx];
      sID[1] = curr_s2[idx];
      sID[2] = curr_s3[idx];
      len = 0;

      for(int i = 0; i < 3; i++) {
          haschildren = d_schild1[sID[i]];
          if(haschildren > -1){
            len+=length[soriginal[spos[d_schild1[sID[i]]]]];
            len+=length[soriginal[spos[d_schild2[sID[i]]]]];
          }
          else{
              len+= length[soriginal[spos[ sID[i] ]]];
          }
      }
      error[idx] = fabs(rhs[idx])/len;

    }
}


/* left & right evaluator
 * 
 * calculate U_left and U_right at the integration point,
 * using boundary conditions when necessary.
 */
 __device__ void eval_left_right(double **C, double *C_left, double *C_right, 
                             double *U_left, double *U_right,
                             int j, 
                             int left_side, int right_side,
                             int left_idx, int right_idx,
                             double *basis_side,
                             int n, int n_p, int n_quad1d) { 

    int i;

    // set U to 0
      *U_left  = 0.;
      *U_right = 0.;

    //evaluate U at the integration points
    for (i = 0; i < n_p; i++) {
      *U_left += C_left[i] * 
                   basis_side[left_side * n_p * n_quad1d + i * n_quad1d + j];
    }

    if (right_idx < 0) {
        // evaluate the right side at the integration point
        for (i = 0; i < n_p; i++) {
          *U_right = *U_left; //prob not the best...
        }
    } else {
        // evaluate the right side at the integration point
        for (i = 0; i < n_p; i++) {
          *U_right += C_right[i] * 
                        basis_side[right_side * n_p * n_quad1d + i * n_quad1d + n_quad1d - j - 1];
        }
    }
}

__global__ void eval_jumps(double **C, 
                           int *curr_side, int *original_side,
                           int *slevel,
                           double *side_len, 
                           int *left_elem,  int *right_elem,
                           int *left_side_number, int *right_side_number,
                           double *basis_side, double *w_oned, int n_quad1d,
                           int n_p, int n,
                           double *detector, double *length, 
                           int csum, int max) {
    int pow2[18];
    pow2[0] = 1; pow2[1] = 2; pow2[2] = 4; pow2[3] = 8; pow2[4] = 16; pow2[5] = 32; pow2[6] = 64; pow2[7] = 128; pow2[8] = 256; pow2[9] = 512;
    pow2[10] = 1024; pow2[11] = 2048; pow2[12] = 4096; pow2[13] = 8192; pow2[14] = 16384; pow2[15] = 32768; pow2[16] = 65536; pow2[17] = 131072;

    int idx = blockDim.x * blockIdx.x + threadIdx.x + csum;
    if (idx < max + csum) {
        int i, j;
        int level;
        int left_idx, right_idx, left_side, right_side;
        int soriginal_sideID, sID;
        double len, nx, ny;
        double C_left [NP_MAX];
        double C_right[NP_MAX];
        double U_left, U_right;

        // read edge data
        sID = curr_side[idx];
        level = slevel[idx];
        soriginal_sideID = original_side[idx];
        len = side_len[soriginal_sideID] / pow2[level]; 

        left_idx   = left_elem[idx];
        right_idx  = right_elem[idx];
        left_side  = left_side_number[idx];
        right_side = right_side_number[idx];

        // read coefficients
        if (right_idx > -1) {
          for (i = 0; i < n_p; i++) {
              C_left[i]  = C[n*n_p + i][left_idx];
              C_right[i] = C[n*n_p + i][right_idx];
          }
        } else {
         for (i = 0; i < n_p; i++) {
              C_left[i]  = C[n*n_p + i][left_idx];
          }
        } 
        

        length[left_idx]  += len;
        if(right_idx > -1)
          length[right_idx]  += len;

        // at each integration point
        for (j = 0; j < n_quad1d; j++) {

            // calculate the left and right values along the surface
            eval_left_right(C, C_left, C_right,
                            &U_left, &U_right,
                            j, left_side, right_side,
                            left_idx, right_idx,basis_side, n, n_p, n_quad1d);

            detector[left_idx]  += len / 2. * w_oned[j] * abs(U_left - U_right);
            if(right_idx > -1)
              detector[right_idx] +=  len / 2. * w_oned[j] * abs(U_left - U_right);

              // printf("%i %lf %lf %lf\n", idx, detector[left_idx], detector[right_idx], len);
        }
    }
}
__global__ void eval_max(double *error_estimate, double **C, int max_idx) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x ;
    if (idx < max_idx ) {
      error_estimate[idx] = max(abs(C[1][idx]), abs(C[2][idx]));
    }
}
void evaluate_max(double *d_error_estimate,double **d_curr_c, int curr_num_elem){
    int n_threads = 512;
    eval_max<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_error_estimate,d_curr_c, curr_num_elem);
}

void evaluate_jumps(double *d_error_estimate, 
                    double **d_curr_c, double *d_J,
                    int *d_curr_side, int *d_original_side,
                    int *d_slevel,double *d_slength, 
                    int *d_left_elem, int *d_right_elem,
                    int *lsn, int *rsn,
                    int *num_color, int curr_num_elem, int curr_num_sides){

    int n_threads = 512;
    int csum = 0;


    set_value<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_length, 0., curr_num_elem);
    set_value<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_error_estimate, 0., curr_num_elem);
    for(int k = 0; k < 6; k++) {
        if(num_color[k] > 0){
          eval_jumps<<<get_blocks(num_color[k], n_threads), n_threads>>>(d_curr_c, 
                                                                         d_curr_side, d_original_side,
                                                                         d_slevel,d_slength, 
                                                                         d_left_elem,  d_right_elem,
                                                                         lsn, rsn,
                                                                         d_basis_side, d_w_oned, n_quad1d,
                                                                         local_np, 0,
                                                                         d_error_estimate, d_length, 
                                                                         csum, num_color[k]);
              csum+=num_color[k];
        }
    }

}







void evaluate_error_l2(double **C, double *d_error_estimate, 
                       double *V1x, double *V1y,
                       double *V2x, double *V2y,
                       double *V3x, double *V3y,
                       double *J,
                       int n, double t,
                       int max){ 
    int n_threads = 512;
    eval_error_L2_higher_order<<<get_blocks(max, n_threads), n_threads>>>(C, d_error_estimate,
                                                                          V1x, V1y,
                                                                          V2x, V2y,
                                                                          V3x, V3y,
                                                                          J,
                                                                          n, t,
                                                                          d_w, d_basis_higher_order, n_quad_higher_order,
                                                                          local_np,
                                                                          max);
}











void evaluate_error_l1(double **C, double *d_error_estimate, 
                       double *V1x, double *V1y,
                       double *V2x, double *V2y,
                       double *V3x, double *V3y,
                       double *J,
                       int n, double t,
                       int max){
    int n_threads = 512;
    eval_error_L1_higher_order<<<get_blocks(max, n_threads), n_threads>>>(C, d_error_estimate,
                                                                          V1x, V1y,
                                                                          V2x, V2y,
                                                                          V3x, V3y,
                                                                          J,
                                                                          n, t,
                                                                          d_w, d_basis_higher_order, n_quad_higher_order,
                                                                          local_np,
                                                                          max);
}




void evaluate_error_l1_m(double **C, double *d_error_estimate, 
                       double *V1x, double *V1y,
                       double *V2x, double *V2y,
                       double *V3x, double *V3y,
                       double *J,
                       int n, double t,
                       int max){
    int n_threads = 512;
    eval_error_L1_higher_order_m<<<get_blocks(max, n_threads), n_threads>>>(C, d_error_estimate,
                                                                          V1x, V1y,
                                                                          V2x, V2y,
                                                                          V3x, V3y,
                                                                          J,
                                                                          n, t,
                                                                          d_w, d_basis_higher_order, n_quad_higher_order,
                                                                          local_np,
                                                                          max);
}

void evaluate_error_linfinity(double **C, double *d_error_estimate, 
                       double *V1x, double *V1y,
                       double *V2x, double *V2y,
                       double *V3x, double *V3y,
                       int n, double t,
                       int max){
    int n_threads = 512;
    eval_error_Linfinity<<<get_blocks(max, n_threads), n_threads>>>(C, d_error_estimate,
                                                                          V1x, V1y,
                                                                          V2x, V2y,
                                                                          V3x, V3y,
                                                                          n, t,
                                                                          d_basis_vertex,
                                                                          local_np,
                                                                          max);
}


void evaluate_error_rhs(double *d_error_estimate, 
                        double *d_length,
                        int *d_curr_s1, int *d_curr_s2, int *d_curr_s3,
                        int *d_spos, int *d_schild1, int *d_schild2,
                        int *d_soriginal,
                        int power,
                        int max){
    int n_threads = 512;
    int index = (power + 1)*(power + 2)/2    - 1;
    absolute_value<<<get_blocks(max, n_threads), n_threads>>>(d_error_estimate, h_rhs[index], max);
    // cudaMemcpy(d_error_estimate, h_rhs[index], max*sizeof(double), cudaMemcpyDeviceToDevice);
    // eval_error_rhs<<<get_blocks(max, n_threads), n_threads>>>(h_rhs[index], d_error_estimate,
    //                                                           d_length,
    //                                                           d_curr_s1, d_curr_s2, d_curr_s3,
    //                                                           d_spos, d_schild1, d_schild2,
    //                                                           d_soriginal,
    //                                                           max);

}


void evaluate_gradient(double *d_error_estimate, 
                       double **d_curr_c, 
                       double *d_curr_xr, double *d_curr_xs,
                       double *d_curr_yr, double *d_curr_ys,
                       double *d_curr_J,
                       int n_p, int n_quad,
                       double *d_w, double *d_basis_grad_x, double *d_basis_grad_y,
                       double *d_circles,
                       int curr_num_elem){
    int n_threads = 512;
     eval_gradient<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_error_estimate, 
                                                                        d_curr_c, 
                                                                        d_curr_xr, d_curr_xs,
                                                                        d_curr_yr, d_curr_ys,
                                                                        d_curr_J,
                                                                        n_p,n_quad,
                                                                        d_w, d_basis_grad_x, d_basis_grad_y,
                                                                        curr_num_elem);  
    
    scale<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_error_estimate,d_circles,1., curr_num_elem);  
} 


void evaluate_gradient_trumpet(double *d_error_estimate, 
                       double **d_curr_c, 
                       double *V1x, double *V1y,
                       double *V2x, double *V2y,
                       double *V3x, double *V3y,
                       double *d_curr_xr, double *d_curr_xs,
                       double *d_curr_yr, double *d_curr_ys,
                       double *d_curr_J,
                       int n_p, int n_quad,
                       double *d_w, double *d_basis_grad_x, double *d_basis_grad_y,
                       double *d_circles,
                       int curr_num_elem){
    int n_threads = 512;
     eval_gradient<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_error_estimate, 
                                                                        d_curr_c, 
                                                                        V1x, V1y,
                                                                        V2x, V2y,
                                                                        V3x, V3y,
                                                                        d_curr_xr, d_curr_xs,
                                                                        d_curr_yr, d_curr_ys,
                                                                        d_curr_J,
                                                                        n_p,n_quad,
                                                                        d_w, d_basis_grad_x, d_basis_grad_y,
                                                                        curr_num_elem);  
    
    scale<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_error_estimate,d_circles,approx_order+3, curr_num_elem);  
}


 __global__ void detector(int *flag, double *error, double *circles, double order, int max){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < max) {
        flag[idx] = ( error[idx] * pow(circles[idx], -order) ) > 1 ;
    }
}

 __global__ void detector(int *flag,double *detector, double *error, double *circles, double order, int max){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < max) {
        // flag[idx] = ( error[idx] * pow(circles[idx], -order) ) > 3e2  ;  // max_level = 6, JUMPS
        flag[idx] = ( error[idx] * pow(circles[idx], -order) ) > 3e2  ; 
        detector[idx] = error[idx] * pow(circles[idx], -order)  ;
    }
}

void shock_detector(int *d_flag, double *d_error_estimate, double *d_circles, double order, int max) {
  int n_threads = 512;
  detector<<<get_blocks(max, n_threads), n_threads>>>(d_flag, d_error_estimate, d_circles, order, max);
}

void shock_detector(int *d_flag, double *d_detector, double *d_error_estimate, double *d_circles, double order, int max) {
  int n_threads = 512;
  detector<<<get_blocks(max, n_threads), n_threads>>>(d_flag,d_detector, d_error_estimate, d_circles, order, max);
}



void write_limit(int curr_num_elem,
                 double *d_detector,
                 double *d_curr_V1x, double *d_curr_V1y,
                 double *d_curr_V2x, double *d_curr_V2y,
                 double *d_curr_V3x, double *d_curr_V3y) {
    double *limit;
    double *V1x, *V1y, *V2x, *V2y, *V3x, *V3y;
    int i, n;
    int n_threads     = 512;
    int n_blocks_elem = (curr_num_elem  / n_threads) + ((curr_num_elem  % n_threads) ? 1 : 0);
    FILE *out_file;
    char out_filename[100];

    // evaluate at the vertex points and copy over data
    limit = (double *) malloc(curr_num_elem * sizeof(double));

    V1x = (double *) malloc(curr_num_elem * sizeof(double));
    V1y = (double *) malloc(curr_num_elem * sizeof(double));
    V2x = (double *) malloc(curr_num_elem * sizeof(double));
    V2y = (double *) malloc(curr_num_elem * sizeof(double));
    V3x = (double *) malloc(curr_num_elem * sizeof(double));
    V3y = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(V1x, d_curr_V1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V1y, d_curr_V1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2x, d_curr_V2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V2y, d_curr_V2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3x, d_curr_V3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V3y, d_curr_V3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    // evaluate and write to file
      cudaMemcpy(limit, d_detector, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

      out_file  = fopen("output/limit1.pos" , "w");
      fprintf(out_file, "View \"U0 \" {\n");
      for (i = 0; i < curr_num_elem; i++) {
          fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.15lf,%.15lf,%.15lf};\n", 
                                 V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
                                 limit[i] , limit[i] , limit[i] );
      }
      fprintf(out_file,"};");
      fclose(out_file);


      // out_file  = fopen("output/limit2.pos" , "w");
      // fprintf(out_file, "View \"U0\" {\n");
      // for (i = 0; i < curr_num_elem; i++) {
      //     fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%i,%i,%i};\n", 
      //                            V1x[i], V1y[i], V2x[i], V2y[i], V3x[i], V3y[i],
      //                            limit[i] > 10., limit[i] > 10., limit[i] > 10.);
      // }
      // fprintf(out_file,"};");
      // fclose(out_file);

    free(limit);
    free(V1x);
    free(V1y);
    free(V2x);
    free(V2y);
    free(V3x);
    free(V3y);
}






// __global__ void plot_error_l2(double **C, 
//                               double *V1x, double *V1y,
//                               double *V2x, double *V2y,
//                               double *V3x, double *V3y,
//                               double *Uv1, double *Uv2, double *Uv3, 
//                               double *J,
//                               double t, int n,
//                               int max) {
//     int idx = blockIdx.x * blockDim.x + threadIdx.x;
//     int i, j;
//     double e;
//     double X[2];
//     double U;
//     double u[4];
//     double V[6];
//     double detJ;
//     if (idx < max) {


//         V[0] = V1x[idx];
//         V[1] = V1y[idx];
//         V[2] = V2x[idx];
//         V[3] = V2y[idx];
//         V[4] = V3x[idx];
//         V[5] = V3y[idx];

//         detJ = J[idx];
//         e = 0.;
//         for (j = 0; j < n_quad; j++) {

//             // get the grid points on the mesh
//             get_coordinates_2d(X, V, j);
            
//             // evaluate U at the integration point
//             U = 0.;
//             for (i = 0; i < n_p; i++) {
//                 U += C[n*n_p + i][idx] * basis[i * n_quad + j];
//             } 

//             // get the exact solution
//             U_exact(u, X[0], X[1], t);

//             // evaluate the L2 error
//             e += w[j] * (u[n] - U) * (u[n] - U) * detJ;
//         }
//         // printf("%i %f\n",idx, e);
//         // store the result
//         Uv1[idx] = e;
//         Uv2[idx] = e;
//         Uv3[idx] = e;
//     }
// }

