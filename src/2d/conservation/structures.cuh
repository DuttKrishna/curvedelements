class memoryCounters{

	public:
		size_t etree_memory, stree_memory;
		size_t emesh_memory, smesh_memory;
		size_t ebuffer_memory, sbuffer_memory;
		size_t integration_memory;
		size_t geometry_memory;
	    size_t miscellaneous_memory;
	    size_t visualization_memory;
	    size_t indicator;

	    memoryCounters();
	    size_t total();
};