#include <stdio.h>
#include "./cub-1.5.1/cub/cub.cuh"
using namespace cub;

__device__ int *outflag;
__device__ int *result;
__device__ double *result2;
__device__ void *d_temp_storage;


size_t temp_storage_bytes;

void allocate( void **var, size_t size, size_t *memory_counter) {
    cudaMalloc(var, size); 
    *memory_counter+=size;
}


void write_array(int *array, char *num, int max) {
    FILE *out_file;
    char out_filename[100];

    // evaluate at the vertex points and copy over data
    int *localarray = (int *) malloc(max * sizeof(int));
    cudaMemcpy(localarray, array, max * sizeof(int), cudaMemcpyDeviceToHost);

    sprintf(out_filename, "output/%s.txt", num);
    out_file  = fopen(out_filename , "w");
    for (int i = 0; i < max; i++) {
        fprintf(out_file, "%i ", localarray[i]);
    }
    fclose(out_file);
    free(localarray);
}

void write_array(double *array, char *num, int max) {
    FILE *out_file;
    char out_filename[100];

    // evaluate at the vertex points and copy over data
    double *localarray = (double *) malloc(max * sizeof(double));
    cudaMemcpy(localarray, array, max * sizeof(double), cudaMemcpyDeviceToHost);

    sprintf(out_filename, "output/%s.txt", num);
    out_file  = fopen(out_filename , "w");
    for (int i = 0; i < max; i++) {
        fprintf(out_file, "%f \n", localarray[i]);
    }
    fclose(out_file);
    free(localarray);
}

void print_array( int *array, int length) {
    int *temp = (int *) malloc(length * sizeof(int));
    cudaMemcpy(temp, array, length * sizeof(int), cudaMemcpyDeviceToHost);
    for(int i = 0; i < length; i++)
        printf("%i ", temp[i]);
    printf("\n");

    free(temp);
}
void print_array( double *array, int length) {
    double *temp = (double *) malloc(length * sizeof(double));
    cudaMemcpy(temp, array, length * sizeof(double), cudaMemcpyDeviceToHost);
    for(int i = 0; i < length; i++)
        printf("%lf ",i, temp[i]);
    printf("\n");

    free(temp);
}

void print_array( float *array, int length) {
    float *temp = (float *) malloc(length * sizeof(float));
    cudaMemcpy(temp, array, length * sizeof(float), cudaMemcpyDeviceToHost);
    for(int i = 0; i < length; i++)
        printf("%lf ", temp[i]);
    printf("\n");

    free(temp);
}


void quit(){
    cudaThreadSynchronize(); exit(-1);
}

int get_blocks(int num, int threads) {
    return (num  / threads) + ((num  % threads) ? 1 : 0);
}

int get_num_valid(int *iflag, int *oflag, int max) {
    int last_flag=0, final_sum=0;
    if(max > 0){
        cudaMemcpy(&last_flag, iflag + max - 1, sizeof(int), cudaMemcpyDeviceToHost);
        cudaMemcpy(&final_sum, oflag + max - 1, sizeof(int), cudaMemcpyDeviceToHost);
    }
    return final_sum + last_flag;
}

void check_max(int compare, int max, int *track, char *s) {
    if(*track < compare)
        *track = compare;

    if(compare > max){
        printf("\nRun out of memory : %s (%i / %i)\n", s, compare, max);
        quit();
    }
}




void swap(int **in1, int **in2) {
    int *temp;

    temp = *in2;
    *in2 = *in1;
    *in1 = temp;
}

void swap(double **in1, double **in2) {
    double *temp;

    temp = *in2;
    *in2 = *in1;
    *in1 = temp;
}


void write_C(double *d_curr_c,
             int local_N, int curr_num_elem, int local_n_p) {
    int idx;
    FILE *out_file;
    char out_filename[100];

    // copy over coefficients
    double *c = (double *) malloc(curr_num_elem * local_n_p * local_N * sizeof(double));
    cudaMemcpy(c, d_curr_c, curr_num_elem * local_n_p * local_N * sizeof(double), cudaMemcpyDeviceToHost);

    // write to file
    sprintf(out_filename, "output/C.txt");
    out_file  = fopen(out_filename , "w");
    for (idx = 0; idx < curr_num_elem * local_N * local_n_p; idx++) {
        fprintf(out_file, "%lf\n", c[idx]);
    }

    // printf("ELMENT 0'S COEFFICIENTS ARE: %f %f %f\n", c[0], c[curr_num_elem], c[2*curr_num_elem]);
    // close file and free c
    fclose(out_file);
    free(c);
}

__global__ void set_value(int *array, int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if(idx < max) {
        array[idx] = idx;
    }
}

__global__ void set_value(int *array, int val, int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

	if(idx < max) {
		array[idx] = val;
	}
}

__global__ void set_value(int *array, int *iflag, int val, int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if(idx < max) {
        if(iflag[idx])
            array[idx] = val;
    }
}
__global__ void set_value(double *array, int *iflag, double val, int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if(idx < max) {
        if(iflag[idx])
            array[idx] = val;
    }
}
__global__ void set_value(double *array, double val, int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if(idx < max) {
        array[idx] = val;
    }
}

__global__ void set_value(int *curr, int *pos, int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int ID;
    if(idx < max) {
        ID = curr[idx];
        pos[ID] = -1;
    }
}

__global__ void is_l(int *dr, int *iflag,
                         int num, int max ) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if(idx  < max) { 
        iflag[ idx ] = dr[idx] < num ; 
    }
}

__global__ void is_g(int *dr, int *iflag,
                           int num, int max ) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if(idx  < max) { 
        iflag[ idx ] = dr[idx] > num ;
    }
}

__global__ void is_e(int *in, int *iflag,
                         int num, int max ) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if(idx  < max) { 
        iflag[ idx ] = in[idx] == num ;
    }
}

__global__ void is_neq(int *in, int *iflag,
                         int num, int max ) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if(idx  < max) { 
        iflag[ idx ] = in[idx] != num ;
    } 
}

__global__ void is_geq(int *dr, int *iflag,
                           int num, int max ) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if(idx  < max) { 
        iflag[ idx ] = dr[idx] >= num ;
    }
}


__global__ void compaction(int *in, int *out, int *iflag, int *oflag, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) { 
        if(iflag[idx])
            out[oflag[idx]] = in[idx]; 
    }
}

__global__ void compaction(double *in, double *out, int *iflag, int *oflag, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        if(iflag[idx])
            out[oflag[idx]] = in[idx]; 
    }
}

__global__ void compaction(int *out, int *iflag, int *oflag, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        if(iflag[idx])
            out[oflag[idx]] = idx;
    }
}

__global__ void compaction(int *out, int *iflag, int *oflag, int offset, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        if(iflag[idx])
            out[oflag[idx]] = idx + offset;
    }
}

__global__ void compaction_level(int *in, int *out, int *iflag, int *oflag, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        if(iflag[idx])
            out[oflag[idx]] = in[idx] - 1;
    }
}

__global__ void is_neq(int *in1, int *in2, int *iflag, int max ) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if(idx  < max) { 
        iflag[ idx ] = in1[idx] != in2[idx]  ;
    } 
}
 


void allocateCompaction(int max){
    temp_storage_bytes = 0;
    CubDebugExit(DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes, result, outflag, max));
    cudaMalloc( (void **) &d_temp_storage, temp_storage_bytes);
    cudaMalloc((void **) &outflag, max * sizeof(int));
    cudaMalloc((void **) &result, max * sizeof(int));
    cudaMalloc((void **) &result2, max * sizeof(double));
}

void compaction(int *in, int *inflag, int max, int *new_size){
    CubDebugExit(DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes, inflag, outflag, max));

    *new_size = get_num_valid(inflag, outflag, max); 
    if(*new_size > 0){
        compaction<<<get_blocks(max,512), 512>>>(in, result, inflag, outflag, max); 
        cudaMemcpy(in, result, *new_size * sizeof(int), cudaMemcpyDeviceToDevice);
    }
}

void compaction(double *in, int *inflag, int max, int *new_size){
    CubDebugExit(DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes, inflag, outflag, max));

    *new_size = get_num_valid(inflag, outflag, max); 
    if(*new_size > 0){
        compaction<<<get_blocks(max,512), 512>>>(in, result2, inflag, outflag, max); 
        cudaMemcpy(in, result2, *new_size * sizeof(double), cudaMemcpyDeviceToDevice);
    }
}


void compaction(int *out, int *in, int *inflag, int max, int *new_size){
    CubDebugExit(DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes, inflag, outflag, max));
    *new_size = get_num_valid(inflag, outflag, max); 
    if(*new_size > 0)
        compaction<<<get_blocks(max,512), 512>>>(in, out, inflag, outflag, max); 
}



void position_compaction(int *out,int *inflag, int max, int *new_size){
    CubDebugExit(DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes, inflag, outflag, max));
    *new_size = get_num_valid(inflag, outflag, max); 
    if(*new_size > 0)
        compaction<<<get_blocks(max,512), 512>>>(out, inflag, outflag, max); 
}

void position_compaction_offset(int *out,int *inflag, int offset, int max, int *new_size){
    CubDebugExit(DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes, inflag, outflag, max));
    *new_size = get_num_valid(inflag, outflag, max); 
    if(*new_size > 0)
        compaction<<<get_blocks(max,512), 512>>>(out, inflag, outflag, offset, max); 
}

// __global__ void is_e(int *in, int *iflag,int num, int max );


void in_place_compaction_init(int *inflag, int *to, int *from, int max, int *work_size, int *new_size){
    CubDebugExit(DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes, inflag, outflag, max));
    int head_size, tail_size;
    head_size = get_num_valid(inflag, outflag, max);
    tail_size = max - head_size;

    //[1 0 0 0 1][0 1 0 1 0 1 0 ]
    //   head       tail

    // reduce free positions in head
    is_e<<<get_blocks(head_size,512), 512>>>(inflag, result, 0 , head_size); 
    if(head_size > 0)
        position_compaction(to, result, head_size, work_size); 
    // reduce used positions in tail
    if(tail_size > 0)
        position_compaction_offset(from, inflag+head_size, head_size, tail_size, work_size); 
}

__global__  void in_place_compaction_k(double *data, int *to, int* from, int work_size){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < work_size) {
        data[to[idx]] = data[from[idx]];
    }
}
__global__  void in_place_compaction_k(int *data, int *to, int* from, int work_size){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < work_size) {
        data[to[idx]] = data[from[idx]];
    }
}
void in_place_compaction(double *data, int *to, int* from, int work_size){
    if(work_size > 0){
        in_place_compaction_k<<<get_blocks(work_size,512), 512>>>(data, to, from, work_size); 
    }
}
void in_place_compaction(int *data, int *to, int* from, int work_size){
    if(work_size > 0){
        in_place_compaction_k<<<get_blocks(work_size,512), 512>>>(data, to, from, work_size); 
    }
}



__global__  void write_idx(int *data, int work_size){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < work_size) {
        data[idx] = idx;
    }
}


__global__  void move_to(int *out_data, int* in_data, int *to, int work_size){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < work_size) {
        out_data[to[idx]] = in_data[idx];
    }
}


__global__  void find_duplicates(int* in_data, int *flag, int work_size){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < work_size-1) {
        if(in_data[idx] != in_data[idx+1] && in_data[idx] != -1)
            flag[idx] = 1;
        else
            flag[idx] = 0;

    }
    else if(idx == work_size-1){
        if(in_data[idx] != -1)
            flag[idx] = 1;
        else
            flag[idx] = 0;
    }
}

__global__  void is_neq_array(int* a, int *b, int *flag, int size){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < size) {
        flag[idx] = a[idx] != b[idx];
    }
}

int isdifferent(int *a, int *b, int size){
    is_neq_array<<<get_blocks(size,512), 512>>>(a, b, result, size);
    CubDebugExit(DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes, result, outflag, size));
    int num = get_num_valid(result, outflag, size); 

    return num > 0;
}


__global__ void ordered_copy_kernel(int *out, int *in, int *pos, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < max) {
        if(pos[idx] > -1)
        out[ pos[idx] ] = in[idx];
    }
} 

void count(int *in, int num, int max, int *new_size) {
    is_e<<<get_blocks(max,512), 512>>>(in, result, num, max);
    CubDebugExit(DeviceScan::ExclusiveSum(d_temp_storage, temp_storage_bytes, result, outflag, max));
    *new_size = get_num_valid(result, outflag, max); 
}



void ordered_copy(int *out, int *pos, int max) {
    ordered_copy_kernel<<<get_blocks(max, 512), 512>>>(result, out, pos, max);
    cudaMemcpy(out, result, max*sizeof(int), cudaMemcpyDeviceToDevice);
}













 __global__ void set_ordering(int *iflag, int *oflag, int *ordering, int offset, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        if(iflag[idx] == 1) {
            ordering[idx] = oflag[idx] + offset;
        }
    }
} 



 __global__ void scale(double *error, double *scale, int order, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        error[idx] = error[idx] * pow(scale[idx], order)  ;
    }
}

 __global__ void divide(double *error, double *scale, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        error[idx] = scale[idx] /error[idx]   ;
    }
}

 __global__ void divide2(double *error, double *scale, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        error[idx] = error[idx] / scale[idx]  ;
    }
}

 __global__ void divide(double *out, double *a, double *b, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        out[idx] = a[idx] / b[idx]  ;
    }
}

 __global__ void multiply(double *error, double *scale, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        error[idx] = error[idx] * scale[idx]  ;
    }
}
 __global__ void multiply(double *out_error,double *in_error, double scale, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        out_error[idx] = in_error[idx] * scale ;
    }
}
 __global__ void reverse(double *out,double *in, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        out[idx] = in[max-idx-1] ;
    }
}
 __global__ void scale(int *flag, double *error, double *circles, int order, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        if(flag[idx] == 1)
            error[idx] = error[idx] * pow(circles[idx], order)  ;
    }
}

 __global__ void absolute_value(double *error, double *in, int max){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < max) {
        error[idx] = abs(in[idx]) ;
    }
}


