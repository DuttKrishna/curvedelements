#include <stdio.h>
#include "memoryCounters.cuh"
#include "operations.cuh"
#include "limiter.cuh"

#define N_MAX 4
#define NP_MAX 25
#define BOUNDARY_FACTOR 1.

#define ACTIVATED 0


void write_limit(int curr_num_elem, int num, int total_timesteps);


__device__ int *d_activated;
__device__ double *d_value1;
__device__ double *d_value2;



// modal limiter
__device__ int *d_kp11, *d_kp12;
__device__ int *d_km11, *d_km12;
__device__ double *d_betap11, *d_betap12;
__device__ double *d_betam11, *d_betam12;
__device__ int *d_kp21, *d_kp22;
__device__ int *d_km21, *d_km22;
__device__ double *d_betap21, *d_betap22;
__device__ double *d_betam21, *d_betam22;
__device__ double *d_gammap1, *d_gammap2;
__device__ double *d_gammam1, *d_gammam2;
__device__ double *d_d1p, *d_d1m;
__device__ double *d_d2p, *d_d2m;
__device__ double *d_h;


// cockburn's limiter
__device__ double *d_circ;
__device__ int *d_e11; 
__device__ int *d_e12; 
__device__ int *d_e21; 
__device__ int *d_e22; 
__device__ int *d_e31; 
__device__ int *d_e32; 
__device__ double *d_a11;
__device__ double *d_a12;
__device__ double *d_a21;
__device__ double *d_a22;
__device__ double *d_a31;
__device__ double *d_a32;

// BJ limiter
__device__ int **d_neighbours;
__device__ int *h_neighbours[20];

// mesh variables
int *s1; int *s2; int *s3;
int *le; int *re;
int *lsn; int *rsn;
double *vert1x; double *vert1y;
double *vert2x; double *vert2y;
double *vert3x; double *vert3y;

// basis function variables
__device__ double *d_midpoint; 
__device__ double *d_vertex; 
__device__ double *basis_values;
__device__ static double *d_basis_side;
__device__ double *weights;

//tree variables
__device__ int *spos, *schild1, *schild2;


int limiter_type;
int N_eq, N_poly, n_quadrature;
static int n_quad1d;

int num_elem0; int num_sides0;
int num_threads = 512;



__global__ void init_bj_h(double *h,
                          double *V1x, double *V1y,
                          double *V2x, double *V2y,
                          double *V3x, double *V3y,
                          int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_elem) {
        double x1, y1, x2, y2, x3, y3, J, A, P, l1, l2, l3;

        // read vertex points
        x1 = V1x[idx];
        y1 = V1y[idx];
        x2 = V2x[idx];
        y2 = V2y[idx];
        x3 = V3x[idx];
        y3 = V3y[idx];

        // calculate jacobian determinant
        // x = x2 * r + x3 * s + x1 * (1 - r - s)
        J = (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);
        A = J/2.;

        l1 = sqrt(pow(x2-x1, 2.) + pow(y2-y1, 2.));
        l2 = sqrt(pow(x3-x2, 2.) + pow(y3-y2, 2.));
        l3 = sqrt(pow(x1-x3, 2.) + pow(y1-y3, 2.));

        h[idx] = 2*A / (l1 + l2 + l3);
    }
}


__global__ void init_lbj_h(double *h,
                          double *V1x, double *V1y,
                          double *V2x, double *V2y,
                          double *V3x, double *V3y,
                          int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_elem) {
        double x1, y1, x2, y2, x3, y3, l1, l2, l3;
        double n1x, n1y, n2x, n2y, n3x, n3y;
        double dir_x = sqrt(2.)/2., dir_y = sqrt(2.)/2.; 
        // double dir_x = 0.82903757, dir_y = 0.55919290; 
        // double dir_x = 1., dir_y = 0.; 
        double len1, len2, len3, J, A, theta12, theta13, theta23, theta1a, theta2a, theta3a, H; 

        // read vertex points
        x1 = V1x[idx];
        y1 = V1y[idx]; 
        x2 = V2x[idx];
        y2 = V2y[idx]; 
        x3 = V3x[idx];
        y3 = V3y[idx];

        J = (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);
        A = (J/2.) ; 


        len1 = sqrt(pow(x2-x1, 2.) + pow(y2-y1, 2.));
        len2 = sqrt(pow(x3-x2, 2.) + pow(y3-y2, 2.));
        len3 = sqrt(pow(x1-x3, 2.) + pow(y1-y3, 2.));

        // l1 = sqrt(pow((x1+x2)/2.-x3, 2) +  pow((y1+y2)/2.-y3, 2));
        // l2 = sqrt(pow((x2+x3)/2.-x1, 2) +  pow((y2+y3)/2.-y1, 2));
        // l3 = sqrt(pow((x3+x1)/2.-x2, 2) +  pow((y3+y1)/2.-y2, 2));

        // lengths of the vector components
        n1x = y2 - y1;
        n1y = -(x2 - x1); 

        n2x = y3 - y2; 
        n2y = -(x3 - x2);

        n3x = y1 - y3; 
        n3y = -(x1 - x3);

        // normalize
        l1 = sqrt(pow(n1x, 2) + pow(n1y, 2));
        l2 = sqrt(pow(n2x, 2) + pow(n2y, 2)); 
        l3 = sqrt(pow(n3x, 2) + pow(n3y, 2));
        n1x /= l1;
        n1y /= l1;
        n2x /= l2;
        n2y /= l2;
        n3x /= l3;
        n3y /= l3;
        // printf("%lf %lf, %lf %lf, %lf %lf\n", n1x, n1y, n2x, n2y, n3x, n3y);

 
        // 1   // a
        //.   //
        //   //
        //  //
        // //
        // // // // // 2 

        double temp , min;
        H = 2*A/len3;
        min = H / fabs(dir_x * n3x + dir_y * n3y); 

        H = 2*A/len1;
        temp = H / fabs(dir_x * n1x + dir_y * n1y);
        if( temp < min ){
            min = temp;
        }

        H = 2*A/len2;
        temp = H / fabs(dir_x * n2x + dir_y * n2y);
        if( temp < min ){
            min = temp;
        }
        h[idx] = min;
        // is gradient between 1 and 2
        // theta12 = ((x1-x2) * (x3-x2) + (y1-y2) * (y3-y2))/l1/l2;
        // theta1a = ((x1-x2) * dir_x + (y1-y2) * dir_y)/l1;
        // theta2a = ((x3-x2) * dir_x + (y3-y2) * dir_y)/l2;
        // if( theta12 <= theta1a + 1e-10 && theta12 <= theta2a + 1e-10 && (theta1a > -1e-10 || theta2a > -1e-10)){
        //     H = 2*A/len3;
        //     h[idx] = H / (dir_x * n3x + dir_y * n3y); 
        // }

        // // is gradient between 2 and 3
        // theta23 = ((x2-x3) * (x1-x3) + (y2-y3) * (y1-y3))/l2/l3;
        // theta3a = ((x2-x3) * dir_x   + (y2-y3) * dir_y)/l2;
        // theta2a = ((x1-x3) * dir_x   + (y1-y3) * dir_y)/l3;
        // if( theta23 <= theta2a + 1e-10 && theta23 <= theta3a + 1e-10 && (theta3a > -1e-10 || theta2a > -1e-10)){
        //     H = 2*A/len1;
        //     h[idx] = H / (dir_x * n1x + dir_y * n1y);
        // }

        // // is gradient between 3 and 1
        // theta13 = ((x2-x1) * (x3-x1) + (y2-y1) * (y3-y1))/l1/l3;
        // theta3a = ((x3-x1) * dir_x   + (y3-y1) * dir_y)/l3;
        // theta1a = ((x2-x1) * dir_x   + (y2-y1) * dir_y)/l1;
        // if( theta13 <= theta1a + 1e-10 && theta13 <= theta3a + 1e-10 && (theta1a > -1e-10 || theta3a > -1e-10)){
        //     H = 2*A/len2;
        //     h[idx] = H / (dir_x * n2x + dir_y * n2y);
        // }  

        // dir_x = -dir_x;
        // dir_y = -dir_y;

        // if( theta12 <= theta1a + 1e-10 && theta12 <= theta2a + 1e-10 && (theta1a > -1e-10 || theta2a > -1e-10)){
        //     H = 2*A/len3;
        //     h[idx] = H / (dir_x * n3x + dir_y * n3y); 
        // }

        // // is gradient between 2 and 3
        // theta23 = ((x2-x3) * (x1-x3) + (y2-y3) * (y1-y3))/l2/l3;
        // theta3a = ((x2-x3) * dir_x   + (y2-y3) * dir_y)/l2;
        // theta2a = ((x1-x3) * dir_x   + (y1-y3) * dir_y)/l3;
        // if( theta23 <= theta2a + 1e-10 && theta23 <= theta3a + 1e-10 && (theta3a > -1e-10 || theta2a > -1e-10)){
        //     H = 2*A/len1;
        //     h[idx] = H / (dir_x * n1x + dir_y * n1y);
        // }

        // // is gradient between 3 and 1
        // theta13 = ((x2-x1) * (x3-x1) + (y2-y1) * (y3-y1))/l1/l3;
        // theta3a = ((x3-x1) * dir_x   + (y3-y1) * dir_y)/l3;
        // theta1a = ((x2-x1) * dir_x   + (y2-y1) * dir_y)/l1;
        // if( theta13 <= theta1a + 1e-10 && theta13 <= theta3a + 1e-10 && (theta1a > -1e-10 || theta3a > -1e-10)){
        //     H = 2*A/len2;
        //     h[idx] = H / (dir_x * n2x + dir_y * n2y);
        // }  
// 
    }
}


__global__ void init_nlbj_h(double *h,
                          double *V1x, double *V1y,
                          double *V2x, double *V2y,
                          double *V3x, double *V3y,
                          int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_elem) {
        double a, b, c, k;

        //Twice the area
        k = (V2x[idx]-V1x[idx])*(V3y[idx]-V1y[idx])-(V3x[idx]-V1x[idx])*(V2y[idx]-V1y[idx]);

        //Length of each side
        a = sqrt(pow(V1x[idx] - V2x[idx], 2) + pow(V1y[idx] - V2y[idx], 2));
        b = sqrt(pow(V2x[idx] - V3x[idx], 2) + pow(V2y[idx] - V3y[idx], 2));
        c = sqrt(pow(V1x[idx] - V3x[idx], 2) + pow(V1y[idx] - V3y[idx], 2));

        //Smallest height
        h[idx] = min(min(k/a,k/b),k/c);
    }
}




void allocate_limiter(memoryCounters *counter, int in_num_elem0, int in_num_sides0, int in_limiter_type){
    num_elem0 = in_num_elem0;
	num_sides0 = in_num_sides0;

    limiter_type = in_limiter_type;
    allocate((void **) &d_activated, num_elem0 * sizeof(int), &(counter->limiter_memory));
    allocate((void **) &d_value1, num_elem0 * sizeof(double), &(counter->limiter_memory));
    allocate((void **) &d_value2, num_elem0 * sizeof(double), &(counter->limiter_memory));

    if(limiter_type == MODAL || limiter_type == MOMENT_AM) {
        allocate((void **) &d_kp11, num_elem0 * sizeof(int), &(counter->limiter_memory));
        allocate((void **) &d_kp12, num_elem0 * sizeof(int), &(counter->limiter_memory));

        allocate((void **) &d_km11, num_elem0 * sizeof(int), &(counter->limiter_memory));
        allocate((void **) &d_km12, num_elem0 * sizeof(int), &(counter->limiter_memory));

        allocate((void **) &d_betap11, num_elem0 * sizeof(double), &(counter->limiter_memory));
        allocate((void **) &d_betap12, num_elem0 * sizeof(double), &(counter->limiter_memory));

        allocate((void **) &d_betam11, num_elem0 * sizeof(double), &(counter->limiter_memory));
        allocate((void **) &d_betam12, num_elem0 * sizeof(double), &(counter->limiter_memory));

        allocate((void **) &d_kp21, num_elem0 * sizeof(int), &(counter->limiter_memory));
        allocate((void **) &d_kp22, num_elem0 * sizeof(int), &(counter->limiter_memory));

        allocate((void **) &d_km21, num_elem0 * sizeof(int), &(counter->limiter_memory));
        allocate((void **) &d_km22, num_elem0 * sizeof(int), &(counter->limiter_memory));

        allocate((void **) &d_betap21, num_elem0 * sizeof(double), &(counter->limiter_memory));
        allocate((void **) &d_betap22, num_elem0 * sizeof(double), &(counter->limiter_memory));

        allocate((void **) &d_betam21, num_elem0 * sizeof(double), &(counter->limiter_memory));
        allocate((void **) &d_betam22, num_elem0 * sizeof(double), &(counter->limiter_memory));

        allocate((void **) &d_gammap1, num_elem0 * sizeof(double), &(counter->limiter_memory));
        allocate((void **) &d_gammap2, num_elem0 * sizeof(double), &(counter->limiter_memory));

        allocate((void **) &d_gammam1, num_elem0 * sizeof(double), &(counter->limiter_memory));
        allocate((void **) &d_gammam2, num_elem0 * sizeof(double), &(counter->limiter_memory));

        allocate((void **) &d_d1p, num_elem0 * sizeof(double), &(counter->limiter_memory));
        allocate((void **) &d_d1m, num_elem0 * sizeof(double), &(counter->limiter_memory));

        allocate((void **) &d_d2p, num_elem0 * sizeof(double), &(counter->limiter_memory));
        allocate((void **) &d_d2m, num_elem0 * sizeof(double), &(counter->limiter_memory));

        allocate((void **) &d_h, num_elem0 * sizeof(double), &(counter->limiter_memory));
    }
    else if(limiter_type == COCKBURN) {
        allocate((void **) &d_e11, num_elem0 * sizeof(int), &counter->limiter_memory);
        allocate((void **) &d_e12, num_elem0 * sizeof(int), &counter->limiter_memory);
        allocate((void **) &d_e21 ,num_elem0 * sizeof(int), &counter->limiter_memory);
        allocate((void **) &d_e22 ,num_elem0 * sizeof(int), &counter->limiter_memory);
        allocate((void **) &d_e31, num_elem0 * sizeof(int), &counter->limiter_memory);
        allocate((void **) &d_e32, num_elem0 * sizeof(int), &counter->limiter_memory);

        allocate((void **) &d_a11, num_elem0 * sizeof(double), &counter->limiter_memory);
        allocate((void **) &d_a12, num_elem0 * sizeof(double), &counter->limiter_memory);
        allocate((void **) &d_a21 ,num_elem0 * sizeof(double), &counter->limiter_memory);
        allocate((void **) &d_a22 ,num_elem0 * sizeof(double), &counter->limiter_memory);
        allocate((void **) &d_a31, num_elem0 * sizeof(double), &counter->limiter_memory);
        allocate((void **) &d_a32, num_elem0 * sizeof(double), &counter->limiter_memory);

        allocate((void **) &d_h, num_elem0 * sizeof(double), &(counter->limiter_memory));
    }
    else if(limiter_type == BJ || limiter_type == BJ_N || limiter_type == BJ_QUAD || limiter_type == BJ_R || limiter_type == BJ_QUAD_N || limiter_type == BJ_QUAD_R || limiter_type == NO_LIMITER)
    {
        allocate((void **) &d_h, num_elem0 * sizeof(double), &(counter->limiter_memory));
    }


}



void init_limiter(int curr_num_elem, 
                  double *basis_midpoint, double *basis_vertex, double *in_basis, double *in_weights,
                  double *in_basis_side,
                  double *circles,
                  int *curr_s1, int *curr_s2, int *curr_s3,
                  int *left_elem, int *right_elem,
                  int *left_side_number, int *right_side_number,
                  int *in_spos, int *in_schild1, int *in_schild2,
                  double *V1x, double *V1y,
                  double *V2x, double *V2y,
                  double *V3x, double *V3y,
                  int in_N_eq, int in_N_poly, int in_n_quadrature, int in_n_quad1d,
                  char *limiter_filename){
    d_midpoint = basis_midpoint;
    d_vertex = basis_vertex;
    basis_values = in_basis;
    d_basis_side = in_basis_side;
    weights = in_weights;
    s1 = curr_s1; s2 = curr_s2; s3 = curr_s3;
    le = left_elem; re = right_elem;
    vert1x = V1x; vert1y = V1y;
    vert2x = V2x; vert2y = V2y;
    vert3x = V3x; vert3y = V3y;
    lsn = left_side_number; rsn = right_side_number;
    N_eq = in_N_eq;
    N_poly = in_N_poly;
    n_quadrature = in_n_quadrature;
    n_quad1d = in_n_quad1d; 
    d_circ = circles;

    spos = in_spos,
    schild1 = in_schild1;
    schild2 = in_schild2;


    int items;
    char line[1000];
    FILE *mesh_file;



    if(limiter_type == MODAL || limiter_type == MOMENT_AM) {
        // open the mesh to get curr_num_elem for allocations
        mesh_file = fopen(limiter_filename, "r");
        if (!mesh_file) {
            printf("\nERROR: limiter file not found.\n"); 
            quit();
        }
        // local variables
        int *h_kp11, *h_kp12;
        int *h_km11, *h_km12;
        double *h_betap11, *h_betap12;
        double *h_betam11, *h_betam12;
        int *h_kp21, *h_kp22;
        int *h_km21, *h_km22;
        double *h_betap21, *h_betap22;
        double *h_betam21, *h_betam22;
        double *h_gammap1, *h_gammap2; 
        double *h_gammam1, *h_gammam2;
        double *h_d1p, *h_d1m;
        double *h_d2p, *h_d2m;
        double *h_h;
        // stores the number of sides this element has.
        h_kp11 = (int *) malloc(curr_num_elem * sizeof(int));
        h_kp12 = (int *) malloc(curr_num_elem * sizeof(int));

        h_betap11 = (double *) malloc(curr_num_elem * sizeof(double));
        h_betap12 = (double *) malloc(curr_num_elem * sizeof(double));

        h_km11 = (int *) malloc(curr_num_elem * sizeof(int));
        h_km12 = (int *) malloc(curr_num_elem * sizeof(int));

        h_betam11 = (double *) malloc(curr_num_elem * sizeof(double));
        h_betam12 = (double *) malloc(curr_num_elem * sizeof(double));

        h_kp21 = (int *) malloc(curr_num_elem * sizeof(int));
        h_kp22 = (int *) malloc(curr_num_elem * sizeof(int));

        h_betap21 = (double *) malloc(curr_num_elem * sizeof(double));
        h_betap22 = (double *) malloc(curr_num_elem * sizeof(double));

        h_km21 = (int *) malloc(curr_num_elem * sizeof(int));
        h_km22 = (int *) malloc(curr_num_elem * sizeof(int));

        h_betam21 = (double *) malloc(curr_num_elem * sizeof(double));
        h_betam22 = (double *) malloc(curr_num_elem * sizeof(double));

        h_gammap1 = (double *) malloc(curr_num_elem * sizeof(double));
        h_gammap2 = (double *) malloc(curr_num_elem * sizeof(double));

        h_gammam1 = (double *) malloc(curr_num_elem * sizeof(double));
        h_gammam2 = (double *) malloc(curr_num_elem * sizeof(double));

        h_d1p = (double *) malloc(curr_num_elem * sizeof(double));
        h_d1m = (double *) malloc(curr_num_elem * sizeof(double));

        h_d2p = (double *) malloc(curr_num_elem * sizeof(double));
        h_d2m = (double *) malloc(curr_num_elem * sizeof(double));

        h_h = (double *) malloc(curr_num_elem * sizeof(double));

        // read the elements from the mesh
        for (int i = 0; i < curr_num_elem; i++) {
            fgets(line, sizeof(line), mesh_file);
            items = sscanf(line, "%i %i %lf %lf %i %i %lf %lf %i %i %lf %lf %i %i %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf", 
            								 h_kp11 + i, h_kp12+ i, 
                                             h_betap11 + i, h_betap12+ i, 
            								 h_kp21 + i, h_kp22+ i, 
                                             h_betap21 + i, h_betap22+ i, 
           								     h_km11 + i, h_km12+ i, 
                                             h_betam11 + i, h_betam12+ i, 
            								 h_km21 + i, h_km22+ i, 
                                             h_betam21 + i, h_betam22+ i, 
                                             h_gammap1+ i, h_gammam1+ i,
                                             h_gammap2+ i, h_gammam2+ i,
                                             h_d1p + i, h_d1m +i,
                                             h_d2p + i, h_d2m + i,
                                             h_h + i);

            // printf("%lf %lf\n", h_d1p[i], h_d1m[i]);

            if (items != 25) {
            	h_kp11[i] = -1;
            	h_kp12[i] = -1;
            	h_km11[i] = -1;
            	h_km12[i] = -1;

            	h_kp21[i] = -1;
            	h_kp22[i] = -1;
            	h_km21[i] = -1;
                h_km22[i] = -1;

                items = sscanf(line, "%lf", h_h + i);
            }
        }


        cudaMemcpy(d_kp11,h_kp11, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_kp12,h_kp12, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);

        cudaMemcpy(d_km11,h_km11, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_km12,h_km12, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);

        cudaMemcpy(d_betap11,h_betap11, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_betap12,h_betap12, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);

        cudaMemcpy(d_betam11,h_betam11, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_betam12,h_betam12, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);

        cudaMemcpy(d_kp21,h_kp21, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_kp22,h_kp22, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);

        cudaMemcpy(d_km21,h_km21, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_km22,h_km22, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);

        cudaMemcpy(d_betap21,h_betap21, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_betap22,h_betap22, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);

        cudaMemcpy(d_betam21,h_betam21, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_betam22,h_betam22, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);

        cudaMemcpy(d_gammap1,h_gammap1, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_gammap2,h_gammap2, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);

        cudaMemcpy(d_gammam1,h_gammam1, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_gammam2,h_gammam2, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);

        cudaMemcpy(d_d1p,h_d1p, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_d1m,h_d1m, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);

        cudaMemcpy(d_d2p,h_d2p, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_d2m,h_d2m, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);

        cudaMemcpy(d_h,h_h, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice); 

        free(h_kp11);
        free(h_kp12);

        free(h_km11);
        free(h_km12);

        free(h_betap11);
        free(h_betap12);

        free(h_betam11);
        free(h_betam12);

        free(h_kp21);
        free(h_kp22);

        free(h_km21);
        free(h_km22);

        free(h_betap21);
        free(h_betap22);

        free(h_betam21);
        free(h_betam22);

        free(h_gammap1);
        free(h_gammap2);

        free(h_gammam1);
        free(h_gammam2);

        free(h_d1p);
        free(h_d1m);

        free(h_d2p);
        free(h_d2m);

        free(h_h);
    }
    else if(limiter_type == BJ_R || limiter_type == BJ_QUAD_R) {
        // open the mesh to get curr_num_elem for allocations
        mesh_file = fopen(limiter_filename, "r");
        if (!mesh_file) {
            printf("\nERROR: limiter file not found.\n"); 
            quit();
        }

        int *h_neighbours[20];
        int *val[20];
        cudaMalloc((void **) &d_neighbours, 3 * sizeof(int*));
        for(int i = 0 ; i < 20; i++){
            cudaMalloc((void **) h_neighbours + i, curr_num_elem * sizeof(int));
            val[i] = (int *) malloc(curr_num_elem * sizeof(int));
        }
        cudaMemcpy(d_neighbours, h_neighbours, 3 * sizeof(int*), cudaMemcpyHostToDevice);

        // read the elements from the mesh
        for (int i = 0; i < curr_num_elem; i++) {
            fgets(line, sizeof(line), mesh_file);
            items = sscanf(line, "%i %i %i", val[0] + i, val[1] +i, 
                                             val[2] + i);
            if (items != 3) {
                printf("ERROR: wrong file format for BJ_R limiter.\n");
            }

        }

        for(int i = 0 ; i < 3; i++){
            cudaMemcpy(h_neighbours[i],val[i], curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);
            free(val[i]);
        }

    }
else if(limiter_type == BJ_N || limiter_type == BJ_QUAD_N) {
        // open the mesh to get curr_num_elem for allocations
        mesh_file = fopen(limiter_filename, "r");
        if (!mesh_file) {
            printf("\nERROR: limiter file not found.\n"); 
            quit();
        }

        int *h_neighbours[20];
        int *val[20];
        cudaMalloc((void **) &d_neighbours, 20 * sizeof(int*));
        for(int i = 0 ; i < 20; i++){
            cudaMalloc((void **) h_neighbours + i, curr_num_elem * sizeof(int));
            val[i] = (int *) malloc(curr_num_elem * sizeof(int));
        }
        cudaMemcpy(d_neighbours, h_neighbours, 20 * sizeof(int*), cudaMemcpyHostToDevice);

        // read the elements from the mesh
        for (int i = 0; i < curr_num_elem; i++) {
            fgets(line, sizeof(line), mesh_file);
            items = sscanf(line, "%i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i", 
                                             val[0] + i, val[1] +i, 
                                             val[2] + i, val[3] +i,
                                             val[4] + i, val[5] +i,
                                             val[6] + i, val[7] +i,
                                             val[8] + i, val[9] +i,
                                             val[10] + i,val[11] +i,
                                             val[12] + i,val[13] +i,
                                             val[14] + i,val[15] +i,
                                             val[16] + i,val[17] +i,
                                             val[18] + i,val[19] +i);
            if (items != 20) {
                printf("ERROR: wrong file format for BJ_N limiter.\n");
            }
            // printf("%i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i \n", 
            //                                  h_neighbours[0][i], h_neighbours[1][i], 
            //                                  h_neighbours[2][i], h_neighbours[3][i],
            //                                  h_neighbours[4][i], h_neighbours[5][i],
            //                                  h_neighbours[6][i], h_neighbours[7][i],
            //                                  h_neighbours[8][i], h_neighbours[9][i],
            //                                  h_neighbours[10][i], h_neighbours[11][i],
            //                                  h_neighbours[12][i], h_neighbours[13][i],
            //                                  h_neighbours[14][i], h_neighbours[15][i],
            //                                  h_neighbours[16][i], h_neighbours[17][i],
            //                                  h_neighbours[18][i], h_neighbours[19][i]);
        }

        for(int i = 0 ; i < 20; i++){
            cudaMemcpy(h_neighbours[i],val[i], curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);
            free(val[i]);
        }

    }

    else if(limiter_type == COCKBURN) {
        // open the mesh to get curr_num_elem for allocations
        mesh_file = fopen(limiter_filename, "r");
        if (!mesh_file) {
            printf("\nERROR: limiter file not found.\n"); 
            quit();
        }
        int *h_e11, *h_e12, *h_e21, *h_e22, *h_e31, *h_e32;
        double *h_a11, *h_a12, *h_a21, *h_a22, *h_a31, *h_a32;

        h_e11 = (int *) malloc(curr_num_elem * sizeof(int));
        h_e12 = (int *) malloc(curr_num_elem * sizeof(int));
        h_e21 = (int *) malloc(curr_num_elem * sizeof(int));
        h_e22 = (int *) malloc(curr_num_elem * sizeof(int));
        h_e31 = (int *) malloc(curr_num_elem * sizeof(int));
        h_e32 = (int *) malloc(curr_num_elem * sizeof(int));

        h_a11 = (double *) malloc(curr_num_elem * sizeof(double));
        h_a12 = (double *) malloc(curr_num_elem * sizeof(double));
        h_a21 = (double *) malloc(curr_num_elem * sizeof(double));
        h_a22 = (double *) malloc(curr_num_elem * sizeof(double));
        h_a31 = (double *) malloc(curr_num_elem * sizeof(double));
        h_a32 = (double *) malloc(curr_num_elem * sizeof(double));

        // read the elements from the mesh
        for (int i = 0; i < curr_num_elem; i++) {
            fgets(line, sizeof(line), mesh_file);
            items = sscanf(line, "%i %i %i %i %i %i %lf %lf %lf %lf %lf %lf", 
                                             h_e11 + i, h_e12 +i, 
                                             h_e21 + i, h_e22 +i,
                                             h_e31 + i, h_e32 +i,
                                             h_a11 + i, h_a12 +i,
                                             h_a21 + i, h_a22 +i,
                                             h_a31 + i, h_a32 +i);
            if (items != 12) {
                printf("ERROR: wrong file format for Cockburn limiter.\n");
            }
        }

        cudaMemcpy(d_e11,h_e11, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_e12,h_e12, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_e21,h_e21, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_e22,h_e22, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_e31,h_e31, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_e32,h_e32, curr_num_elem * sizeof(int), cudaMemcpyHostToDevice);

        cudaMemcpy(d_a11,h_a11, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_a12,h_a12, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_a21,h_a21, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_a22,h_a22, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_a31,h_a31, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_a32,h_a32, curr_num_elem * sizeof(double), cudaMemcpyHostToDevice);

        free(h_e11);
        free(h_e12);
        free(h_e21);
        free(h_e22);
        free(h_e31);
        free(h_e32);

        free(h_a11);
        free(h_a12);
        free(h_a21);
        free(h_a22);
        free(h_a31);
        free(h_a32);
    }








    if(limiter_type == BJ || limiter_type == BJ_N || limiter_type == BJ_QUAD || limiter_type == BJ_R || limiter_type == BJ_QUAD_N || limiter_type == BJ_QUAD_R || limiter_type == MODAL || limiter_type == COCKBURN || limiter_type ==  NO_LIMITER|| limiter_type ==  MOMENT_AM){
        // init_bj_h<<<get_blocks(curr_num_elem, num_threads),num_threads>>>(d_h,
        //                                                                   V1x, V1y,
        //                                                                   V2x, V2y,
        //                                                                   V3x, V3y,
        //                                                                   curr_num_elem);

        // init_lbj_h<<<get_blocks(curr_num_elem, num_threads),num_threads>>>(d_h,
        //                                                                   V1x, V1y,
        //                                                                   V2x, V2y,
        //                                                                   V3x, V3y,
        //                                                                   curr_num_elem);

        init_nlbj_h<<<get_blocks(curr_num_elem, num_threads),num_threads>>>(d_h,
                                                                          V1x, V1y,
                                                                          V2x, V2y,
                                                                          V3x, V3y,
                                                                          curr_num_elem);
    }

}

__device__ double minmod(double a, double b) { 
    if (a > 0 && b > 0)
        return  fmin(a,b);
    else if(a < 0 && b < 0)
        return fmax(a, b);
    else
        return 0.;

}

__device__ double minmod(double a, double b, double c) { 
	if (a > 0 && b > 0 && c > 0)
		return  fmin(a,fmin(b,c));
	else if(a < 0 && b < 0 && c < 0)
		return fmax(a, fmax(b,c));
	else
		return 0.;

}

__device__ double minmod(double a, double b, double M, double h){

    if(fabs(a) < M * h*h)
        return a;
    else if( a * b < 0)
        return 0.;
    else if( fabs(a) > fabs(b) )
        return b;
    else 
        return a;

}



__global__ void cockburn_limiter(double **C,
                                 double *circles,
                                 double *basis_midpoint,
                                 int *elem11,  int *elem12,
                                 int *elem21,  int *elem22,
                                 int *elem31,  int *elem32,
                                 double *alpha11,  double *alpha12,
                                 double *alpha21,  double *alpha22,
                                 double *alpha31,  double *alpha32,
                                 int N, int n_p, int n_quad,
                                 double *w, double *basis,
                                 int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int sID[3],  neighbor_idx[6];
    double b0, b1, b2;
    double a, b , c;
    double u1, u2, u3;
    double U1, U2, U3;
    double l1, l2, l3;
    double d1, d2, d3 ;
    double v = 1.5, M = 50, pos, neg,theta_p, theta_n;
    double nx, ny, nz, p1[3], p2[3], p3[3];

    int e11, e12, e21, e22, e31, e32;
    double a11, a12, a21, a22, a31, a32;

    if (idx < num_elem) {
        e11 = elem11[idx]; e21 = elem21[idx]; e31 = elem31[idx];
        e12 = elem12[idx]; e22 = elem22[idx]; e32 = elem32[idx];

        a11 = alpha11[idx]; a21 = alpha21[idx]; a31 = alpha31[idx];
        a12 = alpha12[idx]; a22 = alpha22[idx]; a32 = alpha32[idx];

        for (int n = 0; n < N; n++) {
            l1 = l2 = l3 = -1;
            u1 = -C[n*n_p + 0][idx]* sqrt(2.);
            u2 = -C[n*n_p + 0][idx]* sqrt(2.);
            u3 = -C[n*n_p + 0][idx]* sqrt(2.);

            for(int i = 0; i < n_p; i++) {
                u1 += C[n*n_p + i][idx] * basis_midpoint[3 * i + 0];
                u2 += C[n*n_p + i][idx] * basis_midpoint[3 * i + 1];
                u3 += C[n*n_p + i][idx] * basis_midpoint[3 * i + 2];
            }
            b0 = C[n*n_p + 0][idx] * sqrt(2.);


            if(e11 > -1 && e12 > -1){
                b1 = C[n*n_p + 0][e11]* sqrt(2.);
                b2 = C[n*n_p + 0][e12]* sqrt(2.);
                l1 = a11*( b1-b0 ) + a12*( b2 - b0 );
            }
            else
                l1 = u1;
            if(e21 > -1 && e22 > -1){
                b1 = C[n*n_p + 0][e21]* sqrt(2.);
                b2 = C[n*n_p + 0][e22]* sqrt(2.);
                l2 = a21*( b1-b0 ) + a22*( b2-b0 );
            }
            else
                l2 = u2;
            if(e31 > -1 && e32 > -1){
                b1 = C[n*n_p + 0][e31]* sqrt(2.);
                b2 = C[n*n_p + 0][e32]* sqrt(2.);
                l3 = a31*( b1-b0 ) + a32*( b2-b0 );
            }
            else
                l3 = u3;


            d1 = minmod( u1, v*l1);
            d2 = minmod( u2, v*l2);
            d3 = minmod( u3, v*l3); 


            if( fabs(d1 + d2 + d3) > 1e-10 ){
                pos = fmax(0., d1) + fmax(0., d2) + fmax(0., d3);
                neg = fmax(0., -d1) + fmax(0., -d2) + fmax(0., -d3);
                theta_p = fmin(1., neg/pos);
                theta_n = fmin(1., pos/neg);

                d1 = theta_p*fmax(0., d1) - theta_n * fmax(0., -d1);
                d2 = theta_p*fmax(0., d2) - theta_n * fmax(0., -d2);
                d3 = theta_p*fmax(0., d3) - theta_n * fmax(0., -d3);
            }

            U1 = C[n*n_p + 0][idx]*sqrt(2.) + d1;
            U2 = C[n*n_p + 0][idx]*sqrt(2.) + d2;
            U3 = C[n*n_p + 0][idx]*sqrt(2.) + d3;
            a = 2 * U2 -2 * U3;
            b = -2 * U1 + 2*U2;
            c = U1 - U2 + U3;

            for (int i = 1; i < n_p; i++)
                C[n*n_p+i][idx] = 0.;

            // project the d1, d2, d3 onto the basis functions
            for (int i = 1; i < n_p; i++){
                C[n*n_p+i][idx] += w[0] * ( a*(1./6.) + b*(1./6.) + c ) * basis[i * n_quad + 0];
                C[n*n_p+i][idx] += w[1] * ( a*(2./3.) + b*(1./6.) + c ) * basis[i * n_quad + 1];
                C[n*n_p+i][idx] += w[2] * ( a*(1./6.) + b*(2./3.) + c ) * basis[i * n_quad + 2];
            }

        }

    }

}
__global__ void barth_jesperson_m(double **C, int *flag,
                                          double *basis_midpoint, double *basis_vertex,
                                          int *curr_s1, int *curr_s2, int *curr_s3,
                                          int *spos, int *d_schild1, int *d_schild2,
                                          int *left_elem, int *right_elem,
                                          int *lsn, int *rsn,
                                          int N, int n_p,
                                          int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem && flag[idx]) { 
        double U_i, U_c, Umin, Umax, alpha, min_alpha;
        int s[6],  sID[3];
        int n, i, side;
        int neighbor_idx[6]; 
        int haschildren;
        double y;
        int limit = 1;
        double U_vert;

    // load the sides of the elements
        sID[0] = curr_s1[idx];
        sID[1] = curr_s2[idx];
        sID[2] = curr_s3[idx];

        for(int i = 0; i < 3; i++) {
            haschildren = d_schild1[sID[i]];
            if(haschildren > -1){
                s[i] = spos[d_schild1[sID[i]]];
                s[i + 3] = spos[d_schild2[sID[i]]];
            }
            else{
                s[i] = spos[ sID[i] ];
                s[i + 3] = -1;
            }
        }
        // get element neighbor indexes
        // and make sure we aren't on a boundary element
        for( i = 0; i < 6; i++ ) {
            neighbor_idx[i] = -1;

            if(s[i] > -1) {
                if(left_elem[s[i]] == idx )  {
                    neighbor_idx[i] = right_elem[s[i]];
                }
                else{
                    neighbor_idx[i] = left_elem[s[i]];
                }
            }
        }

        // for( i = 0; i < 3; i++ ) {
        //     neighbor_idx[i] = -1;

        //     if(sID[i] > -1) {
        //         if(left_elem[sID[i]] == idx )  {
        //             neighbor_idx[i] = right_elem[sID[i]];
        //         }
        //         else{
        //             neighbor_idx[i] = left_elem[sID[i]];
        //         }
        //     }
        //     // if(idx ==0)
        //     //         printf("%i %i %i %i\n", idx, sID[i], left_elem[sID[i]], right_elem[sID[i]]); 
        // }
        
        for (n = 0; n < N; n++) {
            // set initial stuff
            U_c = C[n*n_p + 0][idx] * sqrt(2.);
            Umin = U_c;
            Umax = U_c;

            // get delmin and delmax
            for (i = 0; i < 6; i++) {
                if(neighbor_idx[i] > -1) {
                    U_i = C[n*n_p + 0][neighbor_idx[i]] * sqrt(2.);
                    
                    Umin = (U_i < Umin) ? U_i : Umin;
                    Umax = (U_i > Umax) ? U_i : Umax;
                }
            }

            min_alpha = 1.;
            // at the midpoints along the boundaries
            for (side = 0; side < 3; side++) {
                U_i = 0.;
                //evaluate U
                for (i = 0; i < n_p; i++) {
                    U_i += C[n*n_p + i][idx] * basis_midpoint[3 * i + side];
                }

                y = 1.;
                // evaluate alpha
                if (U_i > U_c) {
                    y = (Umax - U_c)/(U_i - U_c);
                } else if (U_i < U_c) {
                    y = (Umin - U_c)/(U_i - U_c);
                } 
                alpha = fmin(1., y); // Barth Jesperson

                if (alpha < min_alpha)  {
                    min_alpha = alpha;
                }
            }



            if (min_alpha < 0) {
                min_alpha = 0.;
            }

            // if(1e-10 <min_alpha && min_alpha <1. -1e-10 ){
            //     // printf("%i, %i %i %i, %lf\n neighbours are %lf %lf %lf\n my average is %lf \n\n", idx, 
            //     //                                                                      neighbor_idx[0],
            //     //                                                                      neighbor_idx[1],
            //     //                                                                      neighbor_idx[2],
            //     //                                                                      min_alpha,
            //     //                                                                      C[n*n_p + 0][neighbor_idx[0]] * sqrt(2.), 
            //     //                                                                      C[n*n_p + 0][neighbor_idx[1]] * sqrt(2.), 
            //     //                                                                      C[n*n_p + 0][neighbor_idx[2]] * sqrt(2.), 
            //     //                                                                      C[n*n_p + 0][idx] * sqrt(2.));
            //     printf("%i, %lf\n", idx, min_alpha);
            // }
            // limit = 1;
            // for(int side = 0; side < 3; side++)
            //     limit = limit && (neighbor_idx[side] > -1);

            // if(!limit)
            //     min_alpha = 0.;
            
            // if(idx == 23393)
            //     printf("idx %i alpha %lf\n", idx, alpha);

            limit = 1;
            for(int side = 0; side < 3; side++)
                limit = limit && (neighbor_idx[side] > -1);

            if(!limit)
                min_alpha = BOUNDARY_FACTOR;


            // if(min_alpha < 1 - 1e-10)
            //     printf("alpha %lf\n", min_alpha);
                // min_alpha = 0.;

            // limit the slope
            C[n*n_p + 1][idx] *= min_alpha;
            C[n*n_p + 2][idx] *= min_alpha;
        }
    }
}

__global__ void barth_jesperson_m(double **C,
                                          double *basis_midpoint, double *basis_vertex,
                                          int *curr_s1, int *curr_s2, int *curr_s3,
                                          int *spos, int *d_schild1, int *d_schild2,
                                          int *left_elem, int *right_elem,
                                          int *lsn, int *rsn,
                                          int N, int n_p,
                                          int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) { 
        double U_i, U_c, Umin, Umax, alpha, min_alpha;
        int s[6],  sID[3];
        int n, i, side;
        int neighbor_idx[6]; 
        int haschildren;
        double y;
        int limit = 1;
        double U_vert;

    // load the sides of the elements
        sID[0] = curr_s1[idx];
        sID[1] = curr_s2[idx];
        sID[2] = curr_s3[idx];

        for(int i = 0; i < 3; i++) {
            haschildren = d_schild1[sID[i]];
            if(haschildren > -1){
                s[i] = spos[d_schild1[sID[i]]];
                s[i + 3] = spos[d_schild2[sID[i]]];
            }
            else{
                s[i] = spos[ sID[i] ];
                s[i + 3] = -1;
            }
        }
        // get element neighbor indexes
        // and make sure we aren't on a boundary element
        for( i = 0; i < 6; i++ ) {
            neighbor_idx[i] = -1;

            if(s[i] > -1) {
                if(left_elem[s[i]] == idx )  {
                    neighbor_idx[i] = right_elem[s[i]];
                }
                else{
                    neighbor_idx[i] = left_elem[s[i]];
                }
            }
        }

        // for( i = 0; i < 3; i++ ) {
        //     neighbor_idx[i] = -1;

        //     if(sID[i] > -1) {
        //         if(left_elem[sID[i]] == idx )  {
        //             neighbor_idx[i] = right_elem[sID[i]];
        //         }
        //         else{
        //             neighbor_idx[i] = left_elem[sID[i]];
        //         }
        //     }
        //     // if(idx ==0)
        //     //         printf("%i %i %i %i\n", idx, sID[i], left_elem[sID[i]], right_elem[sID[i]]); 
        // }
        
        for (n = 0; n < N; n++) {
            // set initial stuff
            U_c = C[n*n_p + 0][idx] * sqrt(2.);
            Umin = U_c;
            Umax = U_c;

            // get delmin and delmax
            for (i = 0; i < 6; i++) {
                if(neighbor_idx[i] > -1) {
                    U_i = C[n*n_p + 0][neighbor_idx[i]] * sqrt(2.);
                    
                    Umin = (U_i < Umin) ? U_i : Umin;
                    Umax = (U_i > Umax) ? U_i : Umax;
                }
            }

            min_alpha = 1.;
            // at the midpoints along the boundaries
            for (side = 0; side < 3; side++) {
                U_i = 0.;
                //evaluate U
                for (i = 0; i < n_p; i++) {
                    U_i += C[n*n_p + i][idx] * basis_midpoint[3 * i + side];
                }

                y = 1.;
                // evaluate alpha
                if (U_i > U_c) {
                    y = (Umax - U_c)/(U_i - U_c);
                } else if (U_i < U_c) {
                    y = (Umin - U_c)/(U_i - U_c);
                } 
                alpha = fmin(1., y); // Barth Jesperson

                if (alpha < min_alpha)  {
                    min_alpha = alpha;
                }
            }



            if (min_alpha < 0) {
                min_alpha = 0.;
            }

            // if(1e-10 <min_alpha && min_alpha <1. -1e-10 ){
            //     // printf("%i, %i %i %i, %lf\n neighbours are %lf %lf %lf\n my average is %lf \n\n", idx, 
            //     //                                                                      neighbor_idx[0],
            //     //                                                                      neighbor_idx[1],
            //     //                                                                      neighbor_idx[2],
            //     //                                                                      min_alpha,
            //     //                                                                      C[n*n_p + 0][neighbor_idx[0]] * sqrt(2.), 
            //     //                                                                      C[n*n_p + 0][neighbor_idx[1]] * sqrt(2.), 
            //     //                                                                      C[n*n_p + 0][neighbor_idx[2]] * sqrt(2.), 
            //     //                                                                      C[n*n_p + 0][idx] * sqrt(2.));
            //     printf("%i, %lf\n", idx, min_alpha);
            // }
            // limit = 1;
            // for(int side = 0; side < 3; side++)
            //     limit = limit && (neighbor_idx[side] > -1);

            // if(!limit)
            //     min_alpha = 0.;
            
            // if(idx == 23393)
            //     printf("idx %i alpha %lf\n", idx, alpha);

            limit = 1;
            for(int side = 0; side < 3; side++)
                limit = limit && (neighbor_idx[side] > -1);

            if(!limit)
                min_alpha = BOUNDARY_FACTOR;


            // if(min_alpha < 1 - 1e-10)
            //     printf("alpha %lf\n", min_alpha);
                // min_alpha = 0.;

            // limit the slope
            C[n*n_p + 1][idx] *= min_alpha;
            C[n*n_p + 2][idx] *= min_alpha;
        }
    }
}


__global__ void barth_jesperson_m_second_order(double **C,
                                               double *basis_midpoint, double *basis_vertex,
                                               int *curr_s1, int *curr_s2, int *curr_s3,
                                               int *spos, int *d_schild1, int *d_schild2,
                                               int *left_elem, int *right_elem,
                                               int *lsn, int *rsn,
                                               int N, int n_p,
                                               int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) { 
        double U_i, U_c, Umin, Umax, alpha, min_alpha;
        int s[6],  sID[3];
        int n, i, side;
        int neighbor_idx[6]; 
        int haschildren;
        double y;
        int limit = 1;
        double U_vert;

    // load the sides of the elements
        sID[0] = curr_s1[idx];
        sID[1] = curr_s2[idx];
        sID[2] = curr_s3[idx];

        for(int i = 0; i < 3; i++) {
            haschildren = d_schild1[sID[i]];
            if(haschildren > -1){
                s[i] = spos[d_schild1[sID[i]]];
                s[i + 3] = spos[d_schild2[sID[i]]];
            }
            else{
                s[i] = spos[ sID[i] ];
                s[i + 3] = -1;
            }
        }
        // get element neighbor indexes
        // and make sure we aren't on a boundary element
        for( i = 0; i < 6; i++ ) {
            neighbor_idx[i] = -1;

            if(s[i] > -1) {
                if(left_elem[s[i]] == idx )  {
                    neighbor_idx[i] = right_elem[s[i]];
                }
                else{
                    neighbor_idx[i] = left_elem[s[i]];
                }
            }
        }

        for (n = 0; n < N; n++) {
            // set initial stuff
            U_c = C[n*n_p + 0][idx] * sqrt(2.);
            Umin = U_c;
            Umax = U_c;

            // get delmin and delmax
            for (i = 0; i < 6; i++) {
                if(neighbor_idx[i] > -1) {
                    U_i = C[n*n_p + 0][neighbor_idx[i]] * sqrt(2.);
                    
                    Umin = (U_i < Umin) ? U_i : Umin;
                    Umax = (U_i > Umax) ? U_i : Umax;
                }
            }

            min_alpha = 1.;
            // at the midpoints along the boundaries
            for (side = 0; side < 3; side++) {
                U_i = 0.;
                //evaluate U
                for (i = 0; i < 3; i++) {
                // for (i = 0; i < n_p; i++) {
                    U_i += C[n*n_p + i][idx] * basis_midpoint[3 * i + side];
                }

                y = 1.;
                // evaluate alpha
                if (U_i > U_c) {
                    y = (Umax - U_c)/(U_i - U_c);
                } else if (U_i < U_c) {
                    y = (Umin - U_c)/(U_i - U_c);
                } 
                alpha = fmin(1., y); // Barth Jesperson

                if (alpha < min_alpha)  {
                    min_alpha = alpha;
                }
            }



            if (min_alpha < 0) {
                min_alpha = 0.;
            }


            limit = 1;
            for(int side = 0; side < 3; side++)
                limit = limit && (neighbor_idx[side] > -1);

            if(!limit)
                min_alpha = BOUNDARY_FACTOR;


            // if(min_alpha < 1 - 1e-10)
            //     printf("alpha %lf\n", min_alpha);
                // min_alpha = 0.;

            // limit the slope
            if(min_alpha < 1-1e-10){
                C[n*n_p + 1][idx] *= min_alpha;
                C[n*n_p + 2][idx] *= min_alpha;
                for(int k = 3; k < n_p; k++)
                    C[n*n_p + k][idx] = 0.;
            }


        }
    }
}

__global__ void barth_jesperson_q(double **C, double *basis_side,
                                     int *curr_s1, int *curr_s2, int *curr_s3,
                                     int *spos, int *d_schild1, int *d_schild2,
                                     int *left_elem, int *right_elem,
                                     int *lsn, int *rsn,
                                     int N, int n_p, int n_quad1d,
                                     int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) { 
        double U_i, U_c, Umin, Umax, alpha, min_alpha;
        int s[6],  sID[3];
        int n, i, side, j;
        int neighbor_idx[6];
        int haschildren;
        double y;
        int limit = 1;

    // load the sides of the elements
        sID[0] = curr_s1[idx];
        sID[1] = curr_s2[idx];
        sID[2] = curr_s3[idx];

        for(int i = 0; i < 3; i++) {
            haschildren = d_schild1[sID[i]];
            if(haschildren > -1){
                s[i] = spos[d_schild1[sID[i]]];
                s[i + 3] = spos[d_schild2[sID[i]]];
            }
            else{
                s[i] = spos[ sID[i] ];
                s[i + 3] = -1;
            }
        }


        // get element neighbor indexes
        // and make sure we aren't on a boundary element
        for( i = 0; i < 6; i++ ) {
            neighbor_idx[i] = -1;

            if(s[i] > 0) {
                if(left_elem[s[i]] == idx )  {
                    neighbor_idx[i] = right_elem[s[i]];
                }
                else{
                    neighbor_idx[i] = left_elem[s[i]];
                }
            }
        }
        
        for (n = 0; n < N; n++) {
            // set initial stuff
            U_c = C[n*n_p + 0][idx] * sqrt(2.);
            Umin = U_c;
            Umax = U_c;

            // get delmin and delmax
            for (i = 0; i < 6; i++) {
                if(neighbor_idx[i] > -1) {
                    U_i = C[n*n_p + 0][neighbor_idx[i]] * sqrt(2.);
                    
                    Umin = (U_i < Umin) ? U_i : Umin;
                    Umax = (U_i > Umax) ? U_i : Umax;
                }
            }

            min_alpha = 1.;
            // if(idx == 33)
            //     printf("neighbours are %i %i %i %i %i %i\n", neighbor_idx[0], neighbor_idx[1], neighbor_idx[2], neighbor_idx[3], neighbor_idx[4], neighbor_idx[5]);
            // at the midpoints along the boundaries
            for (side = 0; side < 3; side++) {
                for(j=0; j < n_quad1d; j++) {
                    U_i = 0.;
                    //evaluate U
                    for (i = 0; i < n_p; i++) {
                        U_i += C[n*n_p + i][idx] * basis_side[ side  * (n_quad1d * n_p) + i * n_quad1d + j];
                    }

                    y = 1.;
                    // evaluate alpha
                    if (U_i > U_c) {
                        y = (Umax - U_c)/(U_i - U_c);
                    } else if (U_i < U_c) {
                        y = (Umin - U_c)/(U_i - U_c);
                    } 
                    alpha = fmin(1., y); // Barth Jesperson

                    if (alpha < min_alpha)  {
                        min_alpha = alpha;
                    }
                }
            }



            if (min_alpha < 0) {
                min_alpha = 0.;
            }

            limit = 1;
            for(int side = 0; side < 3; side++)
                limit = limit && (neighbor_idx[side] > -1);

            if(!limit)
                min_alpha = BOUNDARY_FACTOR;
            
            // if(idx == 23393)
            //     printf("idx %i alpha %lf\n", idx, alpha);
            // limit the slope
            C[n*n_p + 1][idx] *= min_alpha;
            C[n*n_p + 2][idx] *= min_alpha;
        }
    }
}



__global__ void barth_jesperson_m_neighbourhood(double **C,double *basis_midpoint,
                                              int **d_neighbours,
                                              int N, int n_p,
                                              int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) { 
        double U_i, U_c, Umin, Umax, alpha, min_alpha;
        int neighbor_idx[20];
        int haschildren;
        double y;

        for(int neigh = 0; neigh < 20 ; neigh ++){
            neighbor_idx[neigh] = d_neighbours[neigh][idx];
        }
        
        if(neighbor_idx[0] > -1) {
            for (int n = 0; n < N; n++) {
                // set initial stuff
                U_c = C[n*n_p + 0][idx] * sqrt(2.);
                Umin = U_c;
                Umax = U_c;

                // get delmin and delmax
                for (int i = 0; i < 20; i++) {
                    if(neighbor_idx[i] > -1) {
                        U_i = C[n*n_p + 0][neighbor_idx[i]] * sqrt(2.);
                        
                        Umin = (U_i < Umin) ? U_i : Umin;
                        Umax = (U_i > Umax) ? U_i : Umax;
                    }
                }

                min_alpha = 1.;
                // at the midpoints along the boundaries
                for (int side = 0; side < 3; side++) {
                    U_i = 0.;
                    //evaluate U
                    for (int i = 0; i < n_p; i++) {
                        U_i += C[n*n_p + i][idx] * basis_midpoint[3 * i + side];
                    }

                    y = 1.;
                    alpha = 1.;
                    // evaluate alpha
                    if (U_i > U_c) {
                        y = (Umax - U_c)/(U_i - U_c);
                    } else if (U_i < U_c) {
                        y = (Umin - U_c)/(U_i - U_c);
                    } 
                    alpha = fmin(1., y); // Barth Jesperson

                    if (alpha < min_alpha)  {
                        min_alpha = alpha;
                    }
                }



                if (min_alpha < 0) {
                    min_alpha = 0.;
                }
                
                // limit the slope
                C[n*n_p + 1][idx] *= min_alpha;
                C[n*n_p + 2][idx] *= min_alpha;
            }
        }
        else{
            for (int n = 0; n < N; n++) {
                C[n*n_p + 1][idx] *= BOUNDARY_FACTOR;
                C[n*n_p + 2][idx] *= BOUNDARY_FACTOR;  
            }
        }


    }
}


__global__ void barth_jesperson_m_rneighbourhood(double **C,double *basis_midpoint,
                                              int **d_neighbours,
                                              int N, int n_p,
                                              int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) { 
        double U_i, U_c, Umin, Umax, alpha, min_alpha;
        int neighbor_idx[20];
        int haschildren;
        double y;

        for(int neigh = 0; neigh < 3 ; neigh ++){
            neighbor_idx[neigh] = d_neighbours[neigh][idx];
        }
        
        if(neighbor_idx[0] > -1){
            for (int n = 0; n < N; n++) {
                // set initial stuff
                U_c = C[n*n_p + 0][idx] * sqrt(2.);
                Umin = U_c;
                Umax = U_c;

                // get delmin and delmax
                for (int i = 0; i < 3; i++) {
                    if(neighbor_idx[i] > -1) {
                        U_i = C[n*n_p + 0][neighbor_idx[i]] * sqrt(2.);
                        
                        Umin = (U_i < Umin) ? U_i : Umin;
                        Umax = (U_i > Umax) ? U_i : Umax;
                    }
                }

                min_alpha = 1.;
                // at the midpoints along the boundaries
                for (int side = 0; side < 3; side++) {
                    U_i = 0.;
                    //evaluate U
                    for (int i = 0; i < n_p; i++) {
                        U_i += C[n*n_p + i][idx] * basis_midpoint[3 * i + side];
                    }

                    y = 1.;
                    alpha = 1.;
                    // evaluate alpha
                    if (U_i > U_c) {
                        y = (Umax - U_c)/(U_i - U_c);
                    } else if (U_i < U_c) {
                        y = (Umin - U_c)/(U_i - U_c);
                    } 
                    alpha = fmin(1., y); // Barth Jesperson

                    if (alpha < min_alpha)  {
                        min_alpha = alpha;
                    }
                }



                if (min_alpha < 0) {
                    min_alpha = 0.;
                }
                
                // limit the slope
                C[n*n_p + 1][idx] *= min_alpha;
                C[n*n_p + 2][idx] *= min_alpha;
            }
        }
        else{
            for (int n = 0; n < N; n++) {
                C[n*n_p + 1][idx] *= BOUNDARY_FACTOR;
                C[n*n_p + 2][idx] *= BOUNDARY_FACTOR;
            }
        }
    }
}



__global__ void barth_jesperson_q_rneighbourhood(double **C,double *basis_side,
                                              int **d_neighbours,
                                              int N, int n_p, int n_quad1d,
                                              int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) { 
        double U_i, U_c, Umin, Umax, alpha, min_alpha;
        int neighbor_idx[20];
        int haschildren;
        double y;

        for(int neigh = 0; neigh < 3 ; neigh ++){
            neighbor_idx[neigh] = d_neighbours[neigh][idx];
        }
        
        if(neighbor_idx[0] > -1){
            for (int n = 0; n < N; n++) {
                // set initial stuff
                U_c = C[n*n_p + 0][idx] * sqrt(2.);
                Umin = U_c;
                Umax = U_c;

                // get delmin and delmax
                for (int i = 0; i < 3; i++) {
                    if(neighbor_idx[i] > -1) {
                        U_i = C[n*n_p + 0][neighbor_idx[i]] * sqrt(2.);
                        
                        Umin = (U_i < Umin) ? U_i : Umin;
                        Umax = (U_i > Umax) ? U_i : Umax;
                    }
                }

                min_alpha = 1.;
                // at the midpoints along the boundaries
                for (int side = 0; side < 3; side++) {
                    for(int j=0; j < n_quad1d; j++) {
                        U_i = 0.;
                        //evaluate U
                        for (int i = 0; i < n_p; i++) {
                            U_i += C[n*n_p + i][idx] * basis_side[ side  * (n_quad1d * n_p) + i * n_quad1d + j];
                        }

                        y = 1.;
                        // evaluate alpha
                        if (U_i > U_c) {
                            y = (Umax - U_c)/(U_i - U_c);
                        } else if (U_i < U_c) {
                            y = (Umin - U_c)/(U_i - U_c);
                        } 
                        alpha = fmin(1., y); // Barth Jesperson

                        if (alpha < min_alpha)  {
                            min_alpha = alpha;
                        }
                    }
                }



                if (min_alpha < 0) {
                    min_alpha = 0.;
                }
                
                // limit the slope
                C[n*n_p + 1][idx] *= min_alpha;
                C[n*n_p + 2][idx] *= min_alpha;
            }
        }
        else{
            for (int n = 0; n < N; n++) {
                C[n*n_p + 1][idx] *= BOUNDARY_FACTOR;
                C[n*n_p + 2][idx] *= BOUNDARY_FACTOR;
            }
        }
    }
}


__global__ void barth_jesperson_q_neighbourhood(double **C,double *basis_side,
                                              int **d_neighbours,
                                              int N, int n_p, int n_quad1d,
                                              int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) { 
        double U_i, U_c, Umin, Umax, alpha, min_alpha;
        int neighbor_idx[20];
        int haschildren;
        double y;

        for(int neigh = 0; neigh < 20 ; neigh ++){
            neighbor_idx[neigh] = d_neighbours[neigh][idx];
        }
        
        if(neighbor_idx[0] > -1){
            for (int n = 0; n < N; n++) {
                // set initial stuff
                U_c = C[n*n_p + 0][idx] * sqrt(2.);
                Umin = U_c;
                Umax = U_c;

                // get delmin and delmax
                for (int i = 0; i < 20; i++) {
                    if(neighbor_idx[i] > -1) {
                        U_i = C[n*n_p + 0][neighbor_idx[i]] * sqrt(2.);
                        
                        Umin = (U_i < Umin) ? U_i : Umin;
                        Umax = (U_i > Umax) ? U_i : Umax;
                    }
                }

                min_alpha = 1.;
                // at the midpoints along the boundaries
                for (int side = 0; side < 3; side++) {
                    for(int j=0; j < n_quad1d; j++) {
                        U_i = 0.;
                        //evaluate U
                        for (int i = 0; i < n_p; i++) {
                            U_i += C[n*n_p + i][idx] * basis_side[ side  * (n_quad1d * n_p) + i * n_quad1d + j];
                        }

                        y = 1.;
                        // evaluate alpha
                        if (U_i > U_c) {
                            y = (Umax - U_c)/(U_i - U_c);
                        } else if (U_i < U_c) {
                            y = (Umin - U_c)/(U_i - U_c);
                        } 
                        alpha = fmin(1., y); // Barth Jesperson

                        if (alpha < min_alpha)  {
                            min_alpha = alpha;
                        }
                    }
                }



                if (min_alpha < 0) {
                    min_alpha = 0.;
                }
                
                // if(n ==0 && min_alpha >1e-10) {
                //     U_i = 0.;
                //     //evaluate U
                //     int j=0; int side = 0;
                //     for (int i = 0; i < n_p; i++) {
                //         U_i += C[n*n_p + i][idx] * basis_side[ side  * (n_quad1d * n_p) + i * n_quad1d + j];
                //     }
                //     printf("idx %i alpha %lf, %lf %lf, Ui %lf\n", idx, min_alpha, Umin, Umax, U_i);
                // }
                // limit the slope
                C[n*n_p + 1][idx] *= min_alpha;
                C[n*n_p + 2][idx] *= min_alpha;
            }
        }
        else{
            for (int n = 0; n < N; n++) {
                C[n*n_p + 1][idx] *= BOUNDARY_FACTOR;
                C[n*n_p + 2][idx] *= BOUNDARY_FACTOR;
            }
        }
        
    }
}







__global__ void bj_v(double **C,
                     double *basis_side, double *basis_midpoint,
                     int **d_neighbours,
                     int N, int n_p, int n_quad1d,
                     int quad,
                     int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int count, neigh;
    double U_i, U_c, Umin, Umax, alpha, min_alpha;
    double C_i[NP_MAX], C_neigh[20];
    double y;

    if (idx < num_elem) { 
        if(d_neighbours[0][idx] > -1){
            for (int n = 0; n < N; n++) {
                // load data
                for (int i = 0; i < n_p; i++) 
                    C_i[i] =  C[n*n_p + i][idx];

                count = 0;
                neigh = d_neighbours[0][idx];
                while(neigh>-1){ 
                    C_neigh[count] =  C[n*n_p + 0][neigh];
                    count ++;
                    neigh = d_neighbours[count][idx];
                }

                U_c = C_i[0] * sqrt(2.);
                Umin = U_c;
                Umax = U_c;

                // get delmin and delmax
                for (int i = 0; i < count; i++) {
                    U_i = C_neigh[i] * sqrt(2.);
                    
                    Umin = (U_i < Umin) ? U_i : Umin;
                    Umax = (U_i > Umax) ? U_i : Umax;
                }

                // if(idx == 722){
                //     printf("%lf \n %lf \n %lf\n %lf  \n %lf  \n %lf\n %lf  \n %lf  \n %lf\n %lf  \n %lf  \n%lf\n", C_neigh[0]* sqrt(2.) , C_neigh[1]* sqrt(2.) , C_neigh[2]* sqrt(2.),
                //                                                                       C_neigh[3]* sqrt(2.) , C_neigh[4]* sqrt(2.) , C_neigh[5]* sqrt(2.),
                //                                                                       C_neigh[6]* sqrt(2.) , C_neigh[7]* sqrt(2.) , C_neigh[8]* sqrt(2.),
                //                                                                       C_neigh[9]* sqrt(2.) , C_neigh[10]* sqrt(2.) , C_neigh[11]* sqrt(2.));
                //     printf("\n %i %i %i\n %i %i %i\n %i %i %i\n %i %i %i\n", d_neighbours[0][idx] , d_neighbours[1][idx] , d_neighbours[2][idx],
                //                                                                       d_neighbours[3][idx] , d_neighbours[4][idx] , d_neighbours[5][idx],
                //                                                                       d_neighbours[6][idx] , d_neighbours[7][idx] , d_neighbours[8][idx],
                //                                                                       d_neighbours[9][idx] , d_neighbours[10][idx] , d_neighbours[11][idx]);
                    
                //     C_i[1] *=  1000.;
                //     C_i[2] *=  1000.;
                //     printf("%lf %lf %lf\n", C_i[0], C_i[1], C_i[2]);
                //     printf("%lf %lf %lf\n", C_i[0]* sqrt(2.), Umin, Umax);
                // }


                min_alpha = 1.;
                // at the midpoints along the boundaries
                for (int side = 0; side < 3; side++) {
                    if(quad){
                        for(int j=0; j < n_quad1d; j++) {
                            U_i = 0.;
                            //evaluate U
                            for (int i = 0; i < n_p; i++) {
                                U_i += C_i[i] * basis_side[ side  * (n_quad1d * n_p) + i * n_quad1d + j];
                            }

                            y = 1.;
                            // evaluate alpha
                            if (U_i > U_c) {
                                y = (Umax - U_c)/(U_i - U_c);
                            } else if (U_i < U_c) {
                                y = (Umin - U_c)/(U_i - U_c);
                            } 
                            alpha = fmin(1., y); // Barth Jesperson

                            if (alpha < min_alpha)  {
                                min_alpha = alpha;
                            }
                        }
                    }
                    else{ //midpoint
                        U_i = 0.;
                        //evaluate U
                        for (int i = 0; i < n_p; i++) {
                            U_i += C_i[i] * basis_midpoint[3 * i + side];
                        }

                        y = 1.;
                        alpha = 1.;
                        // evaluate alpha
                        if (U_i > U_c) {
                            y = (Umax - U_c)/(U_i - U_c);
                        } else if (U_i < U_c) {
                            y = (Umin - U_c)/(U_i - U_c);
                        } 
                        alpha = fmin(1., y); // Barth Jesperson
                            // if(idx == 722)
                            //     printf("y is %lf\n", y);
                        if (alpha < min_alpha)  {
                            min_alpha = alpha;
                        }
                    }
                }



                if (min_alpha < 0) {
                    min_alpha = 0.;
                }
                
                // if(idx == 722)
                //     printf("min alpha is %lf\n", min_alpha);

                // limit the slope
                C[n*n_p + 1][idx] *= min_alpha;
                C[n*n_p + 2][idx] *= min_alpha;
            }
        }
        else{
            for (int n = 0; n < N; n++) {
                C[n*n_p + 1][idx] *= BOUNDARY_FACTOR;
                C[n*n_p + 2][idx] *= BOUNDARY_FACTOR;
            }
        }
        
    }
}





__global__ void bj_r(double **C,
                     double *basis_side, double *basis_midpoint,
                     int **d_neighbours,
                     int N, int n_p, int n_quad1d,
                     int quad,
                     int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    double U_i, U_c, Umin, Umax, alpha, min_alpha;
    double C_i[NP_MAX], C_neigh[3];
    double y; 

    if (idx < num_elem) { 
        if(d_neighbours[0][idx] > -1){
            for (int n = 0; n < N; n++) {
                // load data
                for (int i = 0; i < n_p; i++) 
                    C_i[i] =  C[n*n_p + i][idx];
                for (int i = 0; i < 3; i++) 
                    C_neigh[i] =  C[n*n_p + 0][d_neighbours[i][idx]];

                U_c = C_i[0] * sqrt(2.);
                Umin = U_c;
                Umax = U_c;

                // get delmin and delmax
                for (int i = 0; i < 3; i++) {
                    U_i = C_neigh[i] * sqrt(2.);
                    
                    Umin = (U_i < Umin) ? U_i : Umin;
                    Umax = (U_i > Umax) ? U_i : Umax;
                }

                // if(idx == 722){
                //     printf("%lf \n %lf \n %lf\n ", C_neigh[0]* sqrt(2.) , C_neigh[1]* sqrt(2.) , C_neigh[2]* sqrt(2.));
                //     printf("\n %i %i %i\n ", d_neighbours[0][idx] , d_neighbours[1][idx] , d_neighbours[2][idx]);
                    
                //     C_i[1] *=  1000.;
                //     C_i[2] *=  1000.;
                //     printf("%lf %lf %lf\n", C_i[0], C_i[1], C_i[2]);
                //     printf("%lf %lf %lf\n", C_i[0]* sqrt(2.), Umin, Umax);
                // }




                min_alpha = 1.;
                // at the midpoints along the boundaries
                for (int side = 0; side < 3; side++) {
                    if(quad){
                        for(int j=0; j < n_quad1d; j++) {
                            U_i = 0.;
                            //evaluate U
                            for (int i = 0; i < n_p; i++) {
                                U_i += C_i[i] * basis_side[ side  * (n_quad1d * n_p) + i * n_quad1d + j];
                            }

                            y = 1.;
                            // evaluate alpha
                            if (U_i > U_c) {
                                y = (Umax - U_c)/(U_i - U_c);
                            } else if (U_i < U_c) {
                                y = (Umin - U_c)/(U_i - U_c);
                            } 
                            alpha = fmin(1., y); // Barth Jesperson
                            if (alpha < min_alpha)  {
                                min_alpha = alpha;
                            }
                        }
                    }
                    else{ //midpoint
                        U_i = 0.;
                        //evaluate U
                        for (int i = 0; i < n_p; i++) {
                            U_i += C_i[i] * basis_midpoint[3 * i + side];
                        }

                        y = 1.;
                        alpha = 1.;
                        // evaluate alpha
                        if (U_i > U_c) {
                            y = (Umax - U_c)/(U_i - U_c);
                        } else if (U_i < U_c) {
                            y = (Umin - U_c)/(U_i - U_c);
                        } 
                        alpha = fmin(1., y); // Barth Jesperson
                        if (alpha < min_alpha)  {
                            min_alpha = alpha;
                        }
                    }
                }



                if (min_alpha < 0) {
                    min_alpha = 0.;
                }
                
                // limit the slope
                C[n*n_p + 1][idx] *= min_alpha;
                C[n*n_p + 2][idx] *= min_alpha;
            }
        }
        else{
            for (int n = 0; n < N; n++) {
                C[n*n_p + 1][idx] *= BOUNDARY_FACTOR;
                C[n*n_p + 2][idx] *= BOUNDARY_FACTOR;
            }
        }
        
    }
}



// __global__ void physical_limiter(double **C,
//                                  double *basis_side, double *basis_midpoint,
//                                  int **d_neighbours,
//                                  int N, int n_p, int n_quad1d,
//                                  int quad,
//                                  int num_elem) {

//     int idx = blockIdx.x * blockDim.x + threadIdx.x;
//     if (idx < num_elem) { 
//         double rho_i;

//         // surface quad
//         // positive density
//         for(int q = 0; q < n_quad1d, q++){
//             rho_i = 0.;
//             for(int i = 0; i < n_p; i++)
//                 rho_i += C[n*n_p + i][idx] * basis_side[n_quad1d*i + q];
            
//         }

//         // positive pressure
        
//     }
// }

__global__ void check(double **C, double **C_prev,
                      int **d_neighbours, int timestep,
                      int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) { 
        // double Umin, Umax, Ucurr, Utemp;
        // Ucurr = C[0][idx] * sqrt(2.);
        // if(!(0. - 1e-10 < Ucurr && Ucurr < 1. + 1e-10) )
        //     printf("\n \n LOCAL MAXIMUM PRINCIPLE IS VIOLATED on cell %i, (%.10e, %.10e, %.10e) on timestep %i\n\n", idx, Umin, Ucurr, Umax, timestep);

        double Umin, Umax, Ucurr, Utemp;
        int neighbor_idx[20];
        int n, nn, m;
        if(d_neighbours[0][idx] > -1){
            for(int neigh = 0; neigh < 20 ; neigh ++)
                neighbor_idx[neigh] = d_neighbours[neigh][idx];

            Umin = C_prev[0][neighbor_idx[0]] * sqrt(2.);
            Umax = C_prev[0][neighbor_idx[0]] * sqrt(2.);

            for(int i = 0; i < 20; i++){
                n = neighbor_idx[i];
                for(int j = 0; j < 20 && n > -1; j++){
                    nn = d_neighbours[j][n];
                    if(nn > -1){
                        Utemp = C_prev[0][nn] * sqrt(2.); 
                        Umin = min(Umin, Utemp);
                        Umax = max(Umax, Utemp);
                    }
                }
            }
            Ucurr = C[0][idx] * sqrt(2.);
            if(!(Umin - 1e-10 < Ucurr && Ucurr < Umax + 1e-10) )
                printf("\n \n LOCAL MAXIMUM PRINCIPLE IS VIOLATED on cell %i, (%.10e, %.10e, %.10e) on timestep %i\n\n", idx, Umin, Ucurr, Umax, timestep);
            // else
            //     printf("LOCAL MAXIMUM PRINCIPLE IS NOT VIOLATED on cell %i, (%lf, %lf, %lf)\n", idx, Umin, Ucurr, Umax);
        }
    }
}

void check_lmp(double **C, double **C_prev, int timestep, int num_elem){
    check<<<get_blocks(num_elem, num_threads),num_threads>>>(C, C_prev,
                                                             d_neighbours,timestep,
                                                             num_elem); 
}
__global__ void modal_limiter(double **C,
                        int *kp11, int *kp12,
                        int *km11, int *km12,
                        double *betap11, double *betap12,
                        double *betam11, double *betam12,
                        int *kp21, int *kp22,
                        int *km21, int *km22,
                        double *betap21, double *betap22,
                        double *betam21, double *betam22,
                        double *V1x, double *V1y,
                        double *V2x, double *V2y,
                        double *V3x, double *V3y,
                        double *d1p, double *d1m,
                        double *d2p, double *d2m,
                        double *gamma_1p, double *gamma_1m,
                        double *gamma_2p, double *gamma_2m,
                        int N, int n_p,
                        int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    double r1, r2;
    double c1p, c1m, c2p, c2m, ci;
    double sigma1, sigma2;
    double l1, l2;
    int pidx11, pidx12, midx11, midx12;
    int pidx21, pidx22, midx21, midx22;
    double x1,x2,y1,y2, ltilde, l3;
    double beta1, beta2, beta3, beta4;
    double factor1, factor2, factor3, factor4;
    // double factor = 2.-sqrt(2.);  
    // double factor = 0.7;
    if (idx < num_elem) { 
        // reconstruct the solutions at the pm positions - c1
        pidx11 = kp11[idx];
        pidx12 = kp12[idx];
        midx11 = km11[idx];
        midx12 = km12[idx]; 
        // reconstruct the solutions at the pm positions - c2
        pidx21 = kp21[idx]; 
        pidx22 = kp22[idx];
        midx21 = km21[idx]; 
        midx22 = km22[idx]; 

        beta1 = betap11[idx];
        beta2 = betam11[idx];
        beta3 = betap21[idx];
        beta4 = betam21[idx];
        for (int n = 0; n < N; n++) {
            ci = C[n*n_p + 0][idx];
            if(pidx11 > -1){
              c1p = beta1 * C[n*n_p + 0][pidx11] + (1. - beta1) * C[n*n_p + 0][pidx12];
              C[n*n_p + 1][idx] = minmod(C[n*n_p + 1][idx], (c1p - ci)/sqrt(2.) );
            }
            if(midx11 > -1){
              c1m = beta2 * C[n*n_p + 0][midx11] + (1. - beta2) * C[n*n_p + 0][midx12];
              C[n*n_p + 1][idx] = minmod(C[n*n_p + 1][idx], (ci - c1m)/sqrt(2.) );
            } 

            if(pidx21 > -1){
              c2p = beta3 * C[n*n_p + 0][pidx21] + (1. - beta3)* C[n*n_p + 0][pidx22];
              C[n*n_p + 2][idx] = minmod(C[n*n_p + 2][idx], (c2p - ci) /sqrt(6.) );
            }
            if(midx21 > -1){
              c2m = beta4 * C[n*n_p + 0][midx21] + (1. - beta4)* C[n*n_p + 0][midx22];
              C[n*n_p + 2][idx] = minmod(C[n*n_p + 2][idx], (ci - c2m) /sqrt(6.) );
            }

            // if(pidx11 == -1 || midx11 == -1 || pidx21 == -1 || midx21 == -1){
            //   C[n*n_p + 1][idx] *= 0.;
            //   C[n*n_p + 2][idx] *= 0.;
            // }

        }
        
    } 
}


__global__ void moment_am(double **C,
                        int *kp11, int *kp12,
                        int *km11, int *km12,
                        double *betap11, double *betap12,
                        double *betam11, double *betam12,
                        int *kp21, int *kp22,
                        int *km21, int *km22,
                        double *betap21, double *betap22,
                        double *betam21, double *betam22,
                        double *V1x, double *V1y,
                        double *V2x, double *V2y,
                        double *V3x, double *V3y,
                        double *d1p, double *d1m,
                        double *d2p, double *d2m,
                        double *gamma_1p, double *gamma_1m,
                        double *gamma_2p, double *gamma_2m,
                        int N, int n_p,
                        int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    double r1, r2;
    double c1p, c1m, c2p, c2m, ci;
    double sigma1, sigma2;
    double l1, l2;
    int pidx11, pidx12, midx11, midx12;
    int pidx21, pidx22, midx21, midx22;
    double x1,x2,y1,y2, ltilde, l3;
    double beta1, beta2, beta3, beta4;
    double factor1, factor2, factor3, factor4;
    double rhou0, rhou1, rhou2, rhov0, rhov1, rhov2, n1x, n1y, n2x, n2y, mag;
    double c0_1, c0_2, c1_1n, c1_2n, rhou0_1p, rhou0_2p, rhov0_1p, rhov0_2p,rhou0_1m, rhou0_2m, rhov0_1m, rhov0_2m, c0_1p, c0_1m, c0_2p, c0_2m, c1_1t, c1_2t;
    double c1_1t_after,c1_2t_after, c1_1t_before,c1_2t_before;
    // double factor = 2.-sqrt(2.);  
    // double factor = 0.7;
    if (idx < num_elem) { 
        // reconstruct the solutions at the pm positions - c1
        pidx11 = kp11[idx];
        pidx12 = kp12[idx];
        midx11 = km11[idx];
        midx12 = km12[idx]; 
        // reconstruct the solutions at the pm positions - c2
        pidx21 = kp21[idx]; 
        pidx22 = kp22[idx];
        midx21 = km21[idx]; 
        midx22 = km22[idx]; 

        beta1 = betap11[idx];
        beta2 = betam11[idx];
        beta3 = betap21[idx];
        beta4 = betam21[idx];

        rhou0 = C[1*n_p + 0][idx];
        rhou1 = C[1*n_p + 1][idx];
        rhou2 = C[1*n_p + 2][idx];

        rhov0 = C[2*n_p + 0][idx];
        rhov1 = C[2*n_p + 1][idx];
        rhov2 = C[2*n_p + 2][idx];

        n1x =  V2x[idx] - 0.5*(V1x[idx] + V3x[idx]);
        n1y =  V2y[idx] - 0.5*(V1y[idx] + V3y[idx]);
        mag = sqrt(n1x*n1x + n1y*n1y);
        n1x/=mag;
        n1y/=mag;

        n2x = V3x[idx] - V1x[idx];
        n2y = V3y[idx] - V1y[idx];
        mag = sqrt(n2x*n2x + n2y*n2y);
        n2x/=mag;
        n2y/=mag;

        c1_1t_before = rhou1*(-n1y) + rhov1*n1x;
        c1_2t_before = rhou2*(-n2y) + rhov2*n2x;

        for (int n = 0; n < N; n++) {
            ci = C[n*n_p + 0][idx];
            if(pidx11 > -1){
              c1p = beta1 * C[n*n_p + 0][pidx11] + (1. - beta1) * C[n*n_p + 0][pidx12];
              C[n*n_p + 1][idx] = minmod(C[n*n_p + 1][idx], (c1p - ci)/sqrt(2.) );
            }
            if(midx11 > -1){
              c1m = beta2 * C[n*n_p + 0][midx11] + (1. - beta2) * C[n*n_p + 0][midx12];
              C[n*n_p + 1][idx] = minmod(C[n*n_p + 1][idx], (ci - c1m)/sqrt(2.) );
            } 

            if(pidx21 > -1){
              c2p = beta3 * C[n*n_p + 0][pidx21] + (1. - beta3)* C[n*n_p + 0][pidx22];
              C[n*n_p + 2][idx] = minmod(C[n*n_p + 2][idx], (c2p - ci) /sqrt(6.) );
            }
            if(midx21 > -1){
              c2m = beta4 * C[n*n_p + 0][midx21] + (1. - beta4)* C[n*n_p + 0][midx22];
              C[n*n_p + 2][idx] = minmod(C[n*n_p + 2][idx], (ci - c2m) /sqrt(6.) );
            }
	    }
	    if(pidx11 > -1 && midx11 > -1 && pidx21 > -1 && midx21 > -1){ 
	        c1_1t_after = C[1*n_p + 1][idx]*(-n1y) + C[2*n_p + 1][idx]*n1x;
	        c1_2t_after = C[1*n_p + 2][idx]*(-n2y) + C[2*n_p + 2][idx]*n2x;

	        C[1*n_p + 1][idx] += (c1_1t_before-c1_1t_after)*(-n1y);
	        C[2*n_p + 1][idx] += (c1_1t_before-c1_1t_after)* n1x;

	        C[1*n_p + 2][idx] += (c1_2t_before-c1_2t_after)*(-n2y);
	        C[2*n_p + 2][idx] += (c1_2t_before-c1_2t_after)* n2x;
		}
			// c1_1t_after = C[1*n_p + 1][idx]*(-n1y) + C[2*n_p + 1][idx]*n1x;
			// c1_2t_after = C[1*n_p + 2][idx]*(-n2y) + C[2*n_p + 2][idx]*n2x;
	  //       printf("%lf %lf\n", c1_2t_before, c1_2t_after);

	    // }
        
    } 
}


	            // rhou0 = C[1*n_p + 0][idx];
	            // rhou1 = C[1*n_p + 1][idx];
	            // rhou2 = C[1*n_p + 2][idx];

	            // rhov0 = C[2*n_p + 0][idx];
	            // rhov1 = C[2*n_p + 1][idx];
	            // rhov2 = C[2*n_p + 2][idx];

	            // n1x =  V2x[idx] - 0.5*(V1x[idx] + V3x[idx]);
	            // n1y =  V2y[idx] - 0.5*(V1y[idx] + V3y[idx]);
	            // mag = sqrt(n1x*n1x + n1y*n1y);
	            // n1x/=mag;
	            // n1y/=mag;

	            // n2x = V3x[idx] - V1x[idx];
	            // n2y = V3y[idx] - V1y[idx];
	            // mag = sqrt(n2x*n2x + n2y*n2y);
	            // n2x/=mag;
	            // n2y/=mag;

	            // // printf("%lf %lf, %lf %lf\n", n1x, n1y, n2x, n2y);

	            // c0_1 = rhou0*n1x + rhov0*n1y;
	            // c0_2 = rhou0*n2x + rhov0*n2y;

	            // c1_1n = rhou1*n1x    + rhov1*n1y; 
	            // c1_2n = rhou2*n2x    + rhov2*n2y;

	            // c1_1t = rhou1*(-n1y) + rhov1*n1x;
	            // c1_2t = rhou2*(-n2y) + rhov2*n2x;
	            // // if(pidx11 > -1){
	            // //   rhou0_1p = beta1 * C[1*n_p + 0][pidx11] + (1. - beta1) * C[1*n_p + 0][pidx12];
	            // //   rhov0_1p = beta1 * C[2*n_p + 0][pidx11] + (1. - beta1) * C[2*n_p + 0][pidx12];
	            // //   c0_1p = rhou0_1p*n1x + rhov0_1p*n1y;
	            // //   c1_1n = minmod(c1_1n, (c0_1p - c0_1)/sqrt(2.) );
	            // // }
	            // // if(midx11 > -1){
	            // //   rhou0_1m = beta2 * C[1*n_p + 0][midx11] + (1. - beta2) * C[1*n_p + 0][midx12];
	            // //   rhov0_1m = beta2 * C[2*n_p + 0][midx11] + (1. - beta2) * C[2*n_p + 0][midx12];
	            // //   c0_1m = rhou0_1m*n1x + rhov0_1m*n1y;
	            // //   c1_1n = minmod(c1_1n, (c0_1 - c0_1m)/sqrt(2.) );
	            // // } 
	            // // if(pidx21 > -1){
	            // //   rhou0_2p = beta3 * C[1*n_p + 0][pidx21] + (1. - beta3)* C[1*n_p + 0][pidx22];
	            // //   rhov0_2p = beta3 * C[2*n_p + 0][pidx21] + (1. - beta3)* C[2*n_p + 0][pidx22];
	            // //   c0_2p = rhou0_2p*n2x + rhov0_2p*n2y;
             // //  	  c1_2n = minmod(c1_2n, (c0_2p - c0_2) /sqrt(6.) );
	            // // }
	            // // if(midx21 > -1){
	            // //   rhou0_2m = beta4 * C[1*n_p + 0][midx21] + (1. - beta4)* C[1*n_p + 0][midx22];
	            // //   rhov0_2m = beta4 * C[2*n_p + 0][midx21] + (1. - beta4)* C[2*n_p + 0][midx22];
	            // //   c0_2m = rhou0_2m*n2x + rhov0_2m*n2y;
	            // //   c1_2n = minmod(c1_2n, (c0_2 - c0_2m) /sqrt(6.) );
	            // // }

	            // // printf("%lf %lf before %lf after %lf\n", rhou1*n1x    + rhov1*n1y, c1_1n, C[1*n_p + 1][idx], c1_1t * (-n1y) + c1_1n * n1x);
	            // // if(abs(c1_1t * n1x    + c1_1n * n1y) +1e-5> abs(C[2*n_p + 1][idx]))
	            // // 	printf("%lf %lf before %lf after %lf\n", rhou2*n2x    + rhov2*n2y, c1_2n, C[2*n_p + 1][idx], c1_1t * n1x    + c1_1n * n1y);

	            // // printf("before %lf after %lf\n", C[1*n_p + 2][idx], c1_2t * (-n2y) + c1_2n * n2x);
	            // // printf("before %lf after %lf\n", C[2*n_p + 2][idx], c1_2t * n1x    + c1_2n * n1y);
	            // C[1*n_p + 1][idx] = c1_1t * (-n1y) + c1_1n * n1x;
	            // C[2*n_p + 1][idx] = c1_1t * n1x    + c1_1n * n1y;

	            // C[1*n_p + 2][idx] = c1_2t * (-n2y) + c1_2n * n2x;
	            // C[2*n_p + 2][idx] = c1_2t * n2x    + c1_2n * n2y;
// __global__ void modal_limiter(double **C,
// 						int *kp11, int *kp12,
// 						int *km11, int *km12,
// 						double *betap11, double *betap12,
// 						double *betam11, double *betam12,
// 						int *kp21, int *kp22,
// 						int *km21, int *km22,
// 						double *betap21, double *betap22,
//                         double *betam21, double *betam22,
//                         double *V1x, double *V1y,
//                         double *V2x, double *V2y,
//                         double *V3x, double *V3y,
//                         double *d1p, double *d1m,
//                         double *d2p, double *d2m,
//                         double *gamma_1p, double *gamma_1m,
//                         double *gamma_2p, double *gamma_2m,
//                         int N, int n_p,
// 						int num_elem) {

//     int idx = blockIdx.x * blockDim.x + threadIdx.x;
//     double r1, r2;
//     double c1p, c1m, c2p, c2m, ci;
//     double sigma1, sigma2;
//     double l1, l2;
//     int pidx11, pidx12, midx11, midx12;
//     int pidx21, pidx22, midx21, midx22;
//     double x1,x2,y1,y2, ltilde, l3;
//     double factor = 1.;
//     // double factor = 2.-sqrt(2.);  
//     // double factor = 0.7;
//     if (idx < num_elem) { 
//         // reconstruct the solutions at the pm positions - c1
//         pidx11 = kp11[idx];
//         pidx12 = kp12[idx];
//         midx11 = km11[idx];
//         midx12 = km12[idx]; 
//         // reconstruct the solutions at the pm positions - c2
//         pidx21 = kp21[idx]; 
//         pidx22 = kp22[idx];
//         midx21 = km21[idx]; 
//         midx22 = km22[idx]; 
//         for (int n = 0; n < N; n++) {
// 	    	ci = C[n*n_p + 0][idx];
//             if(pidx11 > -1){
// 	    	  c1p = betap11[idx] * C[n*n_p + 0][pidx11] + betap12[idx] * C[n*n_p + 0][pidx12];
//               C[n*n_p + 1][idx] = minmod(C[n*n_p + 1][idx], (c1p - ci)/sqrt(2.)*factor);
//             }
//             if(midx11 > -1){
// 	    	  c1m = betam11[idx] * C[n*n_p + 0][midx11] + betam12[idx] * C[n*n_p + 0][midx12];
//               C[n*n_p + 1][idx] = minmod(C[n*n_p + 1][idx], (ci - c1m)/sqrt(2.)*factor);
//             }

//             if(pidx21 > -1){
// 	    	  c2p = betap21[idx] * C[n*n_p + 0][pidx21] + betap22[idx] * C[n*n_p + 0][pidx22];
//               C[n*n_p + 2][idx] = minmod(C[n*n_p + 2][idx], (c2p - ci) /sqrt(6.)*factor);
//             }
//             if(midx21 > -1){
// 	    	  c2m = betam21[idx] * C[n*n_p + 0][midx21] + betam22[idx] * C[n*n_p + 0][midx22];
//               C[n*n_p + 2][idx] = minmod(C[n*n_p + 2][idx], (ci - c2m) /sqrt(6.)*factor);
//             }
//         }
    	
//     } 
// }

// __global__ void modal_limiter(double **C,
//                         int *kp11, int *kp12,
//                         int *km11, int *km12,
//                         double *betap11, double *betap12,
//                         double *betam11, double *betam12,
//                         int *kp21, int *kp22,
//                         int *km21, int *km22,
//                         double *betap21, double *betap22,
//                         double *betam21, double *betam22,
//                         double *V1x, double *V1y,
//                         double *V2x, double *V2y,
//                         double *V3x, double *V3y,
//                         double *d1p, double *d1m,
//                         double *d2p, double *d2m,
//                         double *gamma_1p, double *gamma_1m,
//                         double *gamma_2p, double *gamma_2m,
//                         int num_elem) {

//     int idx = blockIdx.x * blockDim.x + threadIdx.x;
//     double r1, r2;
//     double c1p, c1m, c2p, c2m, ci;
//     double sigma1, sigma2;
//     double l1, l2;
//     int pidx1, pidx2, midx1, midx2;
//     double x1,x2,y1,y2, ltilde, l3;

//     if (idx < num_elem) { 
//         if(kp11[idx] > -1){
//             ci = C[0][idx];

//             // reconstruct the solutions at the pm positions - c1
//             pidx1 = kp11[idx];
//             pidx2 = kp12[idx];
//             midx1 = km11[idx];
//             midx2 = km12[idx];
//             c1p = betap11[idx] * C[0][pidx1] + betap12[idx] * C[0][pidx2];
//             c1m = betam11[idx] * C[0][midx1] + betam12[idx] * C[0][midx2];

//             // reconstruct the solutions at the pm positions - c2
//             pidx1 = kp21[idx];
//             pidx2 = kp22[idx];
//             midx1 = km21[idx]; 
//             midx2 = km22[idx]; 
//             c2p = betap21[idx] * C[0][pidx1] + betap22[idx] * C[0][pidx2];
//             c2m = betam21[idx] * C[0][midx1] + betam22[idx] * C[0][midx2];

//             C[1][idx] = minmod(C[1][idx], (c1p - ci)/sqrt(2.), (ci - c1m)/sqrt(2.));
//             C[2][idx] = minmod(C[2][idx], (c2p - ci) /sqrtf(6), (ci - c2m) /sqrtf(6));
//             // C[1][idx] = 0.*minmod(C[1][idx], (c1p - ci)/sqrt(2.), (ci - c1m)/sqrt(2.));
//       //       C[2][idx] = minmod(C[2][idx], sqrt(2.)*(c2p - ci) /sqrtf(3), sqrt(2.)*(ci - c2m) /sqrtf(3));
//         }
//     } 
// }
            // C[1][idx] = 0.*minmod(C[1][idx], (c1p - ci)/sqrt(2.), (ci - c1m)/sqrt(2.));
      //       C[2][idx] = minmod(C[2][idx], sqrt(2.)*(c2p - ci) /sqrtf(3), sqrt(2.)*(ci - c2m) /sqrtf(3));


            // if(fabs(C[2][idx]-minmod(C[2][idx], (c2p - ci) /sqrtf(6), (ci - c2m) /sqrtf(6))) >1e-7 ) 
            //  printf("%i %lf %lf %lf %lf\n", idx, (c2p-ci)/sqrtf(6), (ci-c2m)/sqrtf(6), C[2][idx],  minmod(C[2][idx], (c2p - ci)/sqrtf(6), (ci - c2m)/sqrtf(6)));
            // if(C[1][idx] < minmod(C[1][idx], (c1p - ci)/sqrt(2.), (ci - c1m)/sqrt(2.)) )
            //     printf("%i 1: %lf, %lf\n", idx, C[1][idx], minmod(C[1][idx], (c1p - ci)/sqrt(2.), (ci - c1m)/sqrt(2.)));
            // if(C[2][idx] < minmod(C[2][idx], (c2p - ci) /sqrtf(6), (ci - c2m) /sqrtf(6)) )
            //     printf("%i 2: %lf, %lf\n", idx, C[2][idx],minmod(C[2][idx], (c2p - ci) /sqrtf(6), (ci - c2m) /sqrtf(6)));
            // if(idx == 255) {
            //     x1 = V2x[idx];
            //     y1 = V2y[idx];
            //     x2 = (V1x[idx] + V3x[idx])*0.5;
            //     y2 = (V1y[idx] + V3y[idx])*0.5;
            //     ltilde = sqrtf(  (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2)   );

            //     x1 = V1x[idx];
            //     y1 = V1y[idx];
            //     x2 = V3x[idx];
            //     y2 = V3y[idx];
            //     l3 = sqrtf(  (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2)   );       
            //     printf("%i %lf %lf %lf\n   %lf %lf %lf\n\n", idx, C[1][idx] * 6./ltilde, sqrt(2.)*(c1p-ci)/d1p[idx], sqrt(2.)*(ci-c1m)/d1m[idx],
            //                                                        C[2][idx] * 4*sqrtf(3)/l3, sqrt(2.)*(c2p-ci)/d2p[idx], sqrt(2.)*(ci-c2m)/d2m[idx]);

            //     // printf("%i 1: %lf, %lf\n", idx, C[1][idx], minmod(C[1][idx], (c1p - ci)/sqrt(2.), (ci - c1m)/sqrt(2.)));
            //     // printf("%i 2: %lf, %lf, %lf\n", idx, C[2][idx], (c2p - ci)/sqrtf(6), (ci - c2m)/sqrtf(6));
            //     printf("%lf, %lf\n %lf, %lf\n", 3*d1p[idx]/ltilde, 3*d1m[idx]/ltilde, gamma_1p[idx], gamma_1m[idx]);
            //     printf("%lf, %lf\n %lf, %lf\n", 2*d2p[idx]/l3, 2*d2m[idx]/l3, gamma_2p[idx], gamma_2m[idx]);
            // }
            // printf("nothing is getting limited???");



// void limit(double **C,
// 		   int num_elem){
// 	int num_threads = 512;
// 			limit<<<get_blocks(num_elem, num_threads),num_threads>>>(C,
// 																 d_kp11, d_kp12,
// 																 d_km11, d_km12,
// 																 d_betap11, d_betap12,
// 																 d_betam11, d_betam12,
// 																 d_kp21, d_kp22,
// 																 d_km21, d_km22,
// 																 d_betap21, d_betap22,
// 																 d_betam21, d_betam22,
//                                                                  d_d1p, d_d1m,
// 																 num_elem); 

// }

 
            // if(fabs(C[2][idx]-minmod(C[2][idx], (c2p - ci) /sqrtf(6), (ci - c2m) /sqrtf(6))) >1e-7 ) 
            //  printf("%i %lf %lf %lf %lf\n", idx, (c2p-ci)/sqrtf(6), (ci-c2m)/sqrtf(6), C[2][idx],  minmod(C[2][idx], (c2p - ci)/sqrtf(6), (ci - c2m)/sqrtf(6)));
            // x1 = V2x[idx];
            // y1 = V2y[idx];
            // x2 = (V1x[idx] + V3x[idx])*0.5;
            // y2 = (V1y[idx] + V3y[idx])*0.5;
            // ltilde = sqrtf(  (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2)   );

            // x1 = V1x[idx];
            // y1 = V1y[idx];
            // x2 = V3x[idx];
            // y2 = V3y[idx];
            // l3 = sqrtf(  (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2)   );       
            // printf("%i %lf %lf %lf\n   %lf %lf %lf\n\n", idx, C[1][idx] * 6./ltilde, sqrt(2.)*(c1p-ci)/d1p[idx], sqrt(2.)*(ci-c1m)/d1m[idx],
            //                                                   C[2][idx] * 4*sqrtf(3)/l3, sqrt(2.)*(c2p-ci)/d2p[idx], sqrt(2.)*(ci-c2m)/d2m[idx]);

// __global__ void write_activated(int *activated,
//                           double **C,
//                           int *kp11, int *kp12,
//                           int *km11, int *km12,
//                           double *betap11, double *betap12,
//                           double *betam11, double *betam12,
//                           int *kp21, int *kp22,
//                           int *km21, int *km22,
//                           double *betap21, double *betap22,
//                           double *betam21, double *betam22,
//                           double *V1x, double *V1y,
//                           double *V2x, double *V2y,
//                           double *V3x, double *V3y,
//                           double *d1p, double *d1m,
//                           double *d2p, double *d2m,
//                           double *gamma_1p, double *gamma_1m,
//                           double *gamma_2p, double *gamma_2m,
//                           int N, int n_p,
//                           int num_elem) {

//     int idx = blockIdx.x * blockDim.x + threadIdx.x;
//     double r1, r2;
//     double c1p, c1m, c2p, c2m, ci;
//     double sigma1, sigma2;
//     double l1, l2;
//     int pidx11, pidx12, midx11, midx12;
//     int pidx21, pidx22, midx21, midx22;
//     double x1,x2,y1,y2, ltilde, l3;
//     double factor = 1.;
//     double beta1, beta2, beta3, beta4;
//     double TOL = 1e-8;
//     // double factor = 2.-sqrt(2.);  
//     // double factor = 0.7;
//     if (idx < num_elem) { 
//         // reconstruct the solutions at the pm positions - c1
//         pidx11 = kp11[idx];
//         pidx12 = kp12[idx];
//         midx11 = km11[idx];
//         midx12 = km12[idx]; 
//         // reconstruct the solutions at the pm positions - c2
//         pidx21 = kp21[idx]; 
//         pidx22 = kp22[idx];
//         midx21 = km21[idx]; 
//         midx22 = km22[idx]; 

//         beta1 = betap11[idx];
//         beta2 = betam11[idx];
//         beta3 = betap21[idx];
//         beta4 = betam21[idx];
//         activated[idx] = 0;


//         for (int n = 0; n < 1; n++) {
//             ci = C[n*n_p + 0][idx];
//             if(pidx11 > -1){
//               c1p = beta1 * C[n*n_p + 0][pidx11] + (1. - beta1) * C[n*n_p + 0][pidx12];
//               if( (fabs(C[n*n_p + 1][idx]) > TOL) && (fabs((c1p - ci)/sqrt(2.)) > TOL)   ){
//                   if (C[n*n_p + 1][idx]*(c1p - ci)/sqrt(2.) > 0)
//                     activated[idx] = (fabs(C[n*n_p + 1][idx]) > fabs((c1p - ci)/sqrt(2.))) ? 1 : activated[idx];
//                   else
//                     activated[idx] = 1;
//                 }

//             }
//             if(midx11 > -1){
//               c1m = beta2 * C[n*n_p + 0][midx11] + (1. - beta2) * C[n*n_p + 0][midx12];
//               if(    (fabs(C[n*n_p + 1][idx]) > TOL) && (fabs((ci - c1m)/sqrt(2.)) > TOL)    ){
//                   if (C[n*n_p + 1][idx]*(ci - c1m)/sqrt(2.) > 0)
//                     activated[idx] = (fabs(C[n*n_p + 1][idx]) > fabs((ci - c1m)/sqrt(2.))) ? 1 : activated[idx];
//                   else
//                     activated[idx] = 1;
//               }
//             }

//             if(pidx21 > -1){
//               c2p = beta3 * C[n*n_p + 0][pidx21] + (1. - beta3)* C[n*n_p + 0][pidx22];
//               if(    (fabs(C[n*n_p + 2][idx]) > TOL) && (fabs((c2p - ci) /sqrt(6.)) > TOL)     ){
//                   if (C[n*n_p + 2][idx] * (c2p - ci) /sqrt(6.) > 0)
//                     activated[idx] = (fabs(C[n*n_p + 2][idx]) > fabs((c2p - ci) /sqrt(6.))) ? 1 : activated[idx];
//                   else
//                     activated[idx] = 1;
//               }


//             }
//             if(midx21 > -1){
//               c2m = beta4 * C[n*n_p + 0][midx21] + (1. - beta4)* C[n*n_p + 0][midx22];
//               if(     (fabs(C[n*n_p + 2][idx]) > TOL) && (fabs((ci - c2m) /sqrt(6.)) > TOL)      ){
//                   if (C[n*n_p + 2][idx] * (ci - c2m) /sqrt(6.) > 0)
//                     activated[idx] = (fabs(C[n*n_p + 2][idx]) > fabs((ci - c2m) /sqrt(6.)) ) ? 1 : activated[idx];
//                   else
//                     activated[idx] = 1;
//             }


//             }
//         }

//         // if(activated[idx])
//         //     printf("\n \n %e %e %e \n %e %e %e \n\n", (ci - c1m)/sqrt(2.), C[0*n_p + 1][idx], (c1p - ci)/sqrt(2.),(ci - c2m) /sqrt(6.), C[0*n_p + 2][idx], (c2p - ci) /sqrt(6.));
        




//     } 
// }


__global__ void write_activated(int *activated, double *value1,double *value2,
                          double **C,
                          int *kp11, int *kp12,
                          int *km11, int *km12,
                          double *betap11, double *betap12,
                          double *betam11, double *betam12,
                          int *kp21, int *kp22,
                          int *km21, int *km22,
                          double *betap21, double *betap22,
                          double *betam21, double *betam22,
                          double *V1x, double *V1y,
                          double *V2x, double *V2y,
                          double *V3x, double *V3y,
                          double *d1p, double *d1m,
                          double *d2p, double *d2m,
                          double *gamma_1p, double *gamma_1m,
                          double *gamma_2p, double *gamma_2m,
                          int N, int n_p,
                          int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    double r1, r2;
    double c1p, c1m, c2p, c2m, ci;
    double sigma1, sigma2;
    double l1, l2;
    int pidx11, pidx12, midx11, midx12;
    int pidx21, pidx22, midx21, midx22;
    double x1,x2,y1,y2, ltilde, l3;
    double factor = 1.;
    double beta1, beta2, beta3, beta4;
    double TOL = 1e-5;
    // double factor = 2.-sqrt(2.);  
    // double factor = 0.7;
    if (idx < num_elem) { 
        // reconstruct the solutions at the pm positions - c1
        pidx11 = kp11[idx];
        pidx12 = kp12[idx];
        midx11 = km11[idx];
        midx12 = km12[idx]; 
        // reconstruct the solutions at the pm positions - c2
        pidx21 = kp21[idx]; 
        pidx22 = kp22[idx];
        midx21 = km21[idx]; 
        midx22 = km22[idx]; 

        beta1 = betap11[idx];
        beta2 = betam11[idx];
        beta3 = betap21[idx];
        beta4 = betam21[idx];
        activated[idx] = 1;
        value1[idx] = 1.;
        value2[idx] = 1.;


        for (int n = 0; n < 1; n++) {
            ci = C[n*n_p + 0][idx];
            if(pidx11 > -1){
              c1p = beta1 * C[n*n_p + 0][pidx11] + (1. - beta1) * C[n*n_p + 0][pidx12];
              if( (fabs(C[n*n_p + 1][idx]) > TOL) && (fabs((c1p - ci)/sqrt(2.)) > TOL)   ){
                  if (C[n*n_p + 1][idx]*(c1p - ci)/sqrt(2.) > 0){
                    activated[idx] = (fabs(C[n*n_p + 1][idx]) > fabs((c1p - ci)/sqrt(2.))) ? 0 : activated[idx];
                    value1[idx] = (fabs(C[n*n_p + 1][idx]) > fabs((c1p - ci)/sqrt(2.))) ? fmin(fabs((c1p - ci)/sqrt(2.)) / fabs(C[n*n_p + 1][idx]), value1[idx]) : 1.;
                    }
                  else{
                    activated[idx] = 0;
                    value1[idx] = 0.;
                    }
                }

            }
            if(midx11 > -1){
              c1m = beta2 * C[n*n_p + 0][midx11] + (1. - beta2) * C[n*n_p + 0][midx12];
              if(    (fabs(C[n*n_p + 1][idx]) > TOL) && (fabs((ci - c1m)/sqrt(2.)) > TOL)    ){
                  if (C[n*n_p + 1][idx]*(ci - c1m)/sqrt(2.) > 0){
                    activated[idx] = (fabs(C[n*n_p + 1][idx]) > fabs((ci - c1m)/sqrt(2.))) ? 0 : activated[idx];
                    value1[idx] = fabs(C[n*n_p + 1][idx]) > fabs((ci - c1m)/sqrt(2.)) ? fmin(fabs((ci - c1m)/sqrt(2.)) / fabs(C[n*n_p + 1][idx]), value1[idx]) : value1[idx];
                    }
                  else{
                    activated[idx] = 0;
                    value1[idx] = 0.;
                    }
              }
            }


            if(pidx21 > -1){
              c2p = beta3 * C[n*n_p + 0][pidx21] + (1. - beta3)* C[n*n_p + 0][pidx22];
              if(    (fabs(C[n*n_p + 2][idx]) > TOL) && (fabs((c2p - ci) /sqrt(6.)) > TOL)     ){
                  if (C[n*n_p + 2][idx] * (c2p - ci) /sqrt(6.) > 0){
                    activated[idx] = (fabs(C[n*n_p + 2][idx]) > fabs((c2p - ci) /sqrt(6.))) ? 0 : activated[idx];
                    value2[idx] = (fabs(C[n*n_p + 2][idx]) > fabs((c2p - ci) /sqrt(6.))) ? fmin(fabs((c2p - ci) /sqrt(6.)) / fabs(C[n*n_p + 2][idx]), value2[idx]) : value2[idx];
                    }
                  else{
                    activated[idx] = 0;
                    value2[idx] = 0.;
                    }
              }


            }
            if(midx21 > -1){
              c2m = beta4 * C[n*n_p + 0][midx21] + (1. - beta4)* C[n*n_p + 0][midx22];
              if(     (fabs(C[n*n_p + 2][idx]) > TOL) && (fabs((ci - c2m) /sqrt(6.)) > TOL)      ){
                  if (C[n*n_p + 2][idx] * (ci - c2m) /sqrt(6.) > 0){
                    activated[idx] = (fabs(C[n*n_p + 2][idx]) > fabs(2*(ci - c2m) /sqrt(6.)) ) ? 0 : activated[idx];
                    value2[idx] = (fabs(C[n*n_p + 2][idx]) > fabs((ci - c2m) /sqrt(6.)) ) ? fmax(1.-fabs((ci - c2m) /sqrt(6.)) / fabs(C[n*n_p + 2][idx]), value2[idx]) :value2[idx];
                 } 
                 else{
                    activated[idx] = 0;
                    value2[idx] = 0.;
                }
            }
            }
        }

        // if(activated[idx])
        //     printf("\n \n %e %e %e \n %e %e %e \n\n", (ci - c1m)/sqrt(2.), C[0*n_p + 1][idx], (c1p - ci)/sqrt(2.),(ci - c2m) /sqrt(6.), C[0*n_p + 2][idx], (c2p - ci) /sqrt(6.));
        




    } 
}

__global__ void write_activated(int *activated,
                     double **C,
                     double *basis_side, double *basis_midpoint,
                     int **d_neighbours,
                     int N, int n_p, int n_quad1d,
                     int quad,
                     int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int count, neigh;
    double U_i, U_c, Umin, Umax, alpha, min_alpha;
    double C_i[NP_MAX], C_neigh[20];
    double y;

    if (idx < num_elem) { 
        activated[idx] = 1.;
        if(d_neighbours[0][idx] > -1){
            for (int n = 0; n < N; n++) {
                // load data
                for (int i = 0; i < n_p; i++) 
                    C_i[i] =  C[n*n_p + i][idx];

                count = 0;
                neigh = d_neighbours[0][idx];
                while(neigh>-1){ 
                    C_neigh[count] =  C[n*n_p + 0][neigh];
                    count ++;
                    neigh = d_neighbours[count][idx];
                }

                U_c = C_i[0] * sqrt(2.);
                Umin = U_c;
                Umax = U_c;

                // get delmin and delmax
                for (int i = 0; i < count; i++) {
                    U_i = C_neigh[i] * sqrt(2.);
                    
                    Umin = (U_i < Umin) ? U_i : Umin;
                    Umax = (U_i > Umax) ? U_i : Umax;
                }



                min_alpha = 1.;
                // at the midpoints along the boundaries
                for (int side = 0; side < 3; side++) {
                    if(quad){
                        for(int j=0; j < n_quad1d; j++) {
                            U_i = 0.;
                            //evaluate U
                            for (int i = 0; i < n_p; i++) {
                                U_i += C_i[i] * basis_side[ side  * (n_quad1d * n_p) + i * n_quad1d + j];
                            }

                            y = 1.;
                            // evaluate alpha
                            if (U_i > U_c) {
                                y = (Umax - U_c)/(U_i - U_c);
                            } else if (U_i < U_c) {
                                y = (Umin - U_c)/(U_i - U_c);
                            } 
                            alpha = fmin(1., y); // Barth Jesperson

                            if (alpha < min_alpha)  {
                                min_alpha = alpha;
                            }
                        }
                    }
                    else{ //midpoint
                        U_i = 0.;
                        //evaluate U
                        for (int i = 0; i < n_p; i++) {
                            U_i += C_i[i] * basis_midpoint[3 * i + side];
                        }

                        y = 1.;
                        alpha = 1.;
                        // evaluate alpha
                        if (U_i > U_c) {
                            y = (Umax - U_c)/(U_i - U_c);
                        } else if (U_i < U_c) {
                            y = (Umin - U_c)/(U_i - U_c);
                        } 
                        alpha = fmin(1., y); // Barth Jesperson
                            // if(idx == 722)
                            //     printf("y is %lf\n", y);
                        if (alpha < min_alpha)  {
                            min_alpha = alpha;
                        }
                    }
                }



                if (min_alpha < 0) {
                    min_alpha = 0.;
                }
                
                if(fabs(1.-min_alpha) > 1e-9 && fabs(C_i[1]) >1e-5 && fabs(C_i[2]) > 1e-5)
                    activated[idx] = 0.;
                else
                    activated[idx] = 1.;

            }
        }

        
    }
}


void write_activated(double **C, int num_elem){
    if(ACTIVATED)
        if(limiter_type == MODAL){
            write_activated<<<get_blocks(num_elem, num_threads),num_threads>>>(d_activated, d_value1, d_value2,
                                                                         C,
                                                                         d_kp11, d_kp12,
                                                                         d_km11, d_km12,
                                                                         d_betap11, d_betap12,
                                                                         d_betam11, d_betam12,
                                                                         d_kp21, d_kp22,
                                                                         d_km21, d_km22,
                                                                         d_betap21, d_betap22,
                                                                         d_betam21, d_betam22,
                                                                         vert1x, vert1y,
                                                                         vert2x, vert2y,
                                                                         vert3x, vert3y,
                                                                         d_d1p, d_d1m,
                                                                         d_d2p, d_d2m,
                                                                         d_gammap1, d_gammam1,
                                                                         d_gammap2, d_gammam2,
                                                                         N_eq, N_poly,
                                                                         num_elem); 
            write_limit(num_elem, 0, 0);
        }
        else if(limiter_type == BJ_N){
                write_activated<<<get_blocks(num_elem,num_threads), num_threads>>>(d_activated,
                                                                    C, 
                                                                   d_basis_side,d_midpoint,
                                                                   d_neighbours,
                                                                   N_eq, N_poly,n_quad1d, 0,
                                                                   num_elem);
            write_limit(num_elem, 0, 0);
        }
}
void limit(double **C, int *d_flag, int num_elem){
    barth_jesperson_m<<<get_blocks(num_elem,num_threads), num_threads>>>(C, d_flag,
                                                               d_midpoint, d_vertex,
                                                               s1, s2, s3,
                                                               spos, schild1, schild2,
                                                               le, re, 
                                                               lsn, rsn,
                                                               N_eq, N_poly,
                                                               num_elem);
}

void limit(double **C, int num_elem){

        if(limiter_type == MODAL)
			modal_limiter<<<get_blocks(num_elem, num_threads),num_threads>>>(C,
																 d_kp11, d_kp12,
																 d_km11, d_km12,
																 d_betap11, d_betap12,
																 d_betam11, d_betam12,
																 d_kp21, d_kp22,
																 d_km21, d_km22,
																 d_betap21, d_betap22,
																 d_betam21, d_betam22,
																 vert1x, vert1y,
																 vert2x, vert2y,
																 vert3x, vert3y,
                                                                 d_d1p, d_d1m,
                                                                 d_d2p, d_d2m,
                                                                 d_gammap1, d_gammam1,
                                                                 d_gammap2, d_gammam2,
                                                                 N_eq, N_poly,
																 num_elem); 
		else if(limiter_type == MOMENT_AM)
			moment_am<<<get_blocks(num_elem, num_threads),num_threads>>>(C,
													 d_kp11, d_kp12,
													 d_km11, d_km12,
													 d_betap11, d_betap12,
													 d_betam11, d_betam12,
													 d_kp21, d_kp22,
													 d_km21, d_km22,
													 d_betap21, d_betap22,
													 d_betam21, d_betam22,
													 vert1x, vert1y,
													 vert2x, vert2y,
													 vert3x, vert3y,
	                                                 d_d1p, d_d1m,
	                                                 d_d2p, d_d2m,
	                                                 d_gammap1, d_gammam1,
	                                                 d_gammap2, d_gammam2,
	                                                 N_eq, N_poly,
													 num_elem); 
        else if(limiter_type == COCKBURN){
           cockburn_limiter<<<get_blocks(num_elem,num_threads), num_threads>>>(C,
                                                                               d_circ,
                                                                               d_midpoint,
                                                                               d_e11, d_e12,
                                                                               d_e21, d_e22,
                                                                               d_e31, d_e32,
                                                                               d_a11, d_a12,
                                                                               d_a21, d_a22,
                                                                               d_a31, d_a32,                           
                                                                               N_eq, N_poly, n_quadrature,
                                                                               weights, basis_values,
                                                                               num_elem);
        }

        else if(limiter_type == BJ){
            barth_jesperson_m<<<get_blocks(num_elem,num_threads), num_threads>>>(C, 
                                                                                   d_midpoint, d_vertex,
                                                                                   s1, s2, s3,
                                                                                   spos, schild1, schild2,
                                                                                   le, re, 
                                                                                   lsn, rsn,
                                                                                   N_eq, N_poly,
                                                                                   num_elem);
        }
        else if(limiter_type == BJ_SECOND){
            barth_jesperson_m_second_order<<<get_blocks(num_elem,num_threads), num_threads>>>(C, 
                                                                                   d_midpoint, d_vertex,
                                                                                   s1, s2, s3,
                                                                                   spos, schild1, schild2,
                                                                                   le, re, 
                                                                                   lsn, rsn,
                                                                                   N_eq, N_poly,
                                                                                   num_elem);
        }
        else if(limiter_type == BJ_QUAD){
            barth_jesperson_q<<<get_blocks(num_elem,num_threads), num_threads>>>(C, d_basis_side,
                                                                                   s1, s2, s3,
                                                                                   spos, schild1, schild2,
                                                                                   le, re, 
                                                                                   lsn, rsn,
                                                                                   N_eq, N_poly, n_quad1d,
                                                                                   num_elem);
        }
        else if(limiter_type == BJ_N){
            bj_v<<<get_blocks(num_elem,num_threads), num_threads>>>(C, 
                                                                   d_basis_side,d_midpoint,
                                                                   d_neighbours,
                                                                   N_eq, N_poly,n_quad1d, 0,
                                                                   num_elem);
        }
        else if(limiter_type == BJ_QUAD_N){
            bj_v<<<get_blocks(num_elem,num_threads), num_threads>>>(C, 
                                                                   d_basis_side,d_midpoint,
                                                                   d_neighbours,
                                                                   N_eq, N_poly,n_quad1d, 1,
                                                                   num_elem);
        }
        else if(limiter_type == BJ_R){
            bj_r<<<get_blocks(num_elem,num_threads), num_threads>>>(C, 
                                                                   d_basis_side,d_midpoint,
                                                                   d_neighbours,
                                                                   N_eq, N_poly,n_quad1d, 0,
                                                                   num_elem);
        }
        else if(limiter_type == BJ_QUAD_R){
            bj_r<<<get_blocks(num_elem,num_threads), num_threads>>>(C, 
                                                                   d_basis_side,d_midpoint,
                                                                   d_neighbours,
                                                                   N_eq, N_poly,n_quad1d, 1,
                                                                   num_elem);
        }
        else{
            printf("LIMITER NOT FOUND.\n");
            quit();
        }



        // else if(limiter_type == BJ_QUAD){
        //     barth_jesperson_q<<<get_blocks(num_elem,num_threads), num_threads>>>(C, d_basis_side,
        //                                                                            s1, s2, s3,
        //                                                                            spos, schild1, schild2,
        //                                                                            le, re, 
        //                                                                            lsn, rsn,
        //                                                                            N_eq, N_poly, n_quad1d,
        //                                                                            num_elem);
        // }
        // else if(limiter_type == BJ_QUAD_N){
        //     barth_jesperson_q_neighbourhood<<<get_blocks(num_elem,num_threads), num_threads>>>(C, 
        //                                                                            d_basis_side,
        //                                                                            d_neighbours,
        //                                                                            N_eq, N_poly, n_quad1d,
        //                                                                            num_elem);
        // }
        // else if(limiter_type == BJ_R){
        //     barth_jesperson_m_rneighbourhood<<<get_blocks(num_elem,num_threads), num_threads>>>(C, 
        //                                                                            d_midpoint,
        //                                                                            d_neighbours,
        //                                                                            N_eq, N_poly,
        //                                                                            num_elem);
        // }
        // else if(limiter_type == BJ_QUAD_R){
        //     barth_jesperson_q_rneighbourhood<<<get_blocks(num_elem,num_threads), num_threads>>>(C, 
        //                                                                            d_basis_side,
        //                                                                            d_neighbours,
        //                                                                            N_eq, N_poly,n_quad1d,
        //                                                                            num_elem);
        // }
}

double *get_h(){ return d_h; }


void write_limit(int curr_num_elem, int num, int total_timesteps) {
    int *h_activated;
    double *h_value;
    double *h_V1x, *h_V1y, *h_V2x, *h_V2y, *h_V3x, *h_V3y;
    int i, n;
    int n_threads     = 512;
    int n_blocks_elem = (curr_num_elem  / n_threads) + ((curr_num_elem  % n_threads) ? 1 : 0);
    FILE *out_file;
    char out_filename[100];
    char out_filename2[100];

    // evaluate at the vertex points and copy over data
    h_activated = (int *) malloc(curr_num_elem * sizeof(int));
    h_value = (double *) malloc(curr_num_elem * sizeof(double));
    h_V1x = (double *) malloc(curr_num_elem * sizeof(double));
    h_V1y = (double *) malloc(curr_num_elem * sizeof(double));
    h_V2x = (double *) malloc(curr_num_elem * sizeof(double));
    h_V2y = (double *) malloc(curr_num_elem * sizeof(double));
    h_V3x = (double *) malloc(curr_num_elem * sizeof(double));
    h_V3y = (double *) malloc(curr_num_elem * sizeof(double));

    cudaMemcpy(h_V1x, vert1x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_V1y, vert1y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_V2x, vert2x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_V2y, vert2y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_V3x, vert3x, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_V3y, vert3y, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    // printf("ELEMENT 0'S VERTICES are (%f,%f) (%f,%f) (%f,%f)\n", V1x[0], V1y[0], V2x[0], V2y[0], V3x[0], V3y[0]);
    // evaluate and write to file
    for (n = 0; n < 1; n++) {
        cudaMemcpy(h_activated, d_activated, curr_num_elem * sizeof(int), cudaMemcpyDeviceToHost);

        if (num == total_timesteps) {
            sprintf(out_filename, "output/limit%d-final.pos", n);
        } 

        out_file  = fopen(out_filename , "w");
        fprintf(out_file, "View \"limit%i \" {\n", n);
        for (i = 0; i < curr_num_elem; i++) {
            fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%i,%i,%i};\n", 
                                   h_V1x[i], h_V1y[i], h_V2x[i], h_V2y[i], h_V3x[i], h_V3y[i],
                                   h_activated[i], h_activated[i], h_activated[i]);
        }
        fprintf(out_file,"};");
        fclose(out_file);
    }

    for (n = 0; n < 1; n++) {
        cudaMemcpy(h_value, d_value1, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

        if (num == total_timesteps) {
            sprintf(out_filename, "output/limitvalue1%d-final.pos", n);
        } 

        out_file  = fopen(out_filename , "w");
        fprintf(out_file, "View \"limit%i \" {\n", n);
        for (i = 0; i < curr_num_elem; i++) {
            fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.015lf,%.015lf,%.015lf};\n", 
                                   h_V1x[i], h_V1y[i], h_V2x[i], h_V2y[i], h_V3x[i], h_V3y[i],
                                   h_value[i], h_value[i], h_value[i]);
        }
        fprintf(out_file,"};");
        fclose(out_file);
    }

        for (n = 0; n < 1; n++) {
        cudaMemcpy(h_value, d_value2, curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

        if (num == total_timesteps) {
            sprintf(out_filename, "output/limitvalue2%d-final.pos", n);
        } 

        out_file  = fopen(out_filename , "w");
        fprintf(out_file, "View \"limit%i \" {\n", n);
        for (i = 0; i < curr_num_elem; i++) {
            fprintf(out_file, "ST (%.015lf,%.015lf,0,%.015lf,%.015lf,0,%.015lf,%.015lf,0) {%.015lf,%.015lf,%.015lf};\n", 
                                   h_V1x[i], h_V1y[i], h_V2x[i], h_V2y[i], h_V3x[i], h_V3y[i],
                                   h_value[i], h_value[i], h_value[i]);
        }
        fprintf(out_file,"};");
        fclose(out_file);
    }

    free(h_activated);

    free(h_V1x);
    free(h_V1y);
    free(h_V2x);
    free(h_V2y);
    free(h_V3x);
    free(h_V3y);
}

