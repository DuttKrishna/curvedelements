#include <stdio.h>
#include "tree.cuh"
#include "operations.cuh"
#include <map>
#include "./cub-1.5.1/cub/cub.cuh"
using namespace cub;

size_t temp_storage_bytes_sum;
size_t temp_storage_bytes_sort;
size_t temp_storage_bytes_scan2;
__device__ void *d_temp_storage;
__device__ void *d_temp_storage_sum;
__device__ void *d_temp_storage_scan2;
void     *d_temp_storage_argmin;
size_t   temp_storage_bytes_argmin = 0;

KeyValuePair<int, double> *d_argmin; 
KeyValuePair<int, double> *d_argmax; 

cub::DoubleBuffer<double> d_keys;
cub::DoubleBuffer<int> d_values;

__device__ int *d_inflag;
__device__ int *d_ecadd;
__device__ double *d_error_temp;
__device__ double *d_error_temp2;
__device__ double *d_cost;
__device__ double *d_total_error;
__device__ int *d_flag_temp;

double *error ;

int p;

void allocateStrategy(int max){

    // Create a set of DoubleBuffers to wrap pairs of device pointers
    temp_storage_bytes_sort = 0;
    cudaMalloc((void**)&d_total_error, sizeof(double) );
    cudaMalloc((void**)&d_keys.d_buffers[0], sizeof(double) * max);
    cudaMalloc((void**)&d_keys.d_buffers[1], sizeof(double) * max);
    cudaMalloc((void**)&d_values.d_buffers[0], sizeof(int) * max);
    cudaMalloc((void**)&d_values.d_buffers[1], sizeof(int) * max);
    CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes_sort, d_keys, d_values, max));
    cudaMalloc(&d_temp_storage, temp_storage_bytes_sort);

    CubDebugExit(DeviceReduce::Sum(d_temp_storage_sum, temp_storage_bytes_sum, d_keys.Current(), d_total_error, max));
    cudaMalloc((void **) &d_temp_storage_sum, temp_storage_bytes_sum);

    CubDebugExit(DeviceScan::InclusiveSum(d_temp_storage_scan2, temp_storage_bytes_scan2, d_keys.Current(),d_error_temp2, max));
    cudaMalloc( (void **) &d_temp_storage_scan2, temp_storage_bytes_scan2);

    cudaMalloc((void**)&d_argmin, sizeof(KeyValuePair<int, double> ));
    cudaMalloc((void**)&d_argmax, sizeof(KeyValuePair<int, double> ));
    cub::DeviceReduce::ArgMin(d_temp_storage_argmin, temp_storage_bytes_argmin, d_cost, d_argmin, max);
    cudaMalloc(&d_temp_storage_argmin, temp_storage_bytes_argmin);

    cudaMalloc((void**)&d_inflag, sizeof(int) * max);
    cudaMalloc((void**)&d_ecadd, sizeof(int) * max);
    cudaMalloc((void**)&d_error_temp, sizeof(double) * max);
    cudaMalloc((void**)&d_error_temp2, sizeof(double) * max);
    cudaMalloc((void**)&d_cost, sizeof(double) * max);

    cudaMalloc((void**)&d_flag_temp, sizeof(int) * max);
    error = (double *) malloc(max * sizeof(double));

}

void initStrategy(int in_order) {
  p = in_order;
}






__global__ void refine( int *eIDlist,
                        int *edr, int *epos,
                        double *error, double limit,
                        int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID, eposID;

    if(idx  < max) { 
        eID = eIDlist[idx];
        eposID = epos[eID] ;
        if ( (error[idx] >= limit-1e-13) && (error[idx] > 1e-13) )
            edr[ eposID ] = 1;
    }
}

__global__ void coarsen(int *eIDlist,
                        int *edr, int *epos,
                        double *error, double limit,
                        int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID, eposID;

    if(idx  < max) { 
        eID = eIDlist[idx];
        eposID = epos[eID] ;
        if(error[idx] < limit)
            edr[ eposID ] = -1;
    }
}


__global__ void is_fourth_child(int *curr_elem, int *indicator,
                         int *eparent,
                         int *epos,
                         int *echild1,int *echild2,int *echild3,int *echild4,
                         int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID, eIDparent;
    int inmesh =0, eIDchild4;
    if (idx < max) {
        eID = curr_elem[idx];
        eIDparent = eparent[eID];

        eIDchild4 = (eIDparent > -1) ? echild4[eIDparent] : -1;

        if(eIDchild4 == eID) {
            inmesh = (echild1[echild1[eIDparent]] == -1);
            inmesh = (echild1[echild2[eIDparent]] == -1) && inmesh;
            inmesh = (echild1[echild3[eIDparent]] == -1) && inmesh;
        }
        indicator[idx] =  inmesh ? 1 : 0;
    }
} 

__global__ void is_fourth_child(int *curr_elem, int *indicator, int *flag,
                         int *eparent,
                         int *epos,
                         int *echild1,int *echild2,int *echild3,int *echild4,
                         int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID, eIDparent;
    int eIDchild1, eIDchild2, eIDchild3, eIDchild4;
    int inmesh =0;
    if (idx < max) {
        eID = curr_elem[idx];
        eIDparent = eparent[eID];

        eIDchild4 = (eIDparent > -1) ? echild4[eIDparent] : -1;

        if(eIDchild4 == eID && flag[idx] == 0) { //if the current fourth child is not a shock
            eIDchild1 = echild1[eIDparent];
            eIDchild2 = echild2[eIDparent];
            eIDchild3 = echild3[eIDparent];

            inmesh = (echild1[eIDchild1] == -1 );
            inmesh = (echild1[eIDchild2] == -1) && inmesh;
            inmesh = (echild1[eIDchild3] == -1) && inmesh;
        }

        if(inmesh) {
            inmesh = (flag[epos[eIDchild1]] == 0);
            inmesh = (flag[epos[eIDchild2]] == 0) && inmesh;
            inmesh = (flag[epos[eIDchild3]] == 0) && inmesh;
            inmesh = (flag[epos[eIDchild4]] == 0) && inmesh;
        }

        indicator[idx] =  inmesh ? 1 : 0;
    }
} 

__global__ void add_error(double *error_estimate, double *out,
                          int *ecadd, int *epos,
                          int *eparent, 
                          int *echild1, int *echild2, int *echild3, int *echild4,
                          int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID, eIDparent;
    int ePOSchild1, ePOSchild2, ePOSchild3, ePOSchild4;
    double error1, error2, error3, error4;
    if (idx < max) {
        eID = ecadd[idx];
        eIDparent = eparent[eID]; 
        ePOSchild1 = epos[echild1[eIDparent]];
        ePOSchild2 = epos[echild2[eIDparent]];
        ePOSchild3 = epos[echild3[eIDparent]];
        ePOSchild4 = epos[echild4[eIDparent]];

        error1 = error_estimate[ePOSchild1]; 
        error2 = error_estimate[ePOSchild2]; 
        error3 = error_estimate[ePOSchild3]; 
        error4 = error_estimate[ePOSchild4]; 

        out[idx] = error1 + error2 + error3 + error4;
    }
} 

__global__ void add_error(double *error_estimate, double *out,
                          int *ecadd, 
                          int *flag, int *out_flag,
                          int *epos,int *eparent, 
                          int *echild1, int *echild2, int *echild3, int *echild4,
                          int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID, eIDparent;
    int ePOSchild1, ePOSchild2, ePOSchild3, ePOSchild4;
    double error1, error2, error3, error4;
    if (idx < max) {
        eID = ecadd[idx];
        eIDparent = eparent[eID]; 
        ePOSchild1 = epos[echild1[eIDparent]];
        ePOSchild2 = epos[echild2[eIDparent]];
        ePOSchild3 = epos[echild3[eIDparent]];
        ePOSchild4 = epos[echild4[eIDparent]];

        error1 = error_estimate[ePOSchild1]; 
        error2 = error_estimate[ePOSchild2]; 
        error3 = error_estimate[ePOSchild3]; 
        error4 = error_estimate[ePOSchild4]; 

        out[idx] = error1 + error2 + error3 + error4;
        out_flag[idx] = (flag[ePOSchild1] || flag[ePOSchild2] || flag[ePOSchild3] || flag[ePOSchild4]) ? 1 : 0;
    }
} 

__global__ void coarsen( int *middle,
                         int *edr,
                         int *epos,
                         int *eparent,
                         int *echild1,int *echild2,int *echild3,int *echild4,
                         int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int eID, eIDparent;
    int eIDchild1, eIDchild2, eIDchild3, eIDchild4;

    if(idx  < max) { 
        eID = middle[idx];
        eIDparent = eparent[eID];

        eIDchild1 = echild1[eIDparent];
        eIDchild2 = echild2[eIDparent];
        eIDchild3 = echild3[eIDparent];
        eIDchild4 = echild4[eIDparent];
        // printf("EID IS %i, parent is %i, %i %i %i %i\n",eID, eIDparent, eIDchild1,eIDchild2,eIDchild3,eIDchild4);

        edr[epos[eIDchild1]] = -1;
        edr[epos[eIDchild2]] = -1;
        edr[epos[eIDchild3]] = -1;
        edr[epos[eIDchild4]] = -1;

    }
}


void refine_and_coarsen_fixed_number(double *d_error_estimate, int *d_curr_elem, 
                                       int *edr,int *epos, 
                                       double top_frac, double bottom_frac,
                                       int verbose,
                                       int curr_num_elem) {
    int n_threads = 512;
    double lower_error, upper_error;
    int lower_nth, upper_nth;


    // keys are stored in error_estimate (doubles)
    // values are the eIDs in curr_elem (ints)
    CubDebugExit(cudaMemcpy(d_keys.d_buffers[d_keys.selector], d_error_estimate, sizeof(double) * curr_num_elem, cudaMemcpyDeviceToDevice));
    CubDebugExit(cudaMemcpy(d_values.d_buffers[d_values.selector], d_curr_elem, sizeof(int) * curr_num_elem, cudaMemcpyDeviceToDevice));
    cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes_sort, d_keys, d_values, curr_num_elem);


    lower_nth = ceil( bottom_frac * curr_num_elem );
    upper_nth = floor( (1-top_frac)*curr_num_elem );
    cudaMemcpy(&upper_error, d_keys.Current() + upper_nth,  sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(&lower_error, d_keys.Current() + lower_nth,  sizeof(double), cudaMemcpyDeviceToHost);

    cudaMemset(edr, 0, curr_num_elem*sizeof(int));
    refine<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_values.Current(),
                                                              edr, epos,
                                                              d_keys.Current(), upper_error,
                                                              curr_num_elem);
    coarsen<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_values.Current(),
                                                             edr, epos,
                                                             d_keys.Current(), lower_error,
                                                             curr_num_elem);

}

void refine_and_coarsen(double *d_error_estimate, int *d_curr_elem, 
                                       int *edr,int *epos, 
                                       int verbose,
                                       double lower_error, double upper_error,
                                       int curr_num_elem) {
    int n_threads = 512;
  
    cudaMemset(edr, 0, curr_num_elem*sizeof(int));
    refine<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem,
                                                              edr, epos,
                                                              d_error_estimate, upper_error,
                                                              curr_num_elem);
    coarsen<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem,
                                                             edr, epos,
                                                             d_error_estimate, lower_error,
                                                             curr_num_elem);

}
// void refine_and_coarsen_optimize(double *d_error_estimate, int *d_curr_elem, 
//                                  int *edr,int *epos,
//                                  int verbose,
//                                  int curr_num_elem) {
//     int n_threads = 512;
//     double *error = (double *) malloc(curr_num_elem * sizeof(double));
//     int order = 1;
//     double error_reduction = 0, tot_error = 0;
//     double cost, min_cost;
//     int min_arg;
//     // keys are stored in error_estimate (doubles)
//     // values are the eIDs in curr_elem (ints)

//     CubDebugExit(cudaMemcpy(d_keys.d_buffers[d_keys.selector], d_error_estimate, sizeof(double) * curr_num_elem, cudaMemcpyDeviceToDevice));
//     CubDebugExit(cudaMemcpy(d_values.d_buffers[d_values.selector], d_curr_elem, sizeof(int) * curr_num_elem, cudaMemcpyDeviceToDevice));
//     cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes_sort, d_keys, d_values, curr_num_elem);
//     cudaMemcpy(error, d_keys.Current(), curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

//     for(int i = 0; i < curr_num_elem; i++)
//         tot_error+= error[i];

//     min_cost = tot_error * pow( (double) curr_num_elem, (order+1)/2.);
//     min_arg = 0;
//     for(int i = curr_num_elem-1; i>=0; i--){
//         error_reduction += (1-pow(2., -(order + 1)))*error[i];
//         cost = (tot_error - error_reduction) * pow( 3.*(curr_num_elem - i) + curr_num_elem, (order+1)/2.);

//         if(cost <= min_cost) {
//             min_cost = cost;
//             min_arg = i;
//         }
//     }


//     cudaMemset(edr, 0, curr_num_elem*sizeof(int));
//     refine<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_values.Current(),
//                                                                 edr, epos,
//                                                                 d_keys.Current(), error[min_arg],
//                                                                 curr_num_elem);



//     free(error);
// }

// with coarsening

void refine_and_coarsen_optimize(double *d_error_estimate, int *d_curr_elem, 
                                 int *edr,int *epos,
                                 int verbose, int global_error_order,
                                 int curr_num_elem) {
    int n_threads = 512;
    double power = (double) global_error_order;
    double error_reduction = 0, error_diff = 0, error_increase = 0, tot_error = 0, min_error = 0, min_error_coarsen=0;
    double cost, min_cost;
    int min_arg, max_arg;
    int num_ecadd;
    // keys are stored in error_estimate (doubles)
    // values are the eIDs in curr_elem (ints)

    clock_t s,e;
    s = clock();
    CubDebugExit(cudaMemcpy(d_keys.d_buffers[d_keys.selector], d_error_estimate, sizeof(double) * curr_num_elem, cudaMemcpyDeviceToDevice));
    CubDebugExit(cudaMemcpy(d_values.d_buffers[d_values.selector], d_curr_elem, sizeof(int) * curr_num_elem, cudaMemcpyDeviceToDevice));
    CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes_sort, d_keys, d_values, curr_num_elem));
    e = clock();
    printf("\nsorting %lf", ( (double)(e - s) )/CLOCKS_PER_SEC);
    s = clock();
    cudaMemcpy(error, d_keys.Current(), curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);
    e = clock();
    printf("\ncopying %lf", ( (double)(e - s) )/CLOCKS_PER_SEC);

    for(int i = 0; i < curr_num_elem; i++)
        tot_error+= error[i];

    min_cost = tot_error * pow( (double) curr_num_elem, power/2.);
    min_arg = -1;
    if(verbose)
      printf("\n? the cost before adaptivity is %.4e = %.4e * (%i)^(%i/2)\n", min_cost, tot_error,curr_num_elem, (int) power);
    for(int i = curr_num_elem-1; i>=0; i--){
        error_reduction += (1-pow(2., -power))*error[i];
        cost = (tot_error - error_reduction) * pow( 3.*(curr_num_elem - i) + curr_num_elem, power/2.);

        if(cost <= min_cost) {
            min_cost = cost;
            min_arg = i;
            min_error = tot_error-error_reduction;
        }
    }


    cudaMemset(edr, 0, curr_num_elem*sizeof(int));

    if(min_arg > -1)
        refine<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_values.Current(),
                                                                    edr, epos,
                                                                    d_keys.Current(), error[min_arg],
                                                                    curr_num_elem);



    // coarsen groups
    //find fourth childrens
    s = clock();

    is_fourth_child<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem, d_inflag, 
                                                                           get_eparent(), 
                                                                           get_epos(),
                                                                           get_echild1(),get_echild2(),get_echild3(),get_echild4(),
                                                                           curr_num_elem);


    compaction(d_ecadd,d_curr_elem, d_inflag, curr_num_elem, &num_ecadd); // remove incompatible elements

    // add error of four children together and store in the middle child.
    if(num_ecadd > 0)
        add_error<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_error_estimate, d_error_temp,
                                                                   d_ecadd,
                                                                   get_epos(),
                                                                   get_eparent(), 
                                                                   get_echild1(),get_echild2(),get_echild3(),get_echild4(),
                                                                   num_ecadd);

    
    CubDebugExit(cudaMemcpy(d_keys.d_buffers[d_keys.selector], d_error_temp, sizeof(double) * num_ecadd, cudaMemcpyDeviceToDevice));
    CubDebugExit(cudaMemcpy(d_values.d_buffers[d_values.selector], d_ecadd, sizeof(int) * num_ecadd, cudaMemcpyDeviceToDevice));
    CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes_sort, d_keys, d_values, num_ecadd));
    //copy back onto CPU
    cudaMemcpy(error, d_keys.Current(), num_ecadd * sizeof(double), cudaMemcpyDeviceToHost);
    e = clock();
    printf("\nrest %lf", ( (double)(e - s) )/CLOCKS_PER_SEC);

    min_error_coarsen = min_error;

    error_diff = 0.;
    max_arg = 0;
    for(int i = 0; i < num_ecadd; i++){
        error_diff += (pow(2., power) - 1)*error[i];
        cost = (min_error + error_diff) * pow( 3.*(curr_num_elem - min_arg) - 3*(i+1)+ curr_num_elem  , power/2.);
        // printf("C: %lf, %lf %i %lf\n",  
        //                         cost,
        //                         min_error + error_diff,
        //                         3*(curr_num_elem - min_arg) - 3*(i+1)+ curr_num_elem , 
        //                         pow( 3.*(curr_num_elem - min_arg) - 3*(i+1)+ curr_num_elem  , (order+1.)/2.));
        
        if(cost <= min_cost) {
            min_cost = cost;
            max_arg = i;
            min_error_coarsen = min_error + error_diff;
        }

    }


    if(verbose)
      printf("? the cost after adaptivity is %.4e and error is %.4e\n", min_cost, min_error_coarsen);

    if(max_arg > 0)
        coarsen<<<get_blocks(max_arg, n_threads), n_threads>>>(d_values.Current(),
                                                               edr,
                                                               get_epos(),
                                                               get_eparent(), 
                                                               get_echild1(),get_echild2(),get_echild3(),get_echild4(),
                                                               max_arg);


}
__global__ void evaluate_min( double *out, double *in, double *tot, int curr_num_elem, int power) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if(idx  < curr_num_elem) { 
        out[idx] = (tot[0] - (1.-pow(2., -power))*in[idx]) * pow( 3.*(idx+1) + curr_num_elem, power/2.);
    }
}

__global__ void evaluate_min2( double *out, double *in, double tot, int min_arg, int curr_num_elem, int power, int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if(idx  < max) { 
        out[idx] = (tot +(pow(2., power)-1.)*in[idx]) * pow( 3.*(curr_num_elem - min_arg) - 3*(idx+1)+ curr_num_elem, power/2.);
    }
}

        // cost = (min_error + error_diff) * pow( 3.*(curr_num_elem - min_arg) - 3*(i+1)+ curr_num_elem  , power/2.);


void refine_and_coarsen_optimize_parallel(double *d_error_estimate, int *d_curr_elem, 
                                 int *edr,int *epos,
                                 int verbose, int global_error_order,
                                 int curr_num_elem) {
    int n_threads = 512;
    double power = (double) global_error_order;
    double error_reduction = 0, error_diff = 0, error_increase = 0, tot_error = 0, min_error = 0, min_error_coarsen=0,error_arg_min;
    double cost, min_cost;
    int min_arg, max_arg;
    int num_ecadd;
    // keys are stored in error_estimate (doubles)
    // values are the eIDs in curr_elem (ints)

    CubDebugExit(cudaMemcpy(d_keys.d_buffers[d_keys.selector], d_error_estimate, sizeof(double) * curr_num_elem, cudaMemcpyDeviceToDevice));
    CubDebugExit(cudaMemcpy(d_values.d_buffers[d_values.selector], d_curr_elem, sizeof(int) * curr_num_elem, cudaMemcpyDeviceToDevice));
    CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes_sort, d_keys, d_values, curr_num_elem));





    DeviceReduce::Sum(d_temp_storage_sum, temp_storage_bytes_sum, d_keys.Current(), d_total_error, curr_num_elem);
    cudaMemcpy(&tot_error, d_total_error, sizeof(double), cudaMemcpyDeviceToHost);

    reverse<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_error_temp, d_keys.Current(),  curr_num_elem);
    DeviceScan::InclusiveSum(d_temp_storage_sum, temp_storage_bytes_sum, d_error_temp, d_error_temp2, curr_num_elem);

    evaluate_min<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_cost, d_error_temp2, d_total_error, curr_num_elem, power);
    
    cub::DeviceReduce::ArgMin(d_temp_storage_argmin, temp_storage_bytes_argmin, d_cost, d_argmin, curr_num_elem);

    KeyValuePair<int, double> argmin;
    cudaMemcpy(&argmin, d_argmin, sizeof(KeyValuePair<int, double>), cudaMemcpyDeviceToHost);

    double error_at_arg_min, error_after_refine;
    cudaMemcpy(&min_cost, d_cost + argmin.key, sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(&error_at_arg_min, d_error_temp + argmin.key, sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(&error_after_refine, d_error_temp2 + argmin.key, sizeof(double), cudaMemcpyDeviceToHost);
    min_error = tot_error - (1-pow(2., -power))*error_after_refine;
    min_arg = curr_num_elem- argmin.key-1;


    cudaMemset(edr, 0, curr_num_elem*sizeof(int));

    min_arg = min_arg == 0 ? -1 : min_arg;
    if(min_arg > -1)
        refine<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_values.Current(),
                                                                    edr, epos,
                                                                    d_keys.Current(), error_at_arg_min,
                                                                    curr_num_elem);



    // coarsen groups
    //find fourth childrens

    is_fourth_child<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem, d_inflag, 
                                                                           get_eparent(), 
                                                                           get_epos(),
                                                                           get_echild1(),get_echild2(),get_echild3(),get_echild4(),
                                                                           curr_num_elem);


    compaction(d_ecadd,d_curr_elem, d_inflag, curr_num_elem, &num_ecadd); // remove incompatible elements

    // add error of four children together and store in the middle child.
    if(num_ecadd > 0)
        add_error<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_error_estimate, d_error_temp,
                                                                   d_ecadd,
                                                                   get_epos(),
                                                                   get_eparent(), 
                                                                   get_echild1(),get_echild2(),get_echild3(),get_echild4(),
                                                                   num_ecadd);

    
    CubDebugExit(cudaMemcpy(d_keys.d_buffers[d_keys.selector], d_error_temp, sizeof(double) * num_ecadd, cudaMemcpyDeviceToDevice));
    CubDebugExit(cudaMemcpy(d_values.d_buffers[d_values.selector], d_ecadd, sizeof(int) * num_ecadd, cudaMemcpyDeviceToDevice));
    CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes_sort, d_keys, d_values, num_ecadd));










    // --------
    max_arg = 0;
    if(num_ecadd > 0){
        DeviceScan::InclusiveSum(d_temp_storage_sum, temp_storage_bytes_sum, d_keys.Current(), d_error_temp2, num_ecadd);
        evaluate_min2<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_cost, d_error_temp2, min_error, min_arg,curr_num_elem, power, num_ecadd);

        cub::DeviceReduce::ArgMin(d_temp_storage_argmin, temp_storage_bytes_argmin, d_cost, d_argmax, num_ecadd);
        KeyValuePair<int, double> argmax;
        cudaMemcpy(&argmax, d_argmax, sizeof(KeyValuePair<int, double>), cudaMemcpyDeviceToHost);

        max_arg = argmax.key;
    }


    if(verbose)
      printf("? the cost after adaptivity is %.4e and error is %.4e\n", min_cost, min_error_coarsen);

    if(max_arg > 0)
        coarsen<<<get_blocks(max_arg, n_threads), n_threads>>>(d_values.Current(),
                                                               edr,
                                                               get_epos(),
                                                               get_eparent(), 
                                                               get_echild1(),get_echild2(),get_echild3(),get_echild4(),
                                                               max_arg);


}


void refine_and_coarsen_optimize_shock_A(double *d_error_estimate, int *d_curr_elem,  int *d_flag,
                                       int *edr,int *epos,
                                       int verbose, 
                                       int global_error_order_smooth,
                                       int global_error_order_disc,
                                       int global_error_order,
                                       int curr_num_elem) {
    int n_threads = 512;
    int *flag = (int *) malloc(curr_num_elem * sizeof(int));
    int *c_flag = (int *) malloc(curr_num_elem * sizeof(int));
    double *error = (double *) malloc(curr_num_elem * sizeof(double));
    double power = (double) global_error_order;
    double error_reduction = 0, error_diff = 0, error_increase = 0, tot_error = 0, min_error = 0, min_error_coarsen=0;
    double cost, min_cost;
    int min_arg, max_arg;
    int num_ecadd;

    int curr_num_selem;
    cudaMemcpy(flag, d_flag, curr_num_elem*sizeof(int), cudaMemcpyDeviceToHost);

    // flag shock elements
    // cudaMemcpy(edr, d_flag, curr_num_elem*sizeof(int), cudaMemcpyDeviceToDevice);
    set_value<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, 0, curr_num_elem) ;

    // remove them from the current pool
    // set_value<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_error_estimate, d_flag, 0., curr_num_elem) ;
    count(d_flag, 1, curr_num_elem, &curr_num_selem) ;
    if(verbose)
    printf("\n? the number of elements located on a shock is %i\n", curr_num_selem);


    // keys are stored in error_estimate (doubles)
    // values are the eIDs in curr_elem (ints)
    CubDebugExit(cudaMemcpy(d_keys.d_buffers[d_keys.selector], d_error_estimate, sizeof(double) * curr_num_elem, cudaMemcpyDeviceToDevice));
    CubDebugExit(cudaMemcpy(d_values.d_buffers[d_values.selector], d_curr_elem, sizeof(int) * curr_num_elem, cudaMemcpyDeviceToDevice));
    CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes_sort, d_keys, d_values, curr_num_elem));
    cudaMemcpy(error, d_keys.Current(), curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    for(int i = 0; i < curr_num_elem; i++)
        tot_error+= error[i];

    min_cost = tot_error * pow( (double) curr_num_elem, power/2.);
    min_arg = -1;
    if(verbose)
    printf("\n? the cost before adaptivity is %.4e and error is %.4e\n", min_cost, tot_error);
    for(int i = curr_num_elem-1; i>=0; i--){
        if(flag[i] == 1) // if shock
            error_reduction += (1-pow(2., -global_error_order_disc))*error[i];
        else
            error_reduction += (1-pow(2., -global_error_order_smooth))*error[i];

        cost = (tot_error - error_reduction) * pow( 3.*(curr_num_elem - i) + curr_num_elem, power/2.);

        if(cost <= min_cost) {
            min_cost = cost;
            min_arg = i;
            min_error = tot_error-error_reduction;
        }
    }

    if(min_arg > -1)
        refine<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_values.Current(),
                                                                    edr, epos,
                                                                    d_keys.Current(), error[min_arg],
                                                                    curr_num_elem);



    // coarsen groups
    //find fourth childrens
    is_fourth_child<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem, d_inflag, d_flag,
                                                                           get_eparent(), 
                                                                           get_epos(),
                                                                           get_echild1(),get_echild2(),get_echild3(),get_echild4(),
                                                                           curr_num_elem);


    compaction(d_ecadd,d_curr_elem, d_inflag, curr_num_elem, &num_ecadd); // remove incompatible elements

    // add error of four children together and store in the middle child, same for flags.
    if(num_ecadd > 0)
        add_error<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_error_estimate, d_error_temp,
                                                                   d_ecadd,
                                                                   d_flag, d_flag_temp,
                                                                   get_epos(),
                                                                   get_eparent(), 
                                                                   get_echild1(),get_echild2(),get_echild3(),get_echild4(),
                                                                   num_ecadd);

    cudaMemcpy(c_flag, d_flag_temp, num_ecadd*sizeof(int), cudaMemcpyDeviceToHost);
    
    CubDebugExit(cudaMemcpy(d_keys.d_buffers[d_keys.selector], d_error_temp, sizeof(double) * num_ecadd, cudaMemcpyDeviceToDevice));
    CubDebugExit(cudaMemcpy(d_values.d_buffers[d_values.selector], d_ecadd, sizeof(int) * num_ecadd, cudaMemcpyDeviceToDevice));
    CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes_sort, d_keys, d_values, num_ecadd));
    //copy back onto CPU
    cudaMemcpy(error, d_keys.Current(), num_ecadd * sizeof(double), cudaMemcpyDeviceToHost);

    min_error_coarsen = min_error;

    error_diff = 0.;
    max_arg = 0;
    for(int i = 0; i < num_ecadd; i++){
      if(c_flag[i] == 1)
        error_diff += (pow(2., global_error_order_disc) - 1)*error[i];
      else
        error_diff += (pow(2., global_error_order_smooth) - 1)*error[i];

      cost = (min_error + error_diff) * pow( 3.*(curr_num_elem - min_arg) - 3*(i+1)+ curr_num_elem  , power/2.);
      
      if(cost <= min_cost) {
          min_cost = cost;
          max_arg = i;
          min_error_coarsen = min_error + error_diff;
      }

    }

    if(verbose)
    printf("? the cost after adaptivity is %.4e and error is %.4e\n", min_cost, min_error_coarsen);

    if(max_arg > 0)
        coarsen<<<get_blocks(max_arg, n_threads), n_threads>>>(d_values.Current(),
                                                               edr,
                                                               get_epos(),
                                                               get_eparent(), 
                                                               get_echild1(),get_echild2(),get_echild3(),get_echild4(),
                                                               max_arg);


    free(flag);
    free(c_flag);
    free(error);
}



void refine_and_coarsen_optimize_shock_B(double *d_error_estimate, int *d_curr_elem,  int *d_flag,
                                       int *edr,int *epos,
                                       int verbose, 
                                       int global_error_order_smooth,
                                       int global_error_order_disc,
                                       int global_error_order,
                                       int curr_num_elem) {
    int n_threads = 512;
    int *flag = (int *) malloc(curr_num_elem * sizeof(int));
    double *error = (double *) malloc(curr_num_elem * sizeof(double));
    double power = (double) global_error_order_smooth;
    double error_reduction = 0, error_diff = 0, error_increase = 0, tot_error = 0, min_error = 0, min_error_coarsen=0;
    double cost, min_cost;
    int min_arg, max_arg;
    int num_ecadd;
    int c_element = 1;

    int curr_num_selem;
    cudaMemcpy(flag, d_flag, curr_num_elem*sizeof(int), cudaMemcpyDeviceToHost);

    // flag shock elements
    // cudaMemcpy(edr, d_flag, curr_num_elem*sizeof(int), cudaMemcpyDeviceToDevice);
    set_value<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(edr, 0, curr_num_elem) ;

    // remove them from the current pool
    // set_value<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_error_estimate, d_flag, 0., curr_num_elem) ;
    count(d_flag, 1, curr_num_elem, &curr_num_selem) ;
    if(verbose)
    printf("\n? the number of elements located on a shock is %i\n", curr_num_selem);


    // keys are stored in error_estimate (doubles)
    // values are the eIDs in curr_elem (ints)
    CubDebugExit(cudaMemcpy(d_keys.d_buffers[d_keys.selector], d_error_estimate, sizeof(double) * curr_num_elem, cudaMemcpyDeviceToDevice));
    CubDebugExit(cudaMemcpy(d_values.d_buffers[d_values.selector], d_curr_elem, sizeof(int) * curr_num_elem, cudaMemcpyDeviceToDevice));
    CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes_sort, d_keys, d_values, curr_num_elem));
    cudaMemcpy(error, d_keys.Current(), curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

    for(int i = 0; i < curr_num_elem; i++)
      if(!flag[i])
        tot_error+= error[i];

    min_cost = tot_error * pow( (double) curr_num_elem, power/2.);
    min_arg = -1;

    if(verbose)
    printf("\n? the cost before adaptivity is %.4e and error is %.4e\n", min_cost, tot_error);
    for(int i = curr_num_elem-1; i>=0; i--){
      if(!flag[i]){ // if not shock
          error_reduction += (1-pow(2., -global_error_order_smooth))*error[i];

          cost = (tot_error - error_reduction) * pow( 3.*c_element + curr_num_elem, power/2.);

          if(cost <= min_cost) {
              min_cost = cost;
              min_arg = i;
              min_error = tot_error-error_reduction;
          }
          c_element++;
      }
    }

    cudaMemcpy(edr, d_flag, curr_num_elem * sizeof(int), cudaMemcpyDeviceToDevice);

    if(min_arg > -1)
        refine<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_values.Current(),
                                                                    edr, epos,
                                                                    d_keys.Current(), error[min_arg],
                                                                    curr_num_elem);



    // coarsen groups
    //find fourth childrens
    is_fourth_child<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem, d_inflag, d_flag,
                                                                           get_eparent(), 
                                                                           get_epos(),
                                                                           get_echild1(),get_echild2(),get_echild3(),get_echild4(),
                                                                           curr_num_elem);


    compaction(d_ecadd,d_curr_elem, d_inflag, curr_num_elem, &num_ecadd); // remove incompatible elements

    // add error of four children together and store in the middle child.
    if(num_ecadd > 0)
        add_error<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_error_estimate, d_error_temp,
                                                                   d_ecadd,
                                                                   get_epos(),
                                                                   get_eparent(), 
                                                                   get_echild1(),get_echild2(),get_echild3(),get_echild4(),
                                                                   num_ecadd);

    
    CubDebugExit(cudaMemcpy(d_keys.d_buffers[d_keys.selector], d_error_temp, sizeof(double) * num_ecadd, cudaMemcpyDeviceToDevice));
    CubDebugExit(cudaMemcpy(d_values.d_buffers[d_values.selector], d_ecadd, sizeof(int) * num_ecadd, cudaMemcpyDeviceToDevice));
    CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes_sort, d_keys, d_values, num_ecadd));
    //copy back onto CPU
    cudaMemcpy(error, d_keys.Current(), num_ecadd * sizeof(double), cudaMemcpyDeviceToHost);

    min_error_coarsen = min_error;

    error_diff = 0.;
    max_arg = 0;
    for(int i = 0; i < num_ecadd; i++){
        error_diff += (pow(2., global_error_order_smooth) - 1)*error[i];
        cost = (min_error + error_diff) * pow( 3.*(curr_num_elem - c_element) - 3*(i+1)+ curr_num_elem  , power/2.);
        
        if(cost <= min_cost) {
            min_cost = cost;
            max_arg = i;
            min_error_coarsen = min_error + error_diff;
        }

    }

    if(verbose)
    printf("? the cost after adaptivity is %.4e and error is %.4e\n", min_cost, min_error_coarsen);

    if(max_arg > 0)
        coarsen<<<get_blocks(max_arg, n_threads), n_threads>>>(d_values.Current(),
                                                               edr,
                                                               get_epos(),
                                                               get_eparent(), 
                                                               get_echild1(),get_echild2(),get_echild3(),get_echild4(),
                                                               max_arg);


    free(flag);
    free(error);
}



// added logic to remove shock elements from analysis.


// void refine_and_coarsen_optimize_shock(double *d_error_estimate, int *d_curr_elem, int *d_flag,
//                                        int *edr,int *epos,
//                                        int verbose, int global_error_order,
//                                        int curr_num_elem) {
//     int n_threads = 512;
//     double *error = (double *) malloc(curr_num_elem * sizeof(double));
//     double power = (double) global_error_order;
//     double error_reduction = 0, error_diff = 0, error_increase = 0, tot_error = 0, min_error = 0, min_error_coarsen=0;
//     double cost, min_cost;
//     int min_arg, max_arg;
//     int num_ecadd;

//     int curr_num_selem;

//     // flag shock elements
//     cudaMemcpy(edr, d_flag, curr_num_elem*sizeof(int), cudaMemcpyDeviceToDevice);


//     // remove them from the current pool
//     set_value<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_error_estimate, d_flag, 0., curr_num_elem) ;
//     count(d_flag, 1, curr_num_elem, &curr_num_selem) ;
//     printf("\n? the number of elements located on a shock is %i\n", curr_num_selem);


//     // keys are stored in error_estimate (doubles)
//     // values are the eIDs in curr_elem (ints)
//     CubDebugExit(cudaMemcpy(d_keys.d_buffers[d_keys.selector], d_error_estimate, sizeof(double) * curr_num_elem, cudaMemcpyDeviceToDevice));
//     CubDebugExit(cudaMemcpy(d_values.d_buffers[d_values.selector], d_curr_elem, sizeof(int) * curr_num_elem, cudaMemcpyDeviceToDevice));
//     CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes_sort, d_keys, d_values, curr_num_elem));
//     cudaMemcpy(error, d_keys.Current(), curr_num_elem * sizeof(double), cudaMemcpyDeviceToHost);

//     for(int i = 0; i < curr_num_elem; i++)
//         tot_error+= error[i];

//     min_cost = tot_error * pow( (double) curr_num_elem, power/2.);
//     min_arg = -1;
//     printf("\n? the cost before adaptivity is %.4e and error is %.4e\n", min_cost, tot_error);
//     for(int i = curr_num_elem-1; i>=0; i--){
//         error_reduction += (1-pow(2., -power))*error[i];
//         cost = (tot_error - error_reduction) * pow( 3.*(curr_num_elem - i) + curr_num_elem, power/2.);

//         if(cost <= min_cost) {
//             min_cost = cost;
//             min_arg = i;
//             min_error = tot_error-error_reduction;
//         }
//     }


//     cudaMemset(edr, 0, curr_num_elem*sizeof(int));

//     if(min_arg > -1)
//         refine<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_values.Current(),
//                                                                     edr, epos,
//                                                                     d_keys.Current(), error[min_arg],
//                                                                     curr_num_elem);


//     // WHEN ESTABLISHING THE COARSENING GROUPS - DON'T EVEN ALLOW REFINED ELEMENTS IN THE BUNCHES. DO THIS HERE.
//     // coarsen groups
//     //find fourth childrens

//     is_fourth_child<<<get_blocks(curr_num_elem, n_threads), n_threads>>>(d_curr_elem, d_inflag, 
//                                                                            get_eparent(), 
//                                                                            get_epos(),
//                                                                            get_echild1(),get_echild2(),get_echild3(),get_echild4(),
//                                                                            curr_num_elem);


//     compaction(d_ecadd,d_curr_elem, d_inflag, curr_num_elem, &num_ecadd); // remove incompatible elements

//     // add error of four children together and store in the middle child.
//     if(num_ecadd > 0)
//         add_error<<<get_blocks(num_ecadd, n_threads), n_threads>>>(d_error_estimate, d_error_temp,
//                                                                    d_ecadd,
//                                                                    get_epos(),
//                                                                    get_eparent(), 
//                                                                    get_echild1(),get_echild2(),get_echild3(),get_echild4(),
//                                                                    num_ecadd);

    
//     CubDebugExit(cudaMemcpy(d_keys.d_buffers[d_keys.selector], d_error_temp, sizeof(double) * num_ecadd, cudaMemcpyDeviceToDevice));
//     CubDebugExit(cudaMemcpy(d_values.d_buffers[d_values.selector], d_ecadd, sizeof(int) * num_ecadd, cudaMemcpyDeviceToDevice));
//     CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes_sort, d_keys, d_values, num_ecadd));
//     //copy back onto CPU
//     cudaMemcpy(error, d_keys.Current(), num_ecadd * sizeof(double), cudaMemcpyDeviceToHost);

//     min_error_coarsen = min_error;

//     error_diff = 0.;
//     max_arg = 0;
//     for(int i = 0; i < num_ecadd; i++){
//         error_diff += (pow(2., power) - 1)*error[i];
//         cost = (min_error + error_diff) * pow( 3.*(curr_num_elem - min_arg) - 3*(i+1)+ curr_num_elem  , power/2.);
        
//         if(cost <= min_cost) {
//             min_cost = cost;
//             max_arg = i;
//             min_error_coarsen = min_error + error_diff;
//         }

//     }

//     printf("? the cost after adaptivity is %.4e and error is %.4e\n", min_cost, min_error_coarsen);

//     if(max_arg > 0)
//         coarsen<<<get_blocks(max_arg, n_threads), n_threads>>>(d_values.Current(),
//                                                                edr,
//                                                                get_epos(),
//                                                                get_eparent(), 
//                                                                get_echild1(),get_echild2(),get_echild3(),get_echild4(),
//                                                                max_arg);


//     free(error);
// }












