/* conserv_kernels.cu
 *
 * Contains the GPU variables and kernels to solve hyperbolic conservation laws in two-dimensions.
 * Stores all mesh variables and the following kernels
 * 
 * 1. precompuations
 * 2. surface integral 
 * 3. volume integral 
 * 4. limiter
 * 5. evaluate u 
 *
 */

#include "conserv_headers.cuh"
 
#define N_MAX 4
#define NP_MAX 21

/***********************
 *
 * DEVICE VARIABLES
 *
 ***********************/
/* These are always prefixed with d_ for "device" */
__device__ double *d_curr_c;                 // coefficients for [rho, rho * u, rho * v, E]
double **h_c;                  // coefficients for [rho, rho * u, rho * v, E]
double **h_c_prev;                  // coefficients for [rho, rho * u, rho * v, E]
__device__ double **d_c;                 // coefficients for [rho, rho * u, rho * v, E]
__device__ double **d_c_prev;            // coefficients for [rho, rho * u, rho * v, E]

__device__ double *d_max_error; // the right hand side containing the right riemann contributions
__device__ double *d_error_estimate; // the right hand side containing the right riemann contributions
__device__ double *d_error_left; // the right hand side containing the right riemann contributions
__device__ double *d_error_right; // the right hand side containing the right riemann contributions

// TODO: switch to low storage runge-kutta
// runge kutta variables
__device__ double **d_temp;
__device__ double **d_temp2;
__device__ double **d_k1;
__device__ double **d_k2;
__device__ double **d_k3;
__device__ double **d_k4;
__device__ double **d_k5;
__device__ double **d_k6;
__device__ double **d_k7;
__device__ double **d_k8;
double **h_temp;                  // coefficients for [rho, rho * u, rho * v, E]
double **h_temp2;                  // coefficients for [rho, rho * u, rho * v, E]
double **h_k1;                  // coefficients for [rho, rho * u, rho * v, E]
double **h_k2;                  // coefficients for [rho, rho * u, rho * v, E]
double **h_k3;                  // coefficients for [rho, rho * u, rho * v, E]
double **h_k4;                  // coefficients for [rho, rho * u, rho * v, E]
double **h_k5;                  // coefficients for [rho, rho * u, rho * v, E]
double **h_k6;                  // coefficients for [rho, rho * u, rho * v, E]
double **h_k7;                  // coefficients for [rho, rho * u, rho * v, E]
double **h_k8;                  // coefficients for [rho, rho * u, rho * v, E]

__device__ double *d_volume; // the right hand side containing the right riemann contributions


// precomputed basis functions 
// TODO: maybe making these 2^n makes sure the offsets are cached more efficiently? who knows...
// precomputed basis functions ordered like so
//
// [phi_1(r1, s1), phi_1(r2, s2), ... , phi_1(r_nq, s_nq)   ]
// [phi_2(r1, s1), phi_2(r2, s2), ... , phi_2(r_nq, s_nq)   ]
// [   .               .           .            .           ]
// [   .               .           .            .           ]
// [   .               .           .            .           ]
// [phi_np(r1, s1), phi_np(r2, s2), ... , phi_np(r_nq, s_nq)]
//
__device__ __constant__ double basis[525]; // really only need 525 originally 2048
// note: these are the precomupted gradients of the basis functions
//       multiplied by the weights at that integration point
// technically doesn't need to be in constant memory
__device__ __constant__ double basis_grad_x[525]; // really only need 525
__device__ __constant__ double basis_grad_y[525]; // really only need 525

__device__ __constant__ int N;
__device__ __constant__ int n_p;
__device__ __constant__ int num_elem; //curent number of elements
__device__ __constant__ int num_sides; //current number of sides
__device__ __constant__ int mesh_num_elem; // number of elements present in mesh_tree
__device__ __constant__ int mesh_num_sides; //number of sides present in side_tree

__device__ __constant__ int n_quad;
__device__ __constant__ int n_quad1d;

// precomputed basis functions evaluated along the sides. ordered
// similarly to basis and basis_grad_{x,y} but with one "matrix" for each side
// starting with side 0. to get to each side, offset with:
//      side_number * n_p * num_quad1d.
__device__ __constant__ double basis_side[1134];
__device__ __constant__ double basis_vertex[64];

// weights for 2d and 1d quadrature rules
__device__ __constant__ double w[64];
__device__ __constant__ double w_oned[16];

// integration ponits for 2d and 1d quadrature rules
__device__ __constant__ double r1[32];
__device__ __constant__ double r2[32];
__device__ __constant__ double r_oned[32];

__device__ __constant__ double basis_refined[2100];



__device__ double *d_basis; // really only need 525 originally 2048
__device__ double *d_basis_grad_x; // really only need 525
__device__ double *d_basis_grad_y; // really only need 525
__device__ double *d_w;
__device__ double *d_basis_refined;
__device__ double *d_basis_midpoint; // really only need 525 originally 2048
__device__ double *d_basis_vertex; // really only need 525 originally 2048
__device__ double *d_basis_side; // really only need 525 originally 2048

__device__ int *d_sensor_eID; // index of right element for side idx
__device__ double *d_sensorbuffer; 
__device__ double *d_sensor_basis; // index of right element for side idx







__device__ void get_coordinates_2d(double *x, double *V, int j);


void set_N(int value) {
    cudaMemcpyToSymbol(N, (void *) &value, sizeof(int));
}
void set_n_p(int value) {
    cudaMemcpyToSymbol(n_p, (void *) &value, sizeof(int));
}
void set_num_elem(int value) {
    cudaMemcpyToSymbol(num_elem, (void *) &value, sizeof(int));
}
void set_num_sides(int value) {
    cudaMemcpyToSymbol(num_sides, (void *) &value, sizeof(int));
}
void set_n_quad(int value) {
    cudaMemcpyToSymbol(n_quad, (void *) &value, sizeof(int));
}
void set_n_quad1d(int value) {
    cudaMemcpyToSymbol(n_quad1d, (void *) &value, sizeof(int));
}
void set_basis(double **value, int size) {
    cudaMemcpyToSymbol(basis, (void *) *value, size * sizeof(double));
}
void set_basis_grad_x(void *value, int size) {
    cudaMemcpyToSymbol(basis_grad_x, value, size * sizeof(double));
}
void set_basis_grad_y(void *value, int size) {
    cudaMemcpyToSymbol(basis_grad_y, value, size * sizeof(double));
}
void set_basis_side(void *value, int size) {
    cudaMemcpyToSymbol(basis_side, value, size * sizeof(double));
}
void set_basis_vertex(void *value, int size) {
    cudaMemcpyToSymbol(basis_vertex, value, size * sizeof(double));
}
void set_w(void *value, int size) {
    cudaMemcpyToSymbol(w, value, size * sizeof(double));
}
void set_w_oned(void *value, int size) {
    cudaMemcpyToSymbol(w_oned, value, size * sizeof(double));
}
void set_r1(void *value, int size) {
    cudaMemcpyToSymbol(r1, value, size * sizeof(double));
}
void set_r2(void *value, int size) {
    cudaMemcpyToSymbol(r2, value, size * sizeof(double));
}
void set_r_oned(void *value, int size) {
    cudaMemcpyToSymbol(r_oned, value, size * sizeof(double));
}

 __device__ void eval_left_right(double **C, double *C_left, double *C_right, 
                             double *U_left, double *U_right,
                             double nx, double ny,
                             double *V, int j, 
                             int left_side, int right_side,
                             int left_idx, int right_idx,
                             double t) ;

// tells which side (1, 2, or 3) to evaluate this boundary integral over
__device__ int *d_left_side_number;
__device__ int *d_right_side_number;

__device__ double *d_curr_J;         // jacobian determinant 
__device__ double *d_reduction; // for the min / maxes in the reductions 
__device__ double *d_lambda;    // stores computed lambda values for each element
__device__ double *d_s_length;  // length of sides

// the num_elem values of the x and y coordinates for the two vertices defining a side
// TODO: can i delete these after the lengths are precomputed?
//       maybe these should be in texture memory?
__device__ double *d_s_V1x;
__device__ double *d_s_V1y;
__device__ double *d_s_V2x;
__device__ double *d_s_V2y;

// the num_elem values of the x and y partials
__device__ double *d_curr_xr;
__device__ double *d_curr_yr;
__device__ double *d_curr_xs;
__device__ double *d_curr_ys;

// the K indices of the sides for each element ranged 0->H-1
__device__ int *d_elem_s1;
__device__ int *d_elem_s2;
__device__ int *d_elem_s3;
__device__ int *d_elem_s4;
__device__ int *d_elem_s5;
__device__ int *d_elem_s6;

__device__ int *d_curr_s1;
__device__ int *d_curr_s2;
__device__ int *d_curr_s3;

__device__ int *d_curr_left1;
__device__ int *d_curr_left2;
__device__ int *d_curr_left3;

__device__ int *d_curr_elevel;
__device__ int *d_curr_slevel;

__device__ int *d_curr_scolor;


// vertex x and y coordinates on the mesh which define an element
// TODO: can i delete these after the jacobians are precomputed?
//       maybe these should be in texture memory?
__device__ double *d_curr_V1x;
__device__ double *d_curr_V1y;
__device__ double *d_curr_V2x;
__device__ double *d_curr_V2y;
__device__ double *d_curr_V3x;
__device__ double *d_curr_V3y;

// vertex coordinates for the projection mesh
__device__ double *d_pV1x;
__device__ double *d_pV1y;
__device__ double *d_pV2x;
__device__ double *d_pV2y;
__device__ double *d_pV3x;
__device__ double *d_pV3y;

// to store if a point x y is inside element idx
__device__ int *d_point_is_in_elem;
__device__ int *d_project_idx;



// for computing the error
__device__ double *d_error;

// normal vectors for the sides
__device__ double *d_Nx;
__device__ double *d_Ny;

// index lists for sides
__device__ int *d_left_elem;  // index of left  element for side idx
__device__ int *d_right_elem; // index of right element for side idx

__device__ double *d_circles;

/***********************
 *
 * DEVICE REFINEMENT VARIABLES
 *
 ***********************/





__device__ int *d_curr_elem; // elements in use
__device__ int *d_curr_side; // side in use
__device__ int *d_curr_originalside; // elements in use



int cumulative_sum[4];






__global__ void evaluate_sensors(int *sensor_eID, double *sensorbuffer, double *sensor_basis, double **c, int N, int n_p, int num_elem,int num_sensors) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int eID ;
    double sensor_val[5];
        if (idx < num_sensors) {
            eID = sensor_eID[idx];


            for(int n = 0 ; n < N; n++) {
                sensor_val[n] = 0;
            }

            for(int n = 0 ; n < N; n++) {
                for(int i = 0; i < n_p; i++){
                    sensor_val[n] += c[n*n_p + i][eID] * sensor_basis[num_sensors*i + idx];
                }
            }

            for(int n = 0 ; n < N; n++) {
                sensorbuffer[num_sensors*n + idx] = sensor_val[n];
            }

        }
}

__global__ void evaluate_sensors_trumpet(int *sensor_eID, double *sensorbuffer, double *sensor_basis, double **C, 
                                         double *V1x, double *V1y,
                                         double *V2x, double *V2y,
                                         double *V3x, double *V3y,
                                         int N, int n_p, int num_elem,int num_sensors) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int eID ;
    double sensor_val[5];
    double Ur[N_MAX], c[N_MAX*NP_MAX];
    double X[2], V[6];

    if (idx < num_sensors) {
        eID = sensor_eID[idx];
        V[0] = V1x[eID];
        V[1] = V1y[eID];
        V[2] = V2x[eID];
        V[3] = V2y[eID];
        V[4] = V3x[eID];
        V[5] = V3y[eID];


        for (int i = 0; i < n_p; i++) {
            for (int n = 0; n < N; n++) {
                c[n*n_p + i] = 0.;
            }
        }

        for (int j = 0; j < n_quad; j++) {

        // evaluate U here
            Ur[0] = 0.;
            Ur[1] = 0.;
            Ur[2] = 0.;
            Ur[3] = 0.;


            for (int n = 0; n < N; n++) {
                for(int i = 0; i < n_p; i++){
                    Ur[n] += C[n*n_p + i][eID] * basis[n_quad * i + j] ;
                }
            }

            for (int i = 0; i < n_p; i++) {
                // get the 2d point on the mesh
                get_coordinates_2d(X, V, j);

                // project
                c[0*n_p + i]  += w[j] * (Ur[0]/X[1]) * basis[i * n_quad + j];
                c[1*n_p + i]  += w[j] * (Ur[1]/X[1]) * basis[i * n_quad + j];
                c[2*n_p + i]  += w[j] * (Ur[2]/X[1]) * basis[i * n_quad + j];
                c[3*n_p + i]  += w[j] * (Ur[3]/X[1]) * basis[i * n_quad + j];

            }
        }

        for(int n = 0 ; n < N; n++) {
            sensor_val[n] = 0;
        }

        for(int n = 0 ; n < N; n++) {
            for(int i = 0; i < n_p; i++){
                sensor_val[n] += c[n*n_p + i] * sensor_basis[num_sensors*i + idx];
                // printf("right here %lf\n", c[n*n_p + i]);
            }
        }

        for(int n = 0 ; n < N; n++) {
            sensorbuffer[num_sensors*n + idx] = sensor_val[n];
        }

    }
}


void write_sensor(int *sensor_eID, double *cpu_sensorbuffer, double **c, double *d_sensor_basis,
                  double *V1x, double *V1y,
                  double *V2x, double *V2y,
                  double *V3x, double *V3y,
                  int num_sensors, int N, int n_p, int num_elem, double t) {
    FILE *out_file;
    char sensor_filename[100];
    // printf("num sensors %i %i %i\n", num_sensors, n_p, N);

    int num_blocks = (num_sensors  / 512) 
                      + ((num_sensors  % 512) ? 1 : 0);
    // evaluate and write to file
    // copy_sensor_data<<<num_blocks, 512>>>(d_sensorbuffer, c, sensor_eID, num_sensors, N, n_p, num_elem);
    // evaluate_sensors<<<num_blocks, 512>>>(d_sensor_eID, d_sensorbuffer, d_sensor_basis, c, N, n_p, num_elem,num_sensors);
    evaluate_sensors_trumpet<<<num_blocks, 512>>>(d_sensor_eID, d_sensorbuffer, d_sensor_basis, c,
                                                  V1x, V1y ,
                                                  V2x, V2y ,
                                                  V3x, V3y ,
                                                  N, n_p, num_elem,num_sensors);
    cudaMemcpy(cpu_sensorbuffer, d_sensorbuffer, num_sensors * N * sizeof(double), cudaMemcpyDeviceToHost);

    for(int s = 0; s < num_sensors; s++) {
        sprintf(sensor_filename, "output/sensors/sensor%i.txt", s);
        out_file  = fopen(sensor_filename, "a");
        fprintf(out_file, "%.015lf ", t);
        for (int n = 0; n < N; n++) {
            fprintf(out_file, "%.015lf ", cpu_sensorbuffer[n*num_sensors + s]);
        }

        fprintf(out_file, "\n");
        fclose(out_file);
    }

}



















void set_basis_refined(void *value, int size) {
    cudaMemcpyToSymbol(basis_refined, value, size * sizeof(double));
}




__global__ void init_conditions(double **c, double *J,
                                double *V1x, double *V1y,
                                double *V2x, double *V2y,
                                double *V3x, double *V3y,
                                int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int i, n;
    double U[4];
    double V[6];

    if (idx < num_elem) {
        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        for (i = 0; i < n_p; i++) {
             // evaluate U times the i'th basis function
             evalU0(U, V, i);

             // store the coefficients
             for (n = 0; n < N; n++) {  
                 c[n*n_p + i][idx] = U[n];
             }
        } 
        // if(idx == 0)
        //     printf("element %lf %lf %lf (%lf,%lf), (%lf,%lf), (%lf,%lf) \n", c[0][idx], c[1][idx], c[2][idx], V[0], V[1], V[2], V[3], V[4], V[5]);
    }
}

/* gets the grid coordinates at the j'th integration ponit for 2d */
__device__ void get_coordinates_2d(double *x, double *V, int j) {
    // x = x2 * r + x3 * s + x1 * (1 - r - s)
    x[0] = V[2] * r1[j] + V[4] * r2[j] + V[0] * (1 - r1[j] - r2[j]);
    x[1] = V[3] * r1[j] + V[5] * r2[j] + V[1] * (1 - r1[j] - r2[j]);
}

 /* gets the grid cooridinates at the j'th integration point for 1d */
__device__ void get_coordinates_1d(double *x, double *V, int j, int left_side) {

    double r1_eval, r2_eval;

    // we need the mapping back to the grid space

    switch (left_side) {
        case 0: 
            r1_eval = 0.5 + 0.5 * r_oned[j];
            r2_eval = 0.;
            break;
        case 1: 
            r1_eval = (1. - r_oned[j]) / 2.;
            r2_eval = (1. + r_oned[j]) / 2.;
            break;
        case 2: 
            r1_eval = 0.;
            r2_eval = 0.5 + 0.5 * r_oned[n_quad1d - 1 - j];
            break;
        case 3: 
            r1_eval = 0.5 * (1 + r_oned[j])/2.;
            r2_eval = 0. ;
            break;
        case 4: 
            r1_eval = (1 + r_oned[j])/2. + 0.5 * (1 - r_oned[j])/2.;
            r2_eval = 0. ;
            break;
        case 5: 
            r1_eval = 0.5 * (1 + r_oned[j])/2. + (1 - r_oned[j])/2.;
            r2_eval = 0.5  * (1 + r_oned[j])/2.;
            break;
        case 6: 
            r1_eval = 0.5 * (1 - r_oned[j])/2.;
            r2_eval = (1 + r_oned[j])/2. + 0.5 * (1- r_oned[j])/2.;
            break;
        case 7: 
            r1_eval = 0.;
            r2_eval = 0.5 * (1 + r_oned[j])/2. + (1 - r_oned[j])/2.;
            break;
        case 8: 
            r1_eval = 0.;
            r2_eval =  0.5 * (1 - r_oned[j])/2.;
            break;
        case 9: 
            r1_eval = 0.25 * (1 + r_oned[j])/2.;
            r2_eval = 0.;
            break;
        case 10: 
            r1_eval = 0.5  * (1 + r_oned[j])/2. + 0.25 * (1 - r_oned[j])/2.;
            r2_eval = 0.;
            break; 
        case 11: 
            r1_eval = 0.75  * (1 + r_oned[j])/2. + 0.5 * (1 - r_oned[j])/2.;
            r2_eval = 0.;
            break; 
        case 12: 
            r1_eval = (1 + r_oned[j])/2. + 0.75 * (1 - r_oned[j])/2.;
            r2_eval = 0.;
            break; 
        case 13: 
            r1_eval = 0.75 * (1 + r_oned[j])/2. + (1 - r_oned[j])/2.;
            r2_eval = 0.25 * (1 + r_oned[j])/2.;
            break; 
        case 14: 
            r1_eval = 0.5 *  (1 + r_oned[j])/2. + 0.75 * (1 - r_oned[j])/2.;
            r2_eval = 0.5 * (1 + r_oned[j])/2. + 0.25 * ( 1 - r_oned[j] )/2.;
            break; 
        case 15: 
            r1_eval = 0.25 * (1 + r_oned[j])/2. + 0.5 * (1 - r_oned[j])/2.;
            r2_eval = 0.75 * (1 + r_oned[j])/2. + 0.5 * (1 - r_oned[j])/2.;
            break; 
        case 16: 
            r1_eval = 0.25 * (1 - r_oned[j])/2. ;
            r2_eval = (1 + r_oned[j])/2. + 0.75 * (1 - r_oned[j])/2. ;
            break; 
        case 17: 
            r1_eval = 0. ;
            r2_eval = 0.75 * (1 + r_oned[j])/2. + (1 - r_oned[j])/2.;
            break; 
        case 18: 
            r1_eval = 0. ;
            r2_eval = 0.5 * (1 + r_oned[j])/2. + 0.75 * (1 - r_oned[j])/2. ;
            break; 
        case 19: 
            r1_eval = 0. ;
            r2_eval = 0.25 * (1 + r_oned[j])/2. + 0.5 * (1 - r_oned[j])/2. ;
            break;     
        case 20: 
            r1_eval = 0. ;
            r2_eval = 0.25 * (1 - r_oned[j] )/2.;
            break; 
        }

    // x = x2 * r + x3 * s + x1 * (1 - r - s)
    x[0] = V[2] * r1_eval + V[4] * r2_eval + V[0] * (1 - r1_eval - r2_eval);
    x[1] = V[3] * r1_eval + V[5] * r2_eval + V[1] * (1 - r1_eval - r2_eval);
}
 
__device__ void get_coordinates_m(double *x, double *V, int left_side) {

    double r1_eval, r2_eval;

    // we need the mapping back to the grid space

    switch (left_side) {
        case 0: 
            r1_eval = 0.5;
            r2_eval = 0.;
            break;
        case 1: 
            r1_eval = 0.5;
            r2_eval = 0.5;
            break;
        case 2: 
            r1_eval = 0.;
            r2_eval = 0.5;
            break;
        }

    // x = x2 * r + x3 * s + x1 * (1 - r - s)
    x[0] = V[2] * r1_eval + V[4] * r2_eval + V[0] * (1 - r1_eval - r2_eval);
    x[1] = V[3] * r1_eval + V[5] * r2_eval + V[1] * (1 - r1_eval - r2_eval);
}
/***********************
 *
 * PRECOMPUTING
 *
 ***********************/

/* side length computer
 *
 * precomputes the length of each side.
 * THREADS: num_sides
 */ 
__global__ void preval_side_length(double *s_length, 
                              double *s_V1x, double *s_V1y, 
                              double *s_V2x, double *s_V2y,
                              int num_sides) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_sides) {
        // compute and store the length of the side
        s_length[idx] = sqrt(pow(s_V1x[idx] - s_V2x[idx],2) + pow(s_V1y[idx] - s_V2y[idx],2));
    }
}


/* jacobian computing
 *
 * precomputes the jacobian determinant for each element.
 * THREADS: num_elem
 */
__global__ void preval_jacobian(double *J, 
                           double *V1x, double *V1y, 
                           double *V2x, double *V2y, 
                           double *V3x, double *V3y,
                           int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_elem) {
        double x1, y1, x2, y2, x3, y3;

        // read vertex points
        x1 = V1x[idx];
        y1 = V1y[idx];
        x2 = V2x[idx];
        y2 = V2y[idx];
        x3 = V3x[idx];
        y3 = V3y[idx];

        // calculate jacobian determinant
        // x = x2 * r + x3 * s + x1 * (1 - r - s)
        J[idx] = (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);
    }
}

/* evaluate normal vectors
 *
 * computes the normal vectors for each element along each side.
 * THREADS: num_sides
 *
 */
__global__ void preval_normals(double *Nx, double *Ny, 
                          double *s_V1x, double *s_V1y, 
                          double *s_V2x, double *s_V2y,
                          double *V1x, double *V1y, 
                          double *V2x, double *V2y, 
                          double *V3x, double *V3y,
                          int *left_side_number,
                          int num_sides) {

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_sides) {
        double x, y, length;
        double sv1x, sv1y, sv2x, sv2y;
    
        sv1x = s_V1x[idx];
        sv1y = s_V1y[idx];
        sv2x = s_V2x[idx];
        sv2y = s_V2y[idx];
    
        // lengths of the vector components
        x = sv2x - sv1x;
        y = sv2y - sv1y;
    
        // normalize
        length = sqrt(pow(x, 2) + pow(y, 2));

        // store the result
        Nx[idx] = -y / length;
        Ny[idx] =  x / length;
    }
}

__global__ void preval_normals_direction(double *Nx, double *Ny, 
                          double *V1x, double *V1y, 
                          double *V2x, double *V2y, 
                          double *V3x, double *V3y,
                          int *left_elem, int *left_side_number,
                          int num_sides) {

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_sides) {
        double new_x, new_y, dot;
        double initial_x, initial_y, target_x, target_y;
        double x, y;
        int left_idx, side;

        // get left side's vertices
        left_idx = left_elem[idx];
        side     = left_side_number[idx];

        // get the normal vector
        x = Nx[idx];
        y = Ny[idx];
    
        // make it point the correct direction by learning the third vertex point
        switch (side) {
            case 0: 
                target_x = V3x[left_idx];
                target_y = V3y[left_idx];
                initial_x = (V1x[left_idx] + V2x[left_idx]) / 2.;
                initial_y = (V1y[left_idx] + V2y[left_idx]) / 2.;
                break;
            case 1:
                target_x = V1x[left_idx];
                target_y = V1y[left_idx];
                initial_x = (V2x[left_idx] + V3x[left_idx]) / 2.;
                initial_y = (V2y[left_idx] + V3y[left_idx]) / 2.;
                break;
            case 2:
                target_x = V2x[left_idx];
                target_y = V2y[left_idx];
                initial_x = (V1x[left_idx] + V3x[left_idx]) / 2.;
                initial_y = (V1y[left_idx] + V3y[left_idx]) / 2.;
                break;
        }

        // create the vector pointing towards the third vertex point
        new_x = target_x - initial_x;
        new_y = target_y - initial_y;

        // find the dot product between the normal and new vectors
        dot = x * new_x + y * new_y;
        
        if (dot > 0) {
            Nx[idx] *= -1;
            Ny[idx] *= -1;
        }
    }
}

__global__ void preval_partials(double *V1x, double *V1y,
                                double *V2x, double *V2y,
                                double *V3x, double *V3y,
                                double *xr,  double *yr,
                                double *xs,  double *ys,
                                int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < num_elem) {
        // evaulate the jacobians of the mappings for the chain rule
        // x = x2 * r + x3 * s + x1 * (1 - r - s)
        xr[idx] = V2x[idx] - V1x[idx];
        yr[idx] = V2y[idx] - V1y[idx];
        xs[idx] = V3x[idx] - V1x[idx];
        ys[idx] = V3y[idx] - V1y[idx];
    }
}





/***********************
 *
 * BOUNDARY CONDITIONS
 *
 ***********************/
/* Put the boundary conditions for the problem in here.
*/
__device__ void inflow_boundary(double *U_left, double *U_right,
                                double *V, double nx, double ny,
                                int j, int left_side, double t) {

    double X[2];

    // get x, y coordinates
    get_coordinates_1d(X, V, j, left_side);

    U_inflow(U_right, X[0], X[1], t);
}

__device__ void outflow_boundary(double *U_left, double *U_right,
                                 double *V, double nx, double ny,
                                 int j, int left_side, double t) {
    double X[2];

    // get x, y coordinates
    get_coordinates_1d(X, V, j, left_side);
    
    U_outflow(U_right, X[0], X[1], t);
}

__device__ void reflecting_boundary(double *U_left, double *U_right,
                                    double *V, double nx, double ny, 
                                    int j, int left_side, double t) {
    double X[2];

    // get x, y coordinates
    get_coordinates_1d(X, V, j, left_side);
    
    U_reflection(U_left, U_right, X[0], X[1], t, nx, ny);
}

__device__ void inflow_boundary_m(double *U_left, double *U_right,
                                double *V, double nx, double ny,
                                int left_side, double t) {
    double X[2];
    // get x, y coordinates
    get_coordinates_m(X, V, left_side);
    U_inflow(U_right, X[0], X[1], t);
}

__device__ void outflow_boundary_m(double *U_left, double *U_right,
                                 double *V, double nx, double ny,
                                 int left_side, double t) {
    double X[2];
    // get x, y coordinates
    get_coordinates_m(X, V, left_side);
    U_outflow(U_right, X[0], X[1], t);
}

__device__ void reflecting_boundary_m(double *U_left, double *U_right,
                                    double *V, double nx, double ny, 
                                    int left_side, double t) {
    double X[2];
    // get x, y coordinates
    get_coordinates_m(X, V, left_side);
    U_reflection(U_left, U_right, X[0], X[1], t, nx, ny);
}

/***********************
 *
 * MAIN FUNCTIONS
 *
 ***********************/


__device__ double basis_value(double r, double s, int i){
    if(i == 0){
        return 1.414213562373095E+00;
    }
    else if( i == 1){
        return -1.999999999999999E+00 + 5.999999999999999E+00 * r;
    }
    else {
        return -3.464101615137754E+00 + 3.464101615137750E+00*r + 6.928203230275512E+00*s;
    }
}


__device__ void canonical_coordinates(double *r, double *s, double x, double y,
                                      double a, double b, double c,
                                      double *s_x, double *s_y, int q){

    double d_l, d_v1, d_v2;
    double det, shift = -10.;
    double a_m, b_m, c_m, d_m, x_m, y_m;

    d_l =  abs(a*x + b*y +c) / sqrt(a*a + b*b);
    d_v1 = abs(a*s_x[q] + b*s_y[q] +c) / sqrt(a*a + b*b);
    d_v2 = abs(a*s_x[(q+1)%3] + b*s_y[(q+1)%3] +c) / sqrt(a*a + b*b);

    if(d_l <= d_v1 + 1e-10 && d_l <= d_v2 + 1e-10){
        a_m = s_x[q] + shift;
        b_m = s_x[ (q + 1) % 3 ]+ shift;
        c_m = s_y[q]+ shift;
        d_m = s_y[ (q + 1) % 3 ]+ shift;
        x_m = x+ shift;
        y_m = y+ shift;
        det = a_m * d_m - b_m * c_m;
        a = ( d_m*x_m + (-b_m)*y_m)/det;
        b = ((-c_m)*x_m + a_m*y_m)/det;

    }
    else if( (x - s_x[q])*(x - s_x[q]) + (y - s_y[q])*(y - s_y[q])  <   (x - s_x[(q + 1) % 3])*(x - s_x[(q + 1) % 3]) + (y - s_y[(q + 1) % 3])*(y - s_y[(q + 1) % 3]) ){ // v_1
        a = 1.;
        b = 0.;

    }
    else{ // v_2
        a = 0.;
        b = 1.;
    }


    // canonical coordinates
    if(q == 0){
        *r = a * 0. + b * 1.;
        *s = a * 0. + b * 0.;

    }
    else if (q == 1){
        *r = a * 1. + b * 0.;
        *s = a * 0. + b * 1.;

    }
    else{
        *r = a * 0. + b * 0.;
        *s = a * 1. + b * 0.;

    }

    // printf("%lf %lf\n", *r, *s);

}



__global__ void normal_limiter(double **C,
                               double *detector, int *limit,
                               int *curr_s1, int *curr_s2, int *curr_s3,
                               int *spos, int *d_schild1, int *d_schild2,
                               int *left_elem, int *right_elem,
                               int *lsn, int *rsn,
                               double *gxr, double *gxs, double *gyr, double *gys, double *gJ,
                               double *V1x, double *V1y, double *V2x, double *V2y, double *V3x, double *V3y,
                               int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) { 
            double U_i, U_c, Umin[N_MAX], Umax[N_MAX], alpha, min_alpha[N_MAX];
            int pos[6],  sID[3];
            double s_x[3], s_y[3];
            int neighbor_idx[6];
            int haschildren;
            double y;
            double grad_norm, nx[N_MAX], ny[N_MAX], xr, xs, yr, ys, J;
            double a_m, b_m, c_m;
            double a_e, b_e, c_e;
            double a_c, b_c, c_c;
            double r,s;
            double x_e, y_e;
            double x_l, y_l;
            double x_c, y_c;
            double x_m, y_m;
            double x_0, y_0;
            double det, dx, dy, d_c, d_v1, d_v2, f;



        if(1) {
        // if(limit[idx]) {
            for (int n = 0; n < N; n++) {
                min_alpha[n] = 1.;
            }

            x_m = (V1x[idx] + V2x[idx] + V3x[idx]) / 3.;
            y_m = (V1y[idx] + V2y[idx] + V3y[idx]) / 3.;

            // load the vertices of the element
            s_x[0] = V1x[idx];
            s_y[0] = V1y[idx];
            s_x[1] = V2x[idx];
            s_y[1] = V2y[idx];
            s_x[2] = V3x[idx];
            s_y[2] = V3y[idx];


            // compute the direction of the gradient
            xr = gxr[idx];
            xs = gxs[idx];
            yr = gyr[idx];
            ys = gys[idx];
     

            for(int n = 0; n < N; n++) {
                nx[n] = 0;
                ny[n] = 0; 

                for(int i = 0; i < n_p; i++) {
                    nx[n] += C[n*n_p + i][idx] * (basis_grad_x[n_quad * i + 0] * ys +    basis_grad_y[n_quad * i + 0] * (-yr));
                    ny[n] += C[n*n_p + i][idx] * (basis_grad_x[n_quad * i + 0] *(-xs) +  basis_grad_y[n_quad * i + 0] * xr);
                }

                grad_norm = sqrt(nx[n] * nx[n] + ny[n] * ny[n]);
                if(grad_norm > 1e-10) {
                    nx[n]/=grad_norm;
                    ny[n]/=grad_norm;
                }
            }


            // load the sides of the elements
            sID[0] = curr_s1[idx];
            sID[1] = curr_s2[idx];
            sID[2] = curr_s3[idx];



            for(int i = 0; i < 3; i++) {
                haschildren = d_schild1[sID[i]];
                if(haschildren > -1){
                    pos[i] = spos[d_schild1[sID[i]]];
                    pos[i + 3] = spos[d_schild2[sID[i]]];
                }
                else{
                    pos[i] = spos[ sID[i] ];
                    pos[i + 3] = -1;
                }

            }


            // get element neighbor indexes
            // and make sure we aren't on a boundary element
            for(int i = 0; i < 6; i++ ) {
                neighbor_idx[i] = -1;

                if(pos[i] > 0) {
                    if(left_elem[pos[i]] == idx )  {
                        neighbor_idx[i] = right_elem[pos[i]];
                    }
                    else{
                        neighbor_idx[i] = left_elem[pos[i]];
                    }
                }
            }


            for (int n = 0; n < N; n++) {
                // set initial stuff
                U_c = C[n*n_p + 0][idx] * basis[0];
                Umin[n] = U_c;
                Umax[n] = U_c;

                // get delmin and delmax
                for (int i = 0; i < 6; i++) {
                    if(neighbor_idx[i] > -1) {
                        U_i = C[n*n_p + 0][neighbor_idx[i]] * basis[0];
                        
                        Umin[n] = (U_i < Umin[n]) ? U_i : Umin[n];
                        Umax[n] = (U_i > Umax[n]) ? U_i : Umax[n];
                    }
                }
            }
            
            for(int q = 0; q < 6; q++ ) {
                // printf("neigbour is %i\n", neighbor_idx[q]);
                if(neighbor_idx[q] > -1) {
                    x_c = (V1x[neighbor_idx[q]] + V2x[neighbor_idx[q]] + V3x[neighbor_idx[q]]) / 3.;
                    y_c = (V1y[neighbor_idx[q]] + V2y[neighbor_idx[q]] + V3y[neighbor_idx[q]]) / 3.;


                    x_e = s_x[q%3];
                    y_e = s_y[q%3];
                    dx = s_x[(q%3+1)%3] - s_x[q%3];
                    dy = s_y[(q%3+1)%3] - s_y[q%3];

                    // line defined by the edge
                    a_e = -dy;
                    b_e = dx;
                    c_e = x_e*dy - y_e*dx; 

                    // printf("line defined by the edge %lf %lf %lf\n", a_e, b_e, c_e);

                    for(int n = 0; n < N; n++) { // should be N
                        if( sqrt(nx[n]*nx[n] + ny[n]*ny[n]) > 1e-10 && fabs(nx[n]*(x_c -x_m) + ny[n]*(y_c -y_m)) > 1e-10  ) {
                            // line defined by centroid
                            // a_c = nx[n];
                            // b_c = ny[n];
                            // c_c = -x_c*nx[n] - y_c* ny[n]; 

                            f = 0.6;
                            // line defined by mid-centroid
                            a_c = nx[n];
                            b_c = ny[n];
                            c_c = -(f*x_c + (1-f)*x_m)*nx[n] - (f*y_c + (1-f)*y_m)* ny[n]; 


                            // determine x_l, y_l, i.e. the limiting point
                            det = a_e*b_c - b_e*a_c;
                            if(abs(det) > 1e-10){ // gradient and edge are not parallel
                                x_l = -(c_e*b_c +    c_c*(-b_e))/det;
                                y_l = -(c_e*(-a_c) + c_c*a_e   )/det;
                            }
                            else { // both gradient and edge are parallel
                                x_l = (s_x[q%3] + s_x[(q%3+1)%3]) / 2.;
                                y_l = (s_y[q%3] + s_y[(q%3+1)%3]) / 2.;
                            }
                            // printf("limited point %lf %lf\n",x_l, y_l);


                            // line defined by the edge midpoint
                            a_m = nx[n];
                            b_m = ny[n];
                            c_m = -0.5*(s_x[(q%3+1)%3] + s_x[q%3])*nx[n] - 0.5*(s_y[(q%3+1)%3] + s_y[q%3])* ny[n]; 
                            // printf("line defined by the edge midpoint %lf %lf %lf\n", a_m, b_m, c_m);
                            canonical_coordinates(&r, &s, x_l, y_l,
                                                  a_m, b_m, c_m,
                                                  s_x, s_y, q%3);


                            U_i = 0.;
                            //evaluate U
                            for (int i = 0; i < n_p; i++) {
                                U_i += C[n*n_p + i][idx] * basis_value(r,s,i);
                            }

                            y = 1.;
                            alpha = 1.;
                            U_c = C[n*n_p + 0][idx] * basis[0];
                            // evaluate alpha
                            if (U_i > U_c) {
                                y = (Umax[n] - U_c)/(U_i - U_c);
                            } else if (U_i < U_c) {
                                y = (Umin[n] - U_c)/(U_i - U_c);
                            } 


                            alpha = min(1., y); // Barth Jesperson
                            if (alpha < min_alpha[n])  {
                                min_alpha[n] = alpha;
                            }

                        }



                    }

                }
            }

            // printf("%lf %lf %lf %lf\n", min_alpha[0], min_alpha[1], min_alpha[2], min_alpha[3]);
            for(int n = 0; n < N; n++){

                if(min_alpha[n] <= 0 -1e-10)
                    min_alpha[n] = 0.;
                C[n*n_p + 1][idx] *= min_alpha[n];
                C[n*n_p + 2][idx] *= min_alpha[n];

            }





        }
    }
}































__global__ void normal_limiter2(double **C,
                                double *detector, int *limit,
                                int *curr_s1, int *curr_s2, int *curr_s3,
                                int *spos, int *d_schild1, int *d_schild2,
                                int *left_elem, int *right_elem,
                                int *lsn, int *rsn,
                                double *gxr, double *gxs, double *gyr, double *gys, double *gJ,
                                double *V1x, double *V1y, double *V2x, double *V2y, double *V3x, double *V3y,
                                int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) { 
            double U_i, U_c, Umin[N_MAX], Umax[N_MAX], alpha, min_alpha[N_MAX];
            int pos[6],  sID[3];
            double s_x[3], s_y[3];
            int neighbor_idx[6];
            int haschildren;
            double y;
            double grad_norm, nx[N_MAX], ny[N_MAX], xr, xs, yr, ys, J;
            double a_m, b_m, c_m;
            double a_e, b_e, c_e;
            double a_c, b_c, c_c;
            double r,s;
            double x_e, y_e;
            double x_l, y_l;
            double x_c, y_c;
            double x_m, y_m;
            double x_0, y_0;
            double det, dx, dy, d_c, d_v1, d_v2, f;



        if(1) {
        // if(limit[idx]) {
            for (int n = 0; n < N; n++) {
                min_alpha[n] = 1.;
            }

            x_m = (V1x[idx] + V2x[idx] + V3x[idx]) / 3.;
            y_m = (V1y[idx] + V2y[idx] + V3y[idx]) / 3.;

            // load the vertices of the element
            s_x[0] = V1x[idx];
            s_y[0] = V1y[idx];
            s_x[1] = V2x[idx];
            s_y[1] = V2y[idx];
            s_x[2] = V3x[idx];
            s_y[2] = V3y[idx];


            // compute the direction of the gradient
            xr = gxr[idx];
            xs = gxs[idx];
            yr = gyr[idx];
            ys = gys[idx];
     

            for(int n = 0; n < N; n++) {
                nx[n] = 0;
                ny[n] = 0; 

                for(int i = 0; i < n_p; i++) {
                    nx[n] += C[n*n_p + i][idx] * (basis_grad_x[n_quad * i + 0] * ys +    basis_grad_y[n_quad * i + 0] * (-yr));
                    ny[n] += C[n*n_p + i][idx] * (basis_grad_x[n_quad * i + 0] *(-xs) +  basis_grad_y[n_quad * i + 0] * xr);
                }

                grad_norm = sqrt(nx[n] * nx[n] + ny[n] * ny[n]);
                if(grad_norm > 1e-10) {
                    nx[n]/=grad_norm;
                    ny[n]/=grad_norm;
                }
            }


            // load the sides of the elements
            sID[0] = curr_s1[idx];
            sID[1] = curr_s2[idx];
            sID[2] = curr_s3[idx];



            for(int i = 0; i < 3; i++) {
                haschildren = d_schild1[sID[i]];
                if(haschildren > -1){
                    pos[i] = spos[d_schild1[sID[i]]];
                    pos[i + 3] = spos[d_schild2[sID[i]]];
                }
                else{
                    pos[i] = spos[ sID[i] ];
                    pos[i + 3] = -1;
                }

            }


            // get element neighbor indexes
            // and make sure we aren't on a boundary element
            for(int i = 0; i < 6; i++ ) {
                neighbor_idx[i] = -1;

                if(pos[i] > 0) {
                    if(left_elem[pos[i]] == idx )  {
                        neighbor_idx[i] = right_elem[pos[i]];
                    }
                    else{
                        neighbor_idx[i] = left_elem[pos[i]];
                    }
                }
            }


            for (int n = 0; n < N; n++) {
                // set initial stuff
                U_c = C[n*n_p + 0][idx] * basis[0];
                Umin[n] = U_c;
                Umax[n] = U_c;

                // get delmin and delmax
                for (int i = 0; i < 6; i++) {
                    if(neighbor_idx[i] > -1) {
                        U_i = C[n*n_p + 0][neighbor_idx[i]] * basis[0];
                        
                        Umin[n] = (U_i < Umin[n]) ? U_i : Umin[n];
                        Umax[n] = (U_i > Umax[n]) ? U_i : Umax[n];
                    }
                }
            }
            
            for(int q = 0; q < 6; q++ ) {
                // printf("neigbour is %i\n", neighbor_idx[q]);
                if(neighbor_idx[q] > -1) {
                    x_c = (V1x[neighbor_idx[q]] + V2x[neighbor_idx[q]] + V3x[neighbor_idx[q]]) / 3.;
                    y_c = (V1y[neighbor_idx[q]] + V2y[neighbor_idx[q]] + V3y[neighbor_idx[q]]) / 3.;


                    x_e = s_x[q%3];
                    y_e = s_y[q%3];
                    dx = s_x[(q%3+1)%3] - s_x[q%3];
                    dy = s_y[(q%3+1)%3] - s_y[q%3];

                    // line defined by the edge
                    a_e = -dy;
                    b_e = dx;
                    c_e = x_e*dy - y_e*dx; 

                    // printf("line defined by the edge %lf %lf %lf\n", a_e, b_e, c_e);

                    for(int n = 0; n < N; n++) { // should be N
                        if( sqrt(nx[n]*nx[n] + ny[n]*ny[n]) > 1e-10 && fabs(nx[n]*(x_c -x_m) + ny[n]*(y_c -y_m)) > 1e-10  ) {
                            // line defined by centroid
                            // a_c = nx[n];
                            // b_c = ny[n];
                            // c_c = -x_c*nx[n] - y_c* ny[n]; 

                            f = 0.6;
                            // line defined by mid-centroid
                            a_c = nx[n];
                            b_c = ny[n];
                            c_c = -(f*x_c + (1-f)*x_m)*nx[n] - (f*y_c + (1-f)*y_m)* ny[n]; 


                            // determine x_l, y_l, i.e. the limiting point
                            det = a_e*b_c - b_e*a_c;
                            if(abs(det) > 1e-10){ // gradient and edge are not parallel
                                x_l = -(c_e*b_c +    c_c*(-b_e))/det;
                                y_l = -(c_e*(-a_c) + c_c*a_e   )/det;
                            }
                            else { // both gradient and edge are parallel
                                x_l = (s_x[q%3] + s_x[(q%3+1)%3]) / 2.;
                                y_l = (s_y[q%3] + s_y[(q%3+1)%3]) / 2.;
                            }
                            // printf("limited point %lf %lf\n",x_l, y_l);


                            // line defined by the edge midpoint
                            a_m = nx[n];
                            b_m = ny[n];
                            c_m = -0.5*(s_x[(q%3+1)%3] + s_x[q%3])*nx[n] - 0.5*(s_y[(q%3+1)%3] + s_y[q%3])* ny[n]; 
                            // printf("line defined by the edge midpoint %lf %lf %lf\n", a_m, b_m, c_m);
                            canonical_coordinates(&r, &s, x_l, y_l,
                                                  a_m, b_m, c_m,
                                                  s_x, s_y, q%3);


                            U_i = 0.;
                            //evaluate U
                            for (int i = 0; i < n_p; i++) {
                                U_i += C[n*n_p + i][idx] * basis_value(r,s,i);
                            }

                            y = 1.;
                            alpha = 1.;
                            U_c = C[n*n_p + 0][idx] * basis[0];
                            // evaluate alpha
                            if (U_i > U_c) {
                                y = (Umax[n] - U_c)/(U_i - U_c);
                            } else if (U_i < U_c) {
                                y = (Umin[n] - U_c)/(U_i - U_c);
                            } 


                            alpha = min(1., y); // Barth Jesperson
                            if (alpha < min_alpha[n])  {
                                min_alpha[n] = alpha;
                            }

                        }



                    }

                }
            }

            // printf("%lf %lf %lf %lf\n", min_alpha[0], min_alpha[1], min_alpha[2], min_alpha[3]);
            for(int n = 0; n < N; n++){

                if(min_alpha[n] <= 0 -1e-10)
                    min_alpha[n] = 0.;
                C[n*n_p + 1][idx] *= min_alpha[n];
                C[n*n_p + 2][idx] *= min_alpha[n];

            }





        }
    }
}



































__global__ void shock_detector(double **C, 
                             double *V1x, double *V1y,
                             double *V2x, double *V2y,
                             double *V3x, double *V3y,
                             int *curr_side, int *original_side,
                             int *slevel,
                             double *side_len, 
                             int *left_elem,  int *right_elem,
                             int *left_side_number, int *right_side_number,
                             double *Nx, double *Ny, double t,
                             double *detector, double *length, 
                             int csum, int max) {
    int pow2[18];
    pow2[0] = 1; pow2[1] = 2; pow2[2] = 4; pow2[3] = 8; pow2[4] = 16; pow2[5] = 32; pow2[6] = 64; pow2[7] = 128; pow2[8] = 256; pow2[9] = 512;
    pow2[10] = 1024; pow2[11] = 2048; pow2[12] = 4096; pow2[13] = 8192; pow2[14] = 16384; pow2[15] = 32768; pow2[16] = 65536; pow2[17] = 131072;

    int idx = blockDim.x * blockIdx.x + threadIdx.x + csum;
    if (idx < max + csum) {
        int i, j, n;
        int level;
        int left_idx, right_idx, left_side, right_side;
        int soriginal_sideID, sID;
        double len, nx, ny;
        double C_left [N_MAX * NP_MAX];
        double C_right[N_MAX * NP_MAX];
        double U_left[N_MAX], U_right[N_MAX];
        double V[6];
        double detector_left=0, detector_right=0;

        // read edge data
        sID = curr_side[idx];
        level = slevel[idx];
        soriginal_sideID = original_side[idx];
        len = side_len[soriginal_sideID] / pow2[level]; 

        nx  = Nx[soriginal_sideID];
        ny  = Ny[soriginal_sideID];

        left_idx   = left_elem[idx];
        right_idx  = right_elem[idx];
        left_side  = left_side_number[idx];
        right_side = right_side_number[idx];


        // get verticies
        V[0] = V1x[left_idx];
        V[1] = V1y[left_idx];
        V[2] = V2x[left_idx];
        V[3] = V2y[left_idx];
        V[4] = V3x[left_idx];
        V[5] = V3y[left_idx];

        // read coefficients
        if (right_idx > -1) {
            for (n = 0; n < N; n++) {
                for (i = 0; i < n_p; i++) {
                    C_left[n*n_p + i]  = C[n*n_p + i][left_idx];
                    C_right[n*n_p + i] = C[n*n_p + i][right_idx];
                }
            }
        } else {
            for (n = 0; n < N; n++) {
                for (i = 0; i < n_p; i++) {
                    C_left[n*n_p + i]  = C[n*n_p + i][left_idx];
                }
            }
        } 
        

        eval_left_right(C, C_left, C_right,
                        U_left, U_right,
                        nx, ny,
                        V, n_quad1d /2, left_side, right_side,
                        left_idx, right_idx, t);

        if((U_left[1] /  U_left[0])*nx + (U_left[2] /  U_left[0])*ny < 0){
            length[left_idx]  += len;
            detector_left = 1;
        }
        if ( right_idx > -1)
            if( (U_right[1] /  U_right[0])*nx + (U_right[2] /  U_right[0])*ny > 0   ){
                length[right_idx]  += len;
                detector_right = 1;
            }
        // at each integration point
        for (j = 0; j < n_quad1d; j++) {

            // calculate the left and right values along the surface
            eval_left_right(C, C_left, C_right,
                            U_left, U_right,
                            nx, ny,
                            V, j, left_side, right_side,
                            left_idx, right_idx, t);

            if(detector_left)
                detector[left_idx]  += len / 2. * w_oned[j] * (U_left[0] - U_right[0]);
            if(detector_right)
                detector[right_idx] +=  len / 2. * w_oned[j] * ( U_right[0] - U_left[0] );

        }
    }
}


__global__ void limit_c(double **C,
                        int *curr_s1, int *curr_s2, int *curr_s3,
                        int *spos, int *d_schild1, int *d_schild2,
                        int *left_elem, int *right_elem,
                        int *lsn, int *rsn,
                        int num_elem) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) { 
        double U_i, U_c, Umin, Umax, alpha, min_alpha;
        int s[6], sn[6], sID[3];
        int n, i, side;
        int neighbor_idx[6];
        int haschildren;
        double y;




    // load the sides of the elements
        sID[0] = curr_s1[idx];
        sID[1] = curr_s2[idx];
        sID[2] = curr_s3[idx];

        for(int i = 0; i < 3; i++) {
            haschildren = d_schild1[sID[i]];
            if(haschildren > -1){
                s[i] = spos[d_schild1[sID[i]]];
                s[i + 3] = spos[d_schild2[sID[i]]];
            }
            else{
                s[i] = spos[ sID[i] ];
                s[i + 3] = -1;
            }
        }






        // get element neighbor indexes
        // and make sure we aren't on a boundary element
        for( i = 0; i < 6; i++ ) {
            neighbor_idx[i] = -1;
            sn[i] =  -1; 

            if(s[i] > 0) {
                if(left_elem[s[i]] == idx )  {
                    neighbor_idx[i] = right_elem[s[i]];
                    sn[i] =  lsn[s[i]];
                }
                else{
                    neighbor_idx[i] = left_elem[s[i]];
                    sn[i] =  rsn[s[i]]; 
                }
                if(right_elem[s[i]] < 0)
                    neighbor_idx[i] = idx;
            }
        }
        
        for (n = 0; n < N; n++) {
            // set initial stuff
            U_c = C[n*n_p + 0][idx] * basis[0];
            Umin = U_c;
            Umax = U_c;

            // get delmin and delmax
            for (i = 0; i < 6; i++) {
                if(neighbor_idx[i] > -1) {
                    U_i = C[n*n_p + 0][neighbor_idx[i]] * basis[0];
                    
                    Umin = (U_i < Umin) ? U_i : Umin;
                    Umax = (U_i > Umax) ? U_i : Umax;
                }
            }

            min_alpha = 1.;

            // at the gauss points
            int j;
            for (j = 0; j < n_quad1d; j++) {
            //     // along the boundaries
                for (side = 0; side < 6; side++) {
                    if(neighbor_idx[side] > -1) {
                        U_i = 0.;
                        //evaluate U
                        for (i = 0; i < n_p; i++) {
                            U_i += C[n*n_p + i][idx] 
                                 * basis_side[sn[side] * n_p * n_quad1d + i * n_quad1d + j];
                        }

                    y = 1.;
                    alpha = 1.;
                    // evaluate alpha
                    if (U_i > U_c) {
                        y = (U_c - Umin)/(U_i - U_c);
                    } else if (U_i < U_c) {
                        y = (U_c - Umax)/(U_i - U_c);
                    } 

                    alpha = min(1., y); // Barth Jesperson
                    // alpha = (y*y + 2*y)/(y*y+y+2); // Venkatakrishnan

                    // if( 0 <= y < 1.5)
                    // // if( 0 <= y < 1.75)
                    //     // alpha = (-16./343.)*y*y*y - (8./49.)*y*y + y; // modified
                    //     alpha = (-4./27.)*y*y*y + y; // modified
                    // else
                    //     alpha = 1.0;

                        if (alpha < min_alpha)  {
                            min_alpha = alpha;
                        }
                    }


                }


            }

            if (min_alpha < 0) {
                min_alpha = 0.;
            }


            // limit the slope
            //C[0 * num_elem + idx] = min_alpha / basis[0];
            //C[1 * num_elem + idx] = 0;
            //C[2 * num_elem + idx] = 0;
             
             
            C[n*n_p + 1][idx] *= min_alpha;
            C[n*n_p + 2][idx] *= min_alpha;
        }
    }
}

__device__ void eval_boundary_m(double *U_left, double *U_right, 
                                double *V, double nx, double ny,
                                int left_side, double t, int right_idx) {
    switch (right_idx) {
        // reflecting 
        case -1: 
            reflecting_boundary_m(U_left, U_right,
                V, nx, ny,
                left_side, t); 
            break;
        // outflow 
        case -2: 
            outflow_boundary_m(U_left, U_right,
                V, nx, ny,
                left_side, t);
            break;
        // inflow 
        case -3: 
            inflow_boundary_m(U_left, U_right,
                V, nx, ny, 
                left_side, t);
            break;
    }
}

__device__ void eval_boundary(double *U_left, double *U_right, 
                              double *V, double nx, double ny,
                              int j, int left_side, double t, int right_idx) {
    switch (right_idx) {
        // reflecting 
        case -1: 
            reflecting_boundary(U_left, U_right,
                V, nx, ny,
                j, left_side, t);
            break;
        // outflow 
        case -2: 
            outflow_boundary(U_left, U_right,
                V, nx, ny,
                j, left_side, t);
            break;
        // inflow 
        case -3: 
            inflow_boundary(U_left, U_right,
                V, nx, ny, 
                j, left_side, t);
            break;
    }
}

/* left & right evaluator
 * 
 * calculate U_left and U_right at the integration point,
 * using boundary conditions when necessary.
 */
 __device__ void eval_left_right(double **C, double *C_left, double *C_right, 
                             double *U_left, double *U_right,
                             double nx, double ny,
                             double *V, int j, 
                             int left_side, int right_side,
                             int left_idx, int right_idx,
                             double t) { 

    int i, n;

    // set U to 0
    for (n = 0; n < N; n++) {
        U_left[n]  = 0.;
        U_right[n] = 0.;
    }

    //evaluate U at the integration points
    for (i = 0; i < n_p; i++) {
        for (n = 0; n < N; n++) {
            U_left[n] += C_left[n*n_p + i] * 
                         basis_side[left_side * n_p * n_quad1d + i * n_quad1d + j];
        }
    }

    // make sure U_left is physical
    check_physical(C, C_left, U_left, left_idx);

    // boundaries are sorted to avoid warp divergence
    if (right_idx < 0) {
        eval_boundary(U_left, U_right, V, nx, ny, j, left_side, t, right_idx);
    } else {
        // evaluate the right side at the integration point
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                U_right[n] += C_right[n*n_p + i] * 
                              basis_side[right_side * n_p * n_quad1d + i * n_quad1d + n_quad1d - j - 1];
            }
        }

        // make sure U_right is physical
        check_physical(C, C_right, U_right, right_idx);
    }
}

 __device__ void eval_left_right_m(double **C, double *C_left, double *C_right, 
                                         double *U_left, double *U_right,
                                         double *basis_midpoint,
                                         double nx, double ny,
                                         double *V, 
                                         int left_side, int right_side,
                                         int left_idx, int right_idx,
                                         double t) { 

    int i, n;

    // set U to 0
    for (n = 0; n < N; n++) {
        U_left[n]  = 0.;
        U_right[n] = 0.;
    }

    //evaluate U at the integration points
    for (i = 0; i < n_p; i++) {
        for (n = 0; n < N; n++) {
            U_left[n] += C_left[n*n_p + i] * basis_midpoint[3*i + left_side];
        }
    }

    // make sure U_left is physical
    check_physical(C, C_left, U_left, left_idx);

    // boundaries are sorted to avoid warp divergence
    if (right_idx < 0) {
        eval_boundary_m(U_left, U_right, V, nx, ny, left_side, t, right_idx);
    } else {
        // evaluate the right side at the integration point
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                U_right[n] += C_right[n*n_p + i] * basis_midpoint[3*i+right_side];
            }
        }

        // make sure U_right is physical
        check_physical(C, C_right, U_right, right_idx);
    }
}



__global__ void eval_surface(double **C, 
                             double **C_rhs, 
                             double *side_len, 
                             double *V1x, double *V1y,
                             double *V2x, double *V2y,
                             double *V3x, double *V3y,
                             int *curr_side, int *original_side,
                             int *slevel,
                             int *left_elem,  int *right_elem,
                             int *left_side_number, int *right_side_number, double max_lambda,
                             double *Nx, double *Ny, double t, int csum, int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x + csum;
    int pow2[18];
    pow2[0] = 1; pow2[1] = 2; pow2[2] = 4; pow2[3] = 8; pow2[4] = 16; pow2[5] = 32; pow2[6] = 64; pow2[7] = 128; pow2[8] = 256; pow2[9] = 512;
    pow2[10] = 1024; pow2[11] = 2048; pow2[12] = 4096; pow2[13] = 8192; pow2[14] = 16384; pow2[15] = 32768; pow2[16] = 65536; pow2[17] = 131072;

    if (idx < max + csum) {
        int i, j, n;
        int left_idx, right_idx, left_side, right_side;
        int soriginal_sideID, sID, level;
        double len, nx, ny;
        double C_left [N_MAX * NP_MAX];
        double C_right[N_MAX * NP_MAX];
        double C_rhsleft [N_MAX * NP_MAX];
        double C_rhsright[N_MAX * NP_MAX];
        double U_left[N_MAX], U_right[N_MAX];
        double F_n[N_MAX];
        double V[6];

        // read edge data
        sID = curr_side[idx];
        level = slevel[idx];
        soriginal_sideID = original_side[idx];
        len = side_len[soriginal_sideID] / pow2[level]; 
        nx  = Nx[soriginal_sideID];
        ny  = Ny[soriginal_sideID];

        left_idx   = left_elem[idx];
        right_idx  = right_elem[idx];
        left_side  = left_side_number[idx];
        right_side = right_side_number[idx];

        // if(sID == 2174 || sID ==  2270 || sID == 3530)
        //     printf("%i %i %i %lf %lf %lf %i %i %i %i\n", sID, level, soriginal_sideID, len, nx, ny, left_idx, right_idx, left_side, right_side);
        // if(left_idx == 659 || right_idx == 659)
        //     printf("%i %i %i %lf %lf %lf %i %i %i %i\n", sID, level, soriginal_sideID, len, nx, ny, left_idx, right_idx, left_side, right_side);


        if(right_idx != -4) {

        // get verticies
            V[0] = V1x[left_idx];
            V[1] = V1y[left_idx];
            V[2] = V2x[left_idx];
            V[3] = V2y[left_idx];
            V[4] = V3x[left_idx];
            V[5] = V3y[left_idx];

            // read coefficients
            if (right_idx > -1) {
                for (n = 0; n < N; n++) {
                    for (i = 0; i < n_p; i++) {
                        // C_left[n*n_p + i]  = C[num_elem * n_p * n + i * num_elem + left_idx];
                        // C_right[n*n_p + i] = C[num_elem * n_p * n + i * num_elem + right_idx];
                        C_left[n*n_p + i]  = C[n*n_p + i][left_idx];
                        C_right[n*n_p + i] = C[n*n_p + i][right_idx];
                    }
                }
            } else {
                for (n = 0; n < N; n++) {
                    for (i = 0; i < n_p; i++) {
                        C_left[n*n_p + i]  = C[n*n_p + i][left_idx];
                    }
                }
            }



            for (n = 0; n < N; n++) {
                for (i = 0; i < n_p; i++) {
                    C_rhsleft[n*n_p + i]  = C_rhs[n*n_p + i][left_idx];
                }
            }
            if (right_idx > -1)
                for (n = 0; n < N; n++) {
                    for (i = 0; i < n_p; i++) {
                        C_rhsright[n*n_p + i]  = C_rhs[n*n_p + i][right_idx];
                    }
                }
            // if(idx == 151 || idx ==  44 || idx == 262){
            // if(idx == 250 || idx ==  22 || idx == 133){
            //     printf("before %i, lidx %i ridx %i, l %lf r %lf \n", idx, left_idx, right_idx, C_rhsleft[0], C_rhsright[0]);
            // }
            // at each integration point
            for (j = 0; j < n_quad1d; j++) {

                // calculate the left and right values along the surface
                eval_left_right(C, C_left, C_right,
                                U_left, U_right,
                                nx, ny,
                                V, j, left_side, right_side,
                                left_idx, right_idx, t);


                // compute F_n(U_left, U_right)
                riemann_solver(F_n, U_left, U_right, V, t, nx, ny, j, left_side, max_lambda);

                // multiply across by phi_i at this integration point
                for (i = 0; i < n_p; i++) {
                    for (n = 0; n < N; n++) {
                        C_rhsleft[n*n_p + i]  += -len / 2 * (w_oned[j] * F_n[n] * basis_side[left_side  * n_p * n_quad1d + i * n_quad1d + j]) ;
                        if (right_idx > -1)
                            C_rhsright[n*n_p + i] +=  len / 2 * (w_oned[j] * F_n[n] * basis_side[right_side * n_p * n_quad1d + i * n_quad1d + n_quad1d - 1 - j]);
                    }

                }



            }

                // if(idx == 151 || idx ==  44 || idx == 262){
                // if(idx == 250 || idx ==  22 || idx == 133){
                //     printf("%i l %lf r %lf , Fn %lf \n", idx, C_rhsleft[0], C_rhsright[0], F_n[0]);
                // }

            for (n = 0; n < N; n++) {
                for (i = 0; i < n_p; i++) {
                    C_rhs[n*n_p + i][left_idx]  = C_rhsleft[n*n_p + i];
                }
            }
            if (right_idx > -1)
                for (n = 0; n < N; n++) {
                    for (i = 0; i < n_p; i++) {
                        C_rhs[n*n_p + i][right_idx]  = C_rhsright[n*n_p + i];
                    }
                }

        }
    }
}


// __global__ void eval_surface_m(double **C, double **C_rhs, 
//                              double *basis_midpoint,
//                              double *side_len, 
//                              double *V1x, double *V1y,
//                              double *V2x, double *V2y,
//                              double *V3x, double *V3y,
//                              int *curr_side, int *original_side,
//                              int *slevel,
//                              int *left_elem,  int *right_elem,
//                              int *left_side_number, int *right_side_number,
//                              double *Nx, double *Ny, double t, int csum, int max) {
//     int idx = blockDim.x * blockIdx.x + threadIdx.x + csum;
//     int pow2[18];
//     pow2[0] = 1; pow2[1] = 2; pow2[2] = 4; pow2[3] = 8; pow2[4] = 16; pow2[5] = 32; pow2[6] = 64; pow2[7] = 128; pow2[8] = 256; pow2[9] = 512;
//     pow2[10] = 1024; pow2[11] = 2048; pow2[12] = 4096; pow2[13] = 8192; pow2[14] = 16384; pow2[15] = 32768; pow2[16] = 65536; pow2[17] = 131072;

//     if (idx < max + csum) {
//         int i, j, n;
//         int left_idx, right_idx, left_side, right_side;
//         int soriginal_sideID, sID, level;
//         double len, nx, ny;
//         double C_left [N_MAX * NP_MAX];
//         double C_right[N_MAX * NP_MAX];
//         double C_rhsleft [N_MAX * NP_MAX];
//         double C_rhsright[N_MAX * NP_MAX];
//         double U_left[N_MAX], U_right[N_MAX];
//         double F_n[N_MAX];
//         double V[6];

//         // read edge data
//         sID = curr_side[idx];
//         level = slevel[idx];
//         soriginal_sideID = original_side[idx];
//         len = side_len[soriginal_sideID] / pow2[level]; 
//         nx  = Nx[soriginal_sideID];
//         ny  = Ny[soriginal_sideID];

//         left_idx   = left_elem[idx];
//         right_idx  = right_elem[idx];
//         left_side  = left_side_number[idx];
//         right_side = right_side_number[idx];


//         if(right_idx != -4) {
//         // get verticies
//             V[0] = V1x[left_idx];
//             V[1] = V1y[left_idx];
//             V[2] = V2x[left_idx];
//             V[3] = V2y[left_idx];
//             V[4] = V3x[left_idx];
//             V[5] = V3y[left_idx];

//             // read coefficients
//             if (right_idx > -1) {
//                 for (n = 0; n < N; n++) {
//                     for (i = 0; i < n_p; i++) {
//                         C_left[n*n_p + i]  = C[n*n_p + i][left_idx];
//                         C_right[n*n_p + i] = C[n*n_p + i][right_idx];
//                     }
//                 }
//             } else {
//                 for (n = 0; n < N; n++) {
//                     for (i = 0; i < n_p; i++) {
//                         C_left[n*n_p + i]  = C[n*n_p + i][left_idx];
//                     }
//                 }
//             }



//             for (n = 0; n < N; n++) {
//                 for (i = 0; i < n_p; i++) {
//                     C_rhsleft[n*n_p + i]  = C_rhs[n*n_p + i][left_idx];
//                 }
//             }
//             if (right_idx > -1)
//                 for (n = 0; n < N; n++) {
//                     for (i = 0; i < n_p; i++) {
//                         C_rhsright[n*n_p + i]  = C_rhs[n*n_p + i][right_idx];
//                     }
//                 }

//             eval_left_right_m(C, C_left, C_right,
//                               U_left, U_right,
//                               basis_midpoint,
//                               nx, ny,
//                               V, left_side, right_side,
//                               left_idx, right_idx, t);


//             riemann_solver_m(F_n, U_left, U_right, V, t, nx, ny, left_side);
//             // if(idx ==0)
//             //     printf("%lf %lf %lf n(%lf,%lf), %lf \n", U_left[0], U_right[0], F_n[0], nx, ny, basis_midpoint[left_side*n_p + 0]);
        
//             for (n = 0; n < N; n++) {
//                 C_rhsleft[n*n_p + 0]  += -len * (F_n[n] * basis_midpoint[0*n_p + left_side]) ;
//                 if (right_idx > -1)
//                     C_rhsright[n*n_p + 0] +=  len * (F_n[n] * basis_midpoint[0*n_p + right_side]);
//             }

//             for (i = 1; i < n_p; i++) {
//             // at each integration point
//                 for (j = 0; j < n_quad1d; j++) {
//                     // calculate the left and right values along the surface
//                     eval_left_right(C, C_left, C_right,
//                                     U_left, U_right,
//                                     nx, ny,
//                                     V, j, left_side, right_side,
//                                     left_idx, right_idx, t);


//                     // compute F_n(U_left, U_right)
//                     riemann_solver(F_n, U_left, U_right, V, t, nx, ny, j, left_side);

//                     // multiply across by phi_i at this integration point
//                     for (n = 0; n < N; n++) {
//                         C_rhsleft[n*n_p + i]  += -len / 2 * (w_oned[j] * F_n[n] * basis_side[left_side  * n_p * n_quad1d + i * n_quad1d + j]) ;
//                         if (right_idx > -1)
//                             C_rhsright[n*n_p + i] +=  len / 2 * (w_oned[j] * F_n[n] * basis_side[right_side * n_p * n_quad1d + i * n_quad1d + n_quad1d - 1 - j]);
//                     }
//                 }
//             }


//             for (n = 0; n < N; n++) {
//                 for (i = 0; i < n_p; i++) {
//                     C_rhs[n*n_p + i][left_idx]  = C_rhsleft[n*n_p + i];
//                 }
//             }
//             if (right_idx > -1)
//                 for (n = 0; n < N; n++) {
//                     for (i = 0; i < n_p; i++) {
//                         C_rhs[n*n_p + i][right_idx]  = C_rhsright[n*n_p + i];
//                     }
//                 }

//         }
//     }
// }






// __global__ void eval_surface(double **C, 
//                              double **C_rhs, 
//                              double *side_len, 
//                              double *V1x, double *V1y,
//                              double *V2x, double *V2y,
//                              double *V3x, double *V3y,
//                              int *curr_side, int *original_side,
//                              int *slevel,
//                              int *left_elem,  int *right_elem,
//                              int *left_side_number, int *right_side_number,
//                              double *detector, int *limit, double *circles,
//                              double *Nx, double *Ny, double t, int csum, int max) {
//     int idx = blockDim.x * blockIdx.x + threadIdx.x + csum;
//     int pow2[18];
//     pow2[0] = 1; pow2[1] = 2; pow2[2] = 4; pow2[3] = 8; pow2[4] = 16; pow2[5] = 32; pow2[6] = 64; pow2[7] = 128; pow2[8] = 256; pow2[9] = 512;
//     pow2[10] = 1024; pow2[11] = 2048; pow2[12] = 4096; pow2[13] = 8192; pow2[14] = 16384; pow2[15] = 32768; pow2[16] = 65536; pow2[17] = 131072;

//     if (idx < max + csum) {
//         int i, j, n;
//         int left_idx, right_idx, left_side, right_side;
//         int soriginal_sideID, sID, level;
//         double len, nx, ny;
//         double C_left [N_MAX * NP_MAX];
//         double C_right[N_MAX * NP_MAX];
//         double C_rhsleft [N_MAX * NP_MAX];
//         double C_rhsright[N_MAX * NP_MAX];
//         double U_left[N_MAX], U_right[N_MAX];
//         double F_n[N_MAX];
//         double V[6];
//         double max, diff, order = 1;


//         // read edge data
//         sID = curr_side[idx];
//         level = slevel[idx];
//         soriginal_sideID = original_side[idx];
//         len = side_len[soriginal_sideID] / pow2[level]; 
//         nx  = Nx[soriginal_sideID];
//         ny  = Ny[soriginal_sideID];

//         left_idx   = left_elem[idx];
//         right_idx  = right_elem[idx];
//         left_side  = left_side_number[idx];
//         right_side = right_side_number[idx];



//         if(right_idx != -4) {
//         // get verticies
//             V[0] = V1x[left_idx];
//             V[1] = V1y[left_idx];
//             V[2] = V2x[left_idx];
//             V[3] = V2y[left_idx];
//             V[4] = V3x[left_idx];
//             V[5] = V3y[left_idx];

//             // read coefficients
//             if (right_idx > -1) {
//                 for (n = 0; n < N; n++) {
//                     for (i = 0; i < n_p; i++) {
//                         // C_left[n*n_p + i]  = C[num_elem * n_p * n + i * num_elem + left_idx];
//                         // C_right[n*n_p + i] = C[num_elem * n_p * n + i * num_elem + right_idx];
//                         C_left[n*n_p + i]  = C[n*n_p + i][left_idx];
//                         C_right[n*n_p + i] = C[n*n_p + i][right_idx];
//                     }
//                 }
//             } else {
//                 for (n = 0; n < N; n++) {
//                     for (i = 0; i < n_p; i++) {
//                         C_left[n*n_p + i]  = C[n*n_p + i][left_idx];
//                     }
//                 }
//             }



//             for (n = 0; n < N; n++) {
//                 for (i = 0; i < n_p; i++) {
//                     C_rhsleft[n*n_p + i]  = C_rhs[n*n_p + i][left_idx];
//                 }
//             }
//             if (right_idx > -1)
//                 for (n = 0; n < N; n++) {
//                     for (i = 0; i < n_p; i++) {
//                         C_rhsright[n*n_p + i]  = C_rhs[n*n_p + i][right_idx];
//                     }
//                 }

//             // at each integration point
//             for (j = 0; j < n_quad1d; j++) {

//                 // calculate the left and right values along the surface
//                 eval_left_right(C, C_left, C_right,
//                                 U_left, U_right,
//                                 nx, ny,
//                                 V, j, left_side, right_side,
//                                 left_idx, right_idx, t);


//                 // compute F_n(U_left, U_right)
//                 riemann_solver(F_n, U_left, U_right, V, t, nx, ny, j, left_side);



//                 // multiply across by phi_i at this integration point
//                 for (i = 0; i < n_p; i++) {
//                     for (n = 0; n < N; n++) {
//                         C_rhsleft[n*n_p + i]  += -len / 2 * (w_oned[j] * F_n[n] * basis_side[left_side  * n_p * n_quad1d + i * n_quad1d + j]) ;
//                         if (right_idx > -1)
//                             C_rhsright[n*n_p + i] +=  len / 2 * (w_oned[j] * F_n[n] * basis_side[right_side * n_p * n_quad1d + i * n_quad1d + n_quad1d - 1 - j]);
//                     }
//                 }



//                 max = (abs(U_left[0]) > abs(U_right[0])) ? abs(U_left[0]) : abs(U_right[0]) ;
//                 diff = abs(U_left[0] - U_right[0]) / max / pow(circles[left_idx]/2. , (order+1)/2.);
//                 if(diff > 1.)
//                     limit[left_idx] = 1;

            
//                 if(right_idx > -1) {
//                     diff = abs(U_left[0] - U_right[0]) / max / pow(circles[right_idx]/2. , (order+1)/2.);
//                     if(diff > 1.)
//                         limit[right_idx] = 1;

//                 }


//             }


//             for (n = 0; n < N; n++) {
//                 for (i = 0; i < n_p; i++) {
//                     C_rhs[n*n_p + i][left_idx]  = C_rhsleft[n*n_p + i];
//                 }
//             }
//             if (right_idx > -1)
//                 for (n = 0; n < N; n++) {
//                     for (i = 0; i < n_p; i++) {
//                         C_rhs[n*n_p + i][right_idx]  = C_rhsright[n*n_p + i];
//                     }
//                 }

//         }
//     }
// }



/* volume integrals
 *
 * evaluates and adds the volume integral to the rhs vector
 * THREADS: num_elem
 */
__global__ void eval_volume(double **C, double **rhs_volume, 
                            double *Xr, double *Yr, double *Xs, double *Ys,
                            double *V1x, double *V1y,
                            double *V2x, double *V2y,
                            double *V3x, double *V3y,
                            double *J, double t, int num_elem) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    if (idx < num_elem) {
        double V[6];
        double S[N_MAX];
        double C_left[N_MAX * NP_MAX];
        int i, j, k, n;
        double U[N_MAX];
        double flux_x[N_MAX], flux_y[N_MAX];
        double xr, yr, xs, ys;
        double detJ;

        // read coefficients
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                C_left[n*n_p + i]  = C[n*n_p + i][idx];
            }
        }


        // get element data
        xr = Xr[idx];
        yr = Yr[idx];
        xs = Xs[idx];
        ys = Ys[idx];



        // get jacobian determinant
        detJ = J[idx];
        
        // get verticies
        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        // set to 0
        for (i = 0; i < n_p; i++) {
            for (n = 0; n < N; n++) {
                rhs_volume[n*n_p + i][idx] = 0.;
            }
        }

        // for each integration point
        for (j = 0; j < n_quad; j++) {

            // initialize to zero
            for (n = 0; n < N; n++) {
                U[n] = 0.;
            }

            // calculate at the integration point
            for (k = 0; k < n_p; k++) {
                for (n = 0; n < N; n++) {
                    U[n] += C_left[n*n_p + k] * basis[n_quad * k + j];
                }
            }

            // if(idx ==33)
            //     printf("%i %lf %lf %lf %lf\n", idx, U[0], U[1], U[2], U[3]);

            // make sure U is physical
            check_physical(C, C_left, U, idx);

            // evaluate the flux
            eval_flux(U, flux_x, flux_y, V, t, j, -1);

            // get the source term
            source_term(S, U, V, t, j);

            // multiply across by phi_i
            for (i = 0; i < n_p; i++) {
                for (n = 0; n < N; n++) {
                    // add the sum
                    //     [fx fy] * [y_s, -y_r; -x_s, x_r] * [phi_x phi_y]
                    rhs_volume[n*n_p + i][idx] += 
                              flux_x[n] * ( basis_grad_x[n_quad * i + j] * ys
                                           -basis_grad_y[n_quad * i + j] * yr)
                            + flux_y[n] * (-basis_grad_x[n_quad * i + j] * xs 
                                          + basis_grad_y[n_quad * i + j] * xr);

                    // add the source term S * phi_i
                    rhs_volume[n*n_p + i][idx] += 
                            S[n] * basis[n_quad * i + j] * w[j] * detJ;

                }
            }
        }


    }
}

// __global__ void eval_volume(double *C, double **rhs_volume, 
//                             double *Xr, double *Yr, double *Xs, double *Ys,
//                             double *V1x, double *V1y,
//                             double *V2x, double *V2y,
//                             double *V3x, double *V3y,
//                             double *J, double t, int num_elem) {
//     int idx = blockDim.x * blockIdx.x + threadIdx.x;

//     if (idx < num_elem) {
//         double V[6];
//         double S[N_MAX];
//         double C_left[N_MAX * NP_MAX];
//         int i, j, k, n;
//         double U[N_MAX];
//         double flux_x[N_MAX], flux_y[N_MAX];
//         double xr, yr, xs, ys;
//         double detJ;

//         // read coefficients
//         for (i = 0; i < n_p; i++) {
//             for (n = 0; n < N; n++) {
//                 C_left[n*n_p + i]  = C[(n*n_p + i)*ELEM_MAX + idx];
//             }
//         }


//         // get element data
//         xr = Xr[idx];
//         yr = Yr[idx];
//         xs = Xs[idx];
//         ys = Ys[idx];



//         // get jacobian determinant
//         detJ = J[idx];
        
//         // get verticies
//         V[0] = V1x[idx];
//         V[1] = V1y[idx];
//         V[2] = V2x[idx];
//         V[3] = V2y[idx];
//         V[4] = V3x[idx];
//         V[5] = V3y[idx];

//         // set to 0
//         for (i = 0; i < n_p; i++) {
//             for (n = 0; n < N; n++) {
//                 rhs_volume[n*n_p + i][idx] = 0.;
//             }
//         }

//         // for each integration point
//         for (j = 0; j < n_quad; j++) {

//             // initialize to zero
//             for (n = 0; n < N; n++) {
//                 U[n] = 0.;
//             }

//             // calculate at the integration point
//             for (k = 0; k < n_p; k++) {
//                 for (n = 0; n < N; n++) {
//                     U[n] += C_left[n*n_p + k] * basis[n_quad * k + j];
//                 }
//             }

//             // if(idx ==33)
//             //     printf("%i %lf %lf %lf %lf\n", idx, U[0], U[1], U[2], U[3]);

//             // make sure U is physical
//             check_physical(C, C_left, U, idx);

//             // evaluate the flux
//             eval_flux(U, flux_x, flux_y, V, t, j, -1);

//             // get the source term
//             source_term(S, U, V, t, j);

//             // multiply across by phi_i
//             for (i = 0; i < n_p; i++) {
//                 for (n = 0; n < N; n++) {
//                     // add the sum
//                     //     [fx fy] * [y_s, -y_r; -x_s, x_r] * [phi_x phi_y]
//                     rhs_volume[(n*n_p + i)*ELEM_MAX + idx] += 
//                               flux_x[n] * ( basis_grad_x[n_quad * i + j] * ys
//                                            -basis_grad_y[n_quad * i + j] * yr)
//                             + flux_y[n] * (-basis_grad_x[n_quad * i + j] * xs 
//                                           + basis_grad_y[n_quad * i + j] * xr);

//                     // add the source term S * phi_i
//                     rhs_volume[(n*n_p + i)*ELEM_MAX + idx] += 
//                             S[n] * basis[n_quad * i + j] * w[j] * detJ;

//                 }
//             }
//         }


//     }
// }


/* plot exact
 * 
 * evaluates rho and E at the three vertex points for output
 * THREADS: num_elem
 */
__global__ void plot_exact(double **C, 
                           double *V1x, double *V1y,
                           double *V2x, double *V2y,
                           double *V3x, double *V3y,
                           double *Uv1, double *Uv2, double *Uv3, 
                           double t, int n,
                           int num_elem) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) {
        double U[4];

        // calculate values at the integration points
        U_exact(U, V1x[idx], V1y[idx], t);
        Uv1[idx] = U[n];
        U_exact(U, V2x[idx], V2y[idx], t);
        Uv2[idx] = U[n];
        U_exact(U, V3x[idx], V3y[idx], t);
        Uv3[idx] = U[n];
    }
}




/* evaluate L1 convergence error
 * 
 * evaluates rho and E at the three vertex points for output
 * THREADS: num_elem
 */
__global__ void convergence_L1(double *C, double *C_prev,
                               double *error, double *J,
                               int n) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) {
        int i, j;
        double e;
        double U, U_prev;
        double detJ;

        detJ = J[idx];
        e = 0.;
        for (j = 0; j < n_quad; j++) {

            // evaluate U at the integration point
            U      = 0.;
            U_prev = 0.;
            for (i = 0; i < n_p; i++) {
                U      += C[num_elem * n_p * n + i * num_elem + idx] * basis[i * n_quad + j];
                U_prev += C_prev[num_elem * n_p * n + i * num_elem + idx] * basis[i * n_quad + j];
            }

            // evaluate the L1 error
            e += w[j] * fabs(U - U_prev) * detJ;
        }

        // store the result
        __syncthreads();
        error[idx] = e;
    }
}

/* evaluate error
 * 
 * evaluates rho and E at the three vertex points for output
 * THREADS: num_elem
 */
__global__ void eval_error_L1(double **C, double *error,
                              double *V1x, double *V1y,
                              double *V2x, double *V2y,
                              double *V3x, double *V3y,
                              double *J,
                              int n, double t, int num_elem) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_elem) {
        int i, j;
        double e;
        double X[2];
        double U;
        double u[4];
        double V[6];
        double detJ;

        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        detJ = J[idx];
        e = 0.;
        for (j = 0; j < n_quad; j++) {

            // get the grid points on the mesh
            get_coordinates_2d(X, V, j);
            
            // evaluate U at the integration point
            U = 0.;
            for (i = 0; i < n_p; i++) {
                U += C[n*n_p + i][idx] * basis[i * n_quad + j];
            }

            // get the exact solution
            U_exact(u, X[0], X[1], t);

            // evaluate the L1 error
            e += w[j] * abs(u[n] - U) * detJ;
        }

        // store the result
        error[idx] = e;
    }
}

/* evaluate error
 * 
 * evaluates rho and E at the three vertex points for output
 * THREADS: num_elem
 */
__global__ void eval_error_L2(double **C, double *error,
                              double *V1x, double *V1y,
                              double *V2x, double *V2y,
                              double *V3x, double *V3y,
                              double *J,
                              int n, double t,
                              int max) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < max) {
        int i, j;
        double e;
        double X[2];
        double U;
        double u[4];
        double V[6];
        double detJ;

        V[0] = V1x[idx];
        V[1] = V1y[idx];
        V[2] = V2x[idx];
        V[3] = V2y[idx];
        V[4] = V3x[idx];
        V[5] = V3y[idx];

        detJ = J[idx];
        e = 0.;
        for (j = 0; j < n_quad; j++) {

            // get the grid points on the mesh
            get_coordinates_2d(X, V, j);
            
            // evaluate U at the integration point
            U = 0.;
            for (i = 0; i < n_p; i++) {
                U += C[n*n_p + i][idx] * basis[i * n_quad + j];
            } 

            // get the exact solution
            U_exact(u, X[0], X[1], t);

            // evaluate the L2 error
            e += w[j] * (u[n] - U) * (u[n] - U) * detJ;
        }
        // printf("%i %f\n",idx, e);
        // store the result
        error[idx] = e;
    }
}



/* check for convergence
 *
 * see if the difference in coefficients is less than the tolerance
 */
__global__ void check_convergence(double **c_prev, double **c, int max) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int n, i;
    if(idx < max){

        for (n = 0; n < N; n++) {
            for (i = 0; i < n_p; i++) {
                c_prev[n*n_p + i][idx] = fabs(c[n*n_p + i][idx] - c_prev[n*n_p + i][idx]);
            }
        }
    }
}
