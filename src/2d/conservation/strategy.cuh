void allocateStrategy(int max);
void initStrategy(int in_order);  

void refine_and_coarsen_fixed_number(double *d_error_estimate, int *d_curr_elem, 
                                       int *edr,int *epos,
                                       double top_frac, double bottom_frac,
                                       int verbose,
                                       int curr_num_elem) ;

void refine_and_coarsen_optimize(double *d_error_estimate, int *d_curr_elem, 
                                       int *edr,int *epos, 
                                       int verbose, int order,
                                       int curr_num_elem);

void refine_and_coarsen_optimize_parallel(double *d_error_estimate, int *d_curr_elem, 
                                       int *edr,int *epos, 
                                       int verbose, int order,
                                       int curr_num_elem);

void refine_and_coarsen_optimize_shock_A(double *d_error_estimate, int *d_curr_elem,  int *d_flag,
                                       int *edr,int *epos,
                                       int verbose, 
                                       int global_error_order_smooth,
                                       int global_error_order_disc,
                                       int global_error_order,
                                       int curr_num_elem) ;

void refine_and_coarsen_optimize_shock_B(double *d_error_estimate, int *d_curr_elem,  int *d_flag,
                                       int *edr,int *epos,
                                       int verbose, 
                                       int global_error_order_smooth,
                                       int global_error_order_disc,
                                       int global_error_order,
                                       int curr_num_elem) ;

void refine_and_coarsen(double *d_error_estimate, int *d_curr_elem, 
                                       int *edr,int *epos, 
                                       int verbose,
                                       double lower_error, double upper_error,
                                       int curr_num_elem) ;