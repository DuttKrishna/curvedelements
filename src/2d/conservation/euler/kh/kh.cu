#include "../euler.cu"


#define GAMMA 1.4
#define PI 3.14159265358979323846
#define rho_inf 1.
#define u_inf 0.
#define v_inf 1.

int limiter = BJ ;  


/***********************
 *
 * INITIAL CONDITIONS
 *
 ***********************/

/* initial condition function
 *
 * returns the value of the intial condition at point x,y
 */
__device__ void U0(double *U, double x, double y) {

    double p, rho, u, v, s = 0.05/sqrt(2.), w = 0.1;
    p = 2.5;
    rho = y > -0.5 && y < 0.5  ? 2. : 1.;
    u = y > -0.5 && y < 0.5   ? 0.5 : -0.5;
    v = w * sinpi(4*x)*(exp(-(y+0.5)*(y+0.5)/(2*s*s)) + exp(-(y-0.5)*(y-0.5)/(2*s*s))) ;

    U[0] = rho;
    U[1] = rho*u; 
    U[2] = rho*v; 
    U[3] = 0.5*rho*(u*u+v*v) + p / (GAMMA-1.)  ; 

}

/***********************
*
* INFLOW CONDITIONS
*
************************/

__device__ void U_inflow(double *U, double x, double y, double t) {
    U0(U, x , y );
}

/***********************
*
* OUTFLOW CONDITIONS
*
************************/

__device__ void U_outflow(double *U, double x, double y, double t) {
    U0(U, x , y );
}

/***********************
*
* REFLECTING CONDITIONS
*
************************/
__device__ void U_reflection(double *U_left, double *U_right,
                             double x, double y, double t,
                             double nx, double ny) {
        double dot;
        // set rho and E to be the same in the ghost cell
        U_right[0] = U_left[0];
        U_right[3] = U_left[3];

        // normal reflection
        dot = U_left[1] * nx + U_left[2] * ny;
        U_right[1] = U_left[1] - 2*dot*nx;
        U_right[2] = U_left[2] - 2*dot*ny;
}

/***********************
 *
 * EXACT SOLUTION
 *
 ***********************/

__device__ void U_exact(double *U, double x, double y, double t) {
    U0(U, x , y );
}


/***********************
 *
 * MAIN FUNCTION
 *
 ***********************/

__device__ double get_GAMMA() {
    return GAMMA;
}

/*
 * source term
 */
int main(int argc, char *argv[]) {
    run_dgcuda(argc, argv);
}
