void write_array(int *array, char *num, int max);
void write_array(double *array, char *num, int max);
void print_array( int *array, int length);
void print_array( double *array, int length);
void print_array( float *array, int length);
void quit();
int get_blocks(int num, int threads) ;
int get_num_valid(int *iflag, int *oflag, int max) ;
void check_max(int compare, int max, int *track, char *s) ;
void allocate( void **var, size_t size, size_t *memory_counter);
void swap(int **in1, int **in2) ;
void swap(double **in1, double **in2) ;
void write_C(double *d_curr_c, int local_N, int curr_num_elem, int local_n_p);
__global__ void set_value(int *array, int max);
__global__ void set_value(int *array, int val, int max);
__global__ void set_value(int *array, int *iflag, int val, int max) ;
__global__ void set_value(double *array, double val, int max) ;
__global__ void set_value(int *curr, int *pos, int max) ;
__global__ void set_value(double *array, int *iflag, double val, int max);
__global__ void is_e(int *in, int *iflag,int num, int max );
__global__ void is_l(int *dr, int *iflag,int num, int max ) ;
__global__ void is_g(int *dr, int *iflag,int num, int max );
__global__ void is_neq(int *in, int *iflag,int num, int max ) ;
__global__ void is_geq(int *dr, int *iflag,int num, int max ) ;
__global__ void is_neq(int *in1, int *in2, int *iflag, int max ) ;

__global__ void compaction(int *in, int *out, int *iflag, int *oflag, int max);
__global__ void compaction(int *out, int *iflag, int *oflag, int max);
__global__ void compaction_level(int *in, int *out, int *iflag, int *oflag, int max);
void position_compaction(int *out,int *inflag, int max, int *new_size);
void position_compaction_offset(int *out,int *inflag, int offset, int max, int *new_size);

void ordered_copy(int *out, int *pos, int max);


void in_place_compaction_init(int *inflag, int *to, int *from, int max, int *work_size, int *new_size);
void in_place_compaction(double *data, int *to, int* from, int work_size);
void in_place_compaction(int *data, int *to, int* from, int work_size);


__global__ void set_ordering(int *iflag, int *oflag, int *ordering, int offset, int max);
__global__ void initBoundaryFlag(int *curr_sides, int *right_elem, int *old, int *flag, int i, int old_num_sides, int max);
__global__ void initSideFlag(int *curr_sides, int *right_elem, int *old, int *flag, int old_num_sides, int max);


void allocateCompaction(int max);
void compaction(double *in, int *inflag, int max, int *new_size);
void compaction(int *in, int *inflag, int max, int *new_size);
void compaction(int *out, int *in, int *inflag, int max, int *new_size);
void count(int *in, int num, int max, int *new_size);

__global__  void write_idx(int *data, int work_size);
__global__  void move_to(int *out_data, int* in_data, int *to, int work_size);

__global__  void find_duplicates(int* in_data, int *flag, int work_size);
int isdifferent(int *a, int *b, int size);


__global__ void scale(int *flag, double *error, double *circles, int order, int max);
__global__ void scale(double *error, double *circles, int order, int max);
__global__ void divide(double *error, double *scale, int max);
__global__ void divide2(double *error, double *scale, int max);
__global__ void divide(double *out, double *a, double *b, int max);
__global__ void multiply(double *error, double *scale, int max);
__global__ void multiply(double *out_error,double *in_error, double scale, int max);
__global__ void reverse(double *out,double *in, int max);
__global__ void absolute_value(double *error, double *in, int max);